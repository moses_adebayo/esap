/**
 * Created by olajuwon on 11/28/14.
 */

function Registration(){
    this.submit = function(data){
        $('#reg-success').empty();
        $('#reg-error').empty();
        //show the loader
        $('.loading').removeClass('hidden');

        $.ajax({
            type: "POST",
            url : 'register',
            data : data,
            dataType: 'json'

        }).success(function(response){
                if(!response.status){
                    if(response.error_code == -1){
                        //invalid access
                        window.location.assign('illegal');
                    }else{
//                        console.log('response');
                        $('.loading').addClass('hidden');
                        $('#reg-error').empty().html(response.msg);

                    }
                }else{
//                   registration successfull
                    $('#reg-success').html(response.msg);
//                    $('#reg_form').trigger('reset');
                    $('.loading').addClass('hidden');
                    window.location.assign(response.url);
                }
            }).fail(function(response){
                console.log('fail')
            });
        $('html,body').animate({scrollTop: 0}, 500);
    }
}
var register  = new Registration();
$('document').ready(function(){
    $('#reg_form').on('submit', function(e){
        e.preventDefault();
        var data = $('#reg_form').serialize();
        register.submit(data);

    });
});
var info = new Information();


function Information(){
    this.load = function(){
        $('#info_content').html('<div class="loading_panel" id="loader"> <img src="images/loading.gif"></div>  ');
        $.ajax({
            type: "POST",
            url : "get_info",
            dataType: 'json'
        }).success(function(response){
                var keys = Object.keys(response)
                keys.forEach(function(record){
                    html = "<div class='info-title'> <div class='info-msg'> <h3>" +response[record].title + "</h3> <p>" + response[record].description + "</p></div> </div> ";
                    $('#loader').hide();
                    $('#info_content').append(html);
//                    console.log(response[record].title);
                });
            }).fail(function(response){
                console.log("fail");
            });
        setInterval(this.load, 60000);
    }

}

$(document).ready(function() {
    info.load();
});





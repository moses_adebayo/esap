/**
 * Created by olajuwon on 12/17/14.
 */

window.onload = init;

function init(){
    calendar_pop();
}

function calendar_pop(){
    $('.date-picker').datepicker({
        format: 'yyyy-mm-dd',
        startDate: '-3d'
    })
}

/**
 * Created by olajuwon on 2/3/2015.
 */


(function ($, window, document){

    function MosesSlider(elem){
        this.slider = $(elem).addClass('rs-slider');
        this.settings   = $.extend({}, defaults, settings);    // Obj: Merged user settings/defaults
        this.$slides = this.$slider.find('> li');
        this.totalSlides = this.$slides.length;
        this.cssTransitions = testBrowser.cssTransitions();
        this.cssTransforms3d = testBrowser.cssTransforms3d();
        this.currentPlace = this.startSlide;
        this.$currentSlide = this.$slides.eq(this.currentPlace);
        this.inPorgress = false;
        this.delay = 2000;

        this.autoPlay = true;

        this.init();
    }

    MosesSlider.prototype = {
        cycling: null,
        $slideImages: null,

        init: function(){
            //setup captions
            this.captions();

            for(var i = 0; i < this.totalSlides; i++){
                this.$slides.eq(i).addClass('rs-slide-' + i);
            }

            if(this.autoPlay){
                this.setAutoPlay();
            }

            //get the first image
            this.$slideImages = this.$slides.find('img:eq(0)').addClass('rs-slide-image');

            this.setup();
        }

        ,setup: function(){

            this.$currentSlide.css({'opacity' : 1, 'z-index' : 2});
        }

        ,control : function(){
            var that = this;

            //fire next method
            $('#ms-next').on('click', function(e){
                that.next();
            });

            //fire previous method
            $('#ms-prev').on('click', function(e){
                that.previous();
            });
        }
        ,next: function(){
            if(this.currentPlace === this.totalSlides - 1){
                this.transition(0, true);
            }else{
                this.transition(this.currentPlace + 1, true);
            }
        }
        ,previous: function(){
            if(this.currentPlace === 0){
                this.transition(this.totalSlides - 1, false);
            }else{
                this.transition(this.currentPlace - 1, false);
            }
        }
        ,setAutoPlay: function(){
            var that = this;
             //set timeout to object property
            this.cycling = setTimeout(function(){
                that.next();
            }, this.delay);
        }
        ,transition: function(slideNum, forward){
            //Check there is not transition in progress
            if(!this.inProgress){
                //if not already on requested slide
                if(slideNum !== this.currentPlace){
                    if(typeof  forward === 'undefined'){
                        forward = slideNum > this.currentPlace ? true : false;
                    }

                    //Assign next slide elem
                    this.$nextSlide = this.$slides.eq($slideNum);

                    //Assign next slide index property
                    this.currentPlace = slideNum;

                    new Transition(this, this.transition, forward);
                }
            }
        }
    }

    //Transition Object constructor
    function Transition(MosesSlider, transition, forward){
        this.MS = MS;
        this.MS.inPorgress = true;
        this.forward = forward;
        this.transition = transition;

        this.init();
    }

    Transition.prototype = {
        fallback: 'fade'

        ,anims: ['blockScale', 'sliceH', 'sliceV', 'kaleidoscope', 'fan', 'blindH', 'blindV']

        ,init: function(){
            //Call requested transition method
            this[this.transition]();

        }
        ,before: function(callback){
            var that = this;

            //Prepare slide opacity & z-index
            this.MS.$currentSlide.css('z-index', 2);
            this.MS.$nextSlide.css({'opacity' : 1, 'z-index' : 1});

            //check if transition describes a setup method
            if(typeof this.setup === 'function'){
                var transition = this.setup();

                setTimeout(function(){
                    callback(transition);
                }, 20);
            }else{
                //execute transition
                this.execute();
            }


            //Listen for css transition end on elem
            if(this.MS.cssTransitions){
                $(this.listenTo).one('webkitTransitionEnd transitionend otransitionend oTransitionEnd mstransitionend', $.proxy(this.after, this));
            }
        }

        ,after: function(){
            this.MS.$slider.removeAttr('style');
            this.MS.$currentSlide.removeAttr('style');
            this.MS.$nextSlide.removeAttr('style');

            this.MS.$currentSlide.css({
               zIndex: 1,
                opacity: 0
            });

            this.MS.$nextSlide.css({
                zIndex: 2,
                opacity: 1
            });

            // Additional reset steps required by transition (if any exist)
            if (typeof this.reset === 'function') {
                this.reset();
            }

            // If slideshow is active, reset the timeout
            if (this.RS.settings.autoPlay) {
                clearTimeout(this.RS.cycling);
                this.RS.setAutoPlay();
            }

            // Assign new slide position
            this.RS.$currentSlide = this.RS.$nextSlide;

            // Remove RS obj inProgress flag (i.e. allow new Transition to be instantiated)
            this.RS.inProgress = false;

        }

        ,fade: function(){
            var that = this;

            //If CSS Transition are supported by browser7
            if(this.MS.cssTransitions){
                //Setup steps
                this.setup = function(){
                    //Set event listener to next slide elem
                    that.listenTo = that.MS.$currentSlide;

                    that.MS.$currentSlide.css('transition', 'opacity' + that.MS.settings.transitionDuration + 'ms linear');
                };

                //Execution stops
                this.execute = function () {
                    //Display next slide over current slide
                    that.MS.$currentSlide.css('opacity', 0);
                }
            }else{
                this.execute = function () {
                    that.RS.$currentSlide.animate({'opacity' : 0}, that.MS.settings.transitionDuration, function(){
                        //Reset steps
                        that.after();
                    });
                }
            }

            this.before($.proxy(this.execute, this));
        }
    };

    //Obj to check whether browser capabilities
    var testBrowser = {
        // Browser vendor CSS prefixes
        browserVendors: ['', '-webkit-', '-moz-', '-ms-', '-o-', '-khtml-']

        // Browser vendor DOM prefixes
        ,domPrefixes: ['', 'Webkit', 'Moz', 'ms', 'O', 'Khtml']

        // Method to iterate over a property (using all DOM prefixes)
        // Returns true if prop is recognised by browser (else returns false)
        ,testDom: function (prop) {
            var i = this.domPrefixes.length;

            while (i--) {
                if (typeof document.body.style[this.domPrefixes[i] + prop] !== 'undefined') {
                    return true;
                }
            }

            return false;
        }

        ,cssTransitions: function () {
            // Use Modernizr if available & implements csstransitions test
            if (typeof window.Modernizr !== 'undefined' && Modernizr.csstransitions !== 'undefined') {
                return Modernizr.csstransitions;
            }

            // Use testDom method to check prop (returns bool)
            return this.testDom('Transition');
        }

        ,cssTransforms3d: function () {
            // Use Modernizr if available & implements csstransforms3d test
            if (typeof window.Modernizr !== 'undefined' && Modernizr.csstransforms3d !== 'undefined') {
                return Modernizr.csstransforms3d;
            }

            // Check for vendor-less prop
            if (typeof document.body.style['perspectiveProperty'] !== 'undefined') {
                return true;
            }

            // Use testDom method to check prop (returns bool)
            return this.testDom('Perspective');
        }
    };

    $.fn['mosesSlider'] = function (settings) {
        return this.each(function () {
            // Check if already instantiated on this elem
            if (!$.data(this, 'mosesSlider')) {
                // Instantiate & store elem + string
                $.data(this, 'mosesSlider', new RS(this, settings));
            }
        });
    }
})(window.jQuery, window, window.document);
/**
 * Created by olajuwon on 11/28/14.
 */

function Login(){
    this.submit = function(data){
        $('#log-success').addClass('hidden');
        $('#log-error').addClass('hidden');

        //show the loader
        $('.loading').removeClass('hidden');

        $.ajax({
            type: "POST",
            url : 'user_login',
            data : data,
            dataType: 'json'

        }).success(function(response){
                if(!response.status){
                    if(response.error_code == -1){
                        //invalid access
                        window.location.assign('illegal');
                    }else{
//                        console.log('response');
                        $('#log-error').removeClass('hidden');
                        $('.loading').addClass('hidden');
                        $('#log-error').empty().html(response.msg);

                    }
                }else{
                    console.log(response);
//                   registration successfull
                    $('#log-success').removeClass('hidden');
                    $('#log-success').html("Login successful");
//                    $('#log_form').trigger('reset');
                    $('.loading').addClass('hidden');
                    console.log(response);
//                    setTimeout(function(){
//                        window.location.assign(response.url);
//                    }, 1000)
                }
            }).fail(function(response){
                console.log('fail')
            });
        $('html,body').animate({scrollTop: 0}, 500);
    }
}
var logger  = new Login();
$('document').ready(function(){
//    $('#login_form_data').on('submit', function(e){
//        e.preventDefault();
//        var data = $('#login_form_data').serialize();
//        console.log(data);
//        logger.submit(data);
//
//
//    })
});
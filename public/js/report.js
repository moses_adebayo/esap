/**
 * Created by user on 9/29/14.
 */

function doTable(){
    $('.table').DataTable( {
        dom: 'T<"clear">lfrtip',
        iDisplayLength: 25,
        tableTools: {
            "aButtons": [
                "copy",
                "csv",
                "xls",
                {
                    "sExtends": "pdf",
                    "sPdfOrientation": "landscape",
                    "sPdfMessage": "e-SAP Report"
                },
                {
                    "sExtends" : "xls",
                    "sPdfOrientation": "landscape",
                    "sPdfMessage": "e-SAP Report"
                },
                {
                    "sExtends" : "csv",
                    "sPdfOrientation": "landscape",
                    "sPdfMessage": "e-SAP Report"
                },
                {
                    "sExtends": "print",
                    "sMessage": "e-SAP REPORT."
                }
            ]
        }
    }
    );

   printTable();
}

function printTable(){
    $('.DTTT_button_print').click(function(){
        setTimeout(function(){
            window.print();
            location.reload();
        }, 500);
    });
}
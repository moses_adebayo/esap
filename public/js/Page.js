/**
 * Created by olajuwon on 10/31/14.
 */

function PageActions(){
    this.showIdElement= function(id){
        $('#'+id).removeClass('hidden');
    }
    this.hideIdElement= function(id){
        $('#'+id).addClass('hidden');
    }
    this.showClassElement= function(id){
        $('.'+id).removeClass('hidden');
    }
    this.hideClassElement= function(class_name){
        $('.'+class_name).addClass('hidden');
    }
    this.resetInterval = function(id, dur, callback){
        clearInterval(id);
        interval_id = setInterval(callback, dur);

    }
    this.ajaxSubmit = function(url, data){
        $.ajax({
            type: "POST",
            url : url,
            data : data,
            dataType: 'json'
        }).success(function(response){
                return response;
            }).fail(function(response){
                return response;
            });
    }
}

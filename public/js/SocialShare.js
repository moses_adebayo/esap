/**
 * Created by olajuwon on 1/31/2015.
 */

(function ($){
    $.fn.socialShare = function(action, url){
        if(action === "facebook"){
            t=document.title;
            window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(url)+'&t='+encodeURIComponent(t),
                'Share on Facebook', 'toolbar=0,status=0,width=626,height=436');
        }else if(action === "twitter"){
            window.open('https://twitter.com/home?status=' + url, 'Share on Twitter','toolbar=0,status=0,width=626,height=436');
        }else if(action === "googlePlus"){
            window.open('https://plus.google.com/share?url=' + url, 'Share on Google+','toolbar=0,status=0,width=626,height=436');
        }
    }
})(jQuery);


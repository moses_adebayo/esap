var pager = new PageActions();
var course = new Courses();

var total_cost = 0;
var chosen_courses = {};
var deactivate = false;
function Courses(){
    this.show_page  = function(page){
        pager.hideClassElement('courses-list');
        pager.showIdElement(page.id);
    };

    this.selectCourse = function(course_selected){
        //var course_id = course_selected.id;
        //console.log(course_id);
        //console.log(course_selected);
        var course_details = $(course_selected).attr("data-options");
        var info = JSON.parse(course_details);
        //console.log(info.name);
        $('.course#' + info.course_id).addClass('course_selected').find("input:checkbox").prop("checked", true);
        total_cost += parseInt(info.cost);
        chosen_courses[info.course_id] = info.name;
        //if(!course_selected){
        //    $('.course#' + course_id).addClass('course_selected').find("input:checkbox").prop("checked", true);
        //    total_cost += parseInt(info.cost);
        //    chosen_courses[info.course_id] = info.name;
        //}else if(course_selected){
        //    console.log("uncheck");
        //    $('.course#' + course_id).removeClass('course_selected').find("input:checkbox").prop("checked", false);
        //    total_cost -= parseInt(info.cost);
        //    delete chosen_courses[info.course_id];
        //}else{
        //    console.log('unknown error');
        //}


        this.displayCourseSelectedList();

    };

    this.removeCourse = function(course_selected){
        var course_details = $(course_selected).attr("data-options");
        var info = JSON.parse(course_details);
            total_cost -= parseInt(info.cost);

        delete chosen_courses[info.course_id];
        ////console.log(course);
        //$("#selected_course_list li").each(function () {
        //    console.log(this);
        //});
        //$('.course#' + course_id).removeClass('course_selected').find("input:checkbox").prop("checked", false);
        //total_cost -= parseInt(info.cost);
        //delete chosen_courses[info.course_id];
        //
        ////remove class
        $(course_selected).removeClass("selected_course");

        this.displayCourseSelectedList();

    };

    this.displayCourseSelectedList = function () {

        //cost
        $('#cost').html(total_cost);

        var html ="";

        var courses = Object.keys(chosen_courses);
        courses.forEach(function(course_id){
            html += "<li data-course-id="+ course_id +">"+ chosen_courses[course_id] +"</li>";
        });
        $('#courses_selected').html("<ol id='selected_course_list'>" + html + "</ol>");

        this.showApplyButton();

    };

    this.showApplyButton = function(){
        if(parseInt($('#cost').html()) > 0){
            $('#apply').removeClass('hidden');
        }else{
            $('#apply').addClass('hidden');
            $('#courses_selected').html("<div class='alert alert-danger'>No course selected yet</div> ")
        }
    };

    this.apply = function(id){
        $.ajax({
            type: "POST",
            url : "apply_training",
            data : {cost : total_cost, courses : chosen_courses, training_id : id},
            dataType: 'json'
        }).success(function(response){
//                console.log(response);
            if(response.status){
                $('#app-error').addClass('hidden');
                $('#app-success').removeClass('hidden');
                $('#app-success').html(response.msg + "<br/><strong><a href='" + window.location.href + "'>Print slip</a></strong>");
                //deactivate page
                deactivate = true;
                $('.loading').addClass('hidden');
            }else if(!response.status){
                $('#app-success').addClass('hidden');
                $('#app-error').removeClass('hidden');
                $('#app-error').html(response.msg);
                //deactivate page
                $('.loading').addClass('hidden');
            }

        }).fail(function(response){
            console.log("fail");
        });
    };

    this.print_slip = function(id){
        $.ajax({
            type: "POST",
            url : "apply/print",
            data : {training_id : id},
            dataType: 'json'
        }).success(function(response){
//                console.log(response);
            if(response.status){
                $('#app-error').addClass('hidden');
                $('#app-success').removeClass('hidden');
                $('#app-success').html(response.msg);
                $('.loading').addClass('hidden');
            }else if(!response.status){
                $('#app-success').addClass('hidden');
                $('#app-error').removeClass('hidden');
                $('#app-error').html(response.msg);
                $('.loading').addClass('hidden');
            }

        }).fail(function(response){
            console.log("fail");
        });
    }
}

$(document).ready(function() {
    $('.course_page').click(function(){
        $('.btn').removeClass('selected-page');
        $('.btn#' + this.id).addClass('selected-page');
        course.show_page(this);
        $('html,body').animate({scrollTop: 0}, 1000);
    });

    //select course
    $('.select_course').unbind("click").bind("click", function(e){
        //e.preventDefault();
        if(!deactivate) {
            if ($(this).hasClass('selected_course')) {
                course.removeCourse(this);
            } else {
                $(this).addClass("selected_course");
                //var course_details = $(this).attr("data-options");
                course.selectCourse(this);
            }
        }
    });

    $('#apply').on('click', function(){
        //show the loader
        if(!deactivate){
            var auth = confirm("Are you sure have review the courses chosen");
            if(auth){
                $('.loading').removeClass('hidden');
                course.apply($('#apply').attr("data-training_id"));
            }
        }

    })

});


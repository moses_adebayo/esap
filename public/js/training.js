/**
 * Created by olajuwon on 1/31/2015.
 */

$(function(){
   $('.facebook').on('click',  function(){
       var url = $(this).attr('data-url');
       $.fn.socialShare('facebook', url);
   }) ;
    $('.twitter').on('click',  function(){
       var url = $(this).attr('data-url');
       $.fn.socialShare('twitter', url);
   }) ;
    $('.google-plus').on('click',  function(){
           var url = $(this).attr('data-url');
           $.fn.socialShare('googlePlus', url);
    }) ;

});


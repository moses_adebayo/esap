/**
 * Created by olajuwon on 11/28/14.
 */

function Account(){

    this.byDeposit = function(data){
        $('#account-success').addClass('hidden');
        $('#account-error').addClass('hidden');
        //show the loader
        $('.loading').removeClass('hidden');

        $.ajax({
            type: "POST",
            url : 'recharge_by_deposit',
            data : data,
            dataType: 'json'

        }).success(function(response){
            console.log(response);
                if(!response.status){
//                        console.log('response');
                        $('.loading').addClass('hidden');
                        $('#account-error').empty().removeClass('hidden').html(response.msg + "<p class='small text-center'>(ensure you type the correct teller number/amount)</p>");
                }else{
                    $('.loading').addClass('hidden');
                    $('#account-success').removeClass('hidden').html('Account Recharged');
                    setTimeout(function(){
                        window.location.reload();
                    }, 2000);
                }
            }).fail(function(response){
                console.log(response.responseText);
            });
    };
    this.byVoucher = function(data){
        $('#account-success').addClass('hidden');
        $('#account-error').addClass('hidden');
        //show the loader
        $('.loading').removeClass('hidden');

        $.ajax({
            type: "POST",
            url : 'load_voucher',
            data : data,
            dataType: 'json'

        }).success(function(response){
            console.log(response);
                if(!response.status){
//                        console.log('response');
                        $('.loading').addClass('hidden');
                        $('#account-error').empty().removeClass('hidden').html(response.msg);
                }else{
                    $('.loading').addClass('hidden');
                    $('#account-success').empty().removeClass('hidden').html('Account Recharged');
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                }
            }).fail(function(response){
                console.log(response.responseText);
            });
    };
    this.confirmDeposit = function(data){
        $('#account-success').addClass('hidden');
        $('#account-error').addClass('hidden');
        //show the loader
        $('.loading').removeClass('hidden');

        $.ajax({
            type: "POST",
            url : 'teller_form_process',
            data : data,
            dataType: 'json'

        }).success(function(response){
            console.log(response);
                if(!response.status){
//                        console.log('response');
                        $('.loading').addClass('hidden');
                        $('#account-error').empty().removeClass('hidden').html(response.msg);
                }else{
                    $('.loading').addClass('hidden');
                    $('#account-success').empty().removeClass('hidden').html('Teller Confirmed');
                }
            }).fail(function(response){
                console.log(response.responseText);
            });
    };
    this.hideResponse = function(){
        $('#account-error').empty().addClass('hidden')
    }
}
var account  = new Account();
$('document').ready(function(){
    $('#form_deposit').on('submit', function(e){
        e.preventDefault();
        var data = $('#form_deposit').serialize();
        account.byDeposit(data);

    });
    $('#form_voucher').on('submit', function(e){
        e.preventDefault();
        var data = $('#form_voucher').serialize();
        account.byVoucher(data);

    });
    $('#confirm_deposit').on('submit', function(e){
        e.preventDefault();
        var data = $('#confirm_deposit').serialize();
        account.confirmDeposit(data);

    });
    $('.nav-tabs li').click(function(){
        account.hideResponse();
    });
});
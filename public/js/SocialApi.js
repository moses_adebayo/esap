/**
 * Created by olajuwon on 2/25/2015.
 */
function processUserRegWithSocial(user){
    $('#reg-error').empty();
    $('#reg-success').empty();
    $('.s-loading').removeClass('hidden');

    $.ajax({
        type: "POST",
        url : 'register_with_social',
        data : user,
        dataType: 'json'
    }).success(function(response){
        if(!response.status){
            $('#reg-error').empty().html(response.msg);
            $('.s-loading').addClass('hidden');
        }else if(response.status){
            $('#reg-success').empty().html(response.msg);
            $('.s-loading').addClass('hidden');
            setTimeout(function(){
                window.location.href = response.url;
            }, 2000);
        }
//
    }).fail(function(response){
        //console.log(response.responseText);
    });
}

function processUserLoginWithSocial(user){
    $('#reg-error').empty();
    $('#reg-success').empty();
    $('.s-loading').removeClass('hidden');

    $.ajax({
        type: "POST",
        url : 'user_login_wSocial',
        data : user,
        dataType: 'json'
    }).success(function(response){
        //console.log(response);
        if(!response.status){
            $('#reg-error').empty().html(response.msg);
            $('.s-loading').addClass('hidden');
        }else if(response.status){
            $('#reg-success').empty().html(response.msg);
            setTimeout(function(){
                window.location.href = response.url;
                $('.s-loading').addClass('hidden');
            }, 2000);
        }
//
    }).fail(function(response){
        //console.log(response.responseText);
    });
}
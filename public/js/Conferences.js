var conf = new Conference();
function Conference(){

    this.apply = function(id){
        $.ajax({
            type: "POST",
            url : "apply_conference",
            data : {conference_id : id},
            dataType: 'json'
        }).success(function(response){
                console.log(response);
                if(response.status){
                    $('#app-error').addClass('hidden');
                    $('#app-success').removeClass('hidden');
                    $('#app-success').html(response.msg);
                    $('#apply').addClass('hidden');
                    $('.loading').addClass('hidden');
                }else if(!response.status){
                    $('#app-success').addClass('hidden');
                    $('#app-error').removeClass('hidden');
                    $('#apply').addClass('hidden');
                    $('#app-error').html(response.msg);
                    //deactivate page
                    $('.loading').addClass('hidden');
                }

            }).fail(function(response){
                console.log("fail");
            });
    }

    this.print_slip = function(id){
        $.ajax({
            type: "POST",
            url : "apply/print",
            data : {training_id : id},
            dataType: 'json'
        }).success(function(response){
//                console.log(response);
                if(response.status){
                    $('#app-error').addClass('hidden');
                    $('#app-success').removeClass('hidden');
                    $('#app-success').html(response.msg);
                    $('.loading').addClass('hidden');
                }else if(!response.status){
                    $('#app-success').addClass('hidden');
                    $('#app-error').removeClass('hidden');
                    $('#app-error').html(response.msg);
                    $('.loading').addClass('hidden');
                }

            }).fail(function(response){
                console.log("fail");
            });
    }



}

$(document).ready(function() {

    $('#apply').on('click', function(){
        //show the loader
                $('.loading').removeClass('hidden');
        conf.apply($('#apply').attr("data-conference-id"));


    })

});





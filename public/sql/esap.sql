-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 07, 2015 at 01:26 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `esap`
--

-- --------------------------------------------------------

--
-- Table structure for table `application`
--

CREATE TABLE IF NOT EXISTS `application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `training_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `reg_num` varchar(128) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `app_cost` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `application`
--

INSERT INTO `application` (`id`, `training_id`, `user_id`, `reg_num`, `date_created`, `date_modified`, `app_cost`) VALUES
(11, 2, 2, 'EDE-ADE-11', '2014-12-06 02:35:15', '2014-12-06 02:35:15', 1450),
(12, 4, 6, 'XX-OLA-12', '2014-12-18 12:59:02', '2014-12-18 12:59:02', 2900),
(13, 2, 7, 'EDE-ABI-13', '2014-12-18 18:45:31', '2014-12-18 18:45:31', 1450),
(14, 4, 7, 'XX-ABI-14', '2014-12-18 18:52:29', '2014-12-18 18:52:29', 1450),
(15, 1, 7, 'OAU-ABI-15', '2014-12-18 18:54:21', '2014-12-18 18:54:21', 1450),
(16, 1, 6, 'OAU-OLA-16', '2014-12-18 18:56:31', '2014-12-18 18:56:31', 1450),
(17, 4, 3, 'XX-ADE-17', '2014-12-18 19:37:36', '2014-12-18 19:37:36', 1450),
(18, 2, 6, 'eSAP/EDE/OLA/18', '2014-12-20 14:51:57', '2014-12-20 14:51:57', 1450),
(19, 5, 6, 'eSAP/IBA1/OLA/19', '2014-12-20 19:44:50', '2014-12-20 19:44:50', 2900),
(20, 2, 3, 'eSAP/EDE/ADE/20', '2014-12-21 16:47:50', '2014-12-21 16:47:50', 1450),
(21, 5, 8, 'eSAP/IBA1/MOS/21', '2014-12-22 11:21:33', '2014-12-22 11:21:33', 1450);

-- --------------------------------------------------------

--
-- Table structure for table `application_courses`
--

CREATE TABLE IF NOT EXISTS `application_courses` (
  `app_course_id` int(11) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`app_course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `application_courses`
--

INSERT INTO `application_courses` (`app_course_id`, `application_id`, `course_id`, `user_id`) VALUES
(10, 10, 1, 2),
(11, 11, 1, 2),
(12, 12, 1, 6),
(13, 12, 2, 6),
(14, 13, 1, 7),
(15, 14, 1, 7),
(16, 15, 1, 7),
(18, 17, 1, 3),
(42, 16, 5, 6),
(46, 18, 1, 6),
(47, 19, 1, 6),
(48, 19, 2, 6),
(49, 16, 1, 6),
(50, 20, 1, 3),
(53, 21, 2, 8);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_name` varchar(128) DEFAULT NULL,
  `reg_cost` int(11) DEFAULT '1450',
  `course_desc` text,
  `date_created` datetime DEFAULT NULL,
  `date_modifies` datetime DEFAULT NULL,
  `course_code` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`course_id`, `course_name`, `reg_cost`, `course_desc`, `date_created`, `date_modifies`, `course_code`) VALUES
(1, 'Accounting PeachTree', 1450, 'As a very lucrative course and a highly demanded knowledge area, this programme is designed to assist students in understanding how to computerize accounting information. It will give students the ability to facilitate greater efficiency in management and financial reporting.', '2014-12-05 18:24:06', '2014-12-05 18:24:09', 'ESAP'),
(2, 'French Language', 1450, 'Knowledge of a second language is essential in over 60 occupations.When you know French, you could become a French teacher, an interpreter or a translator.More than 200 million people speak French on the five continents. French is the second most widely learned foreign language after English, and the ninth most widely spoken language in the world. French is also the only language, alongside English, that is taught in every country in the world.\r\n\r\n', '2014-12-05 21:16:49', '2014-12-05 21:16:49', 'ESAP'),
(3, 'German Language', 1450, 'People who speak a second language have better skills in their native language as well, and are also better critical thinkers who are superior problem solvers.', '2014-12-05 21:17:34', '2014-12-05 21:17:34', 'ESAP'),
(4, 'AutoCAD 2D & 3D Modelling', 1450, 'Individuals who work in or are currently pursuing careers in the architecture, mechanical or engineering fields will discover many benefits of using AutoCAD.AutoCAD is a computer-aided drafting (CAD) software application that enables drafters, architects, engineers, and other professionals to create two-dimensional (2D) and three-dimensional (3D) models of mesh and solid surfaces. Prior to computer-aided drafting, manual hand drafting tools such as drafting boards and pencils, inking pens, parallel rules, compasses, and triangles only offered a subset of what can now be done with programs such as AutoCAD.', '2014-12-05 21:18:33', '2014-12-05 21:18:33', 'ESAP'),
(5, 'SPSS Statistical Package', 1450, 'SPSS means “Statistical Package for the Social Sciences”. SPSS is a widely used program for statistical analysis in social science. It is also used by market researchers, health researchers, survey companies, government, education researchers, marketing organizations, data miners, and others.', '2014-12-05 21:19:06', '2014-12-05 21:19:06', 'ESAP'),
(6, 'Java Enterprise Programming Language', 1450, 'Java programming is a lot easier than it looks. There are thousands of programmers who have used their Java skills to get high-paying jobs in software development, Internet programming, and e-commerce. The last thing any of them wants is for the boss to know that anyone with persistence and a little free time can learn the language, the most popular programming language in use today.', '2014-12-05 21:19:39', '2014-12-05 21:19:39', 'ESAP'),
(7, 'Project Management Professional (PMP Certification Course)', 1450, 'Project management is one of the essential processes of an organization for the simple reason that it answers a lot of your questions and adds order to the company. With this, project management training is vital to ensure that you have the right skills and knowledge when it comes to managing a project.', '2014-12-05 21:19:56', '2014-12-05 21:19:56', 'ESAP'),
(8, 'Networking and Networks Administration', 1450, 'Industries across the career spectrum and around the world depend on computer networking to keep employees connected and business flowing. Networking skills give you an edge and an opportunity to make a career in almost any sector you can imagine: financial services, education, transportation, manufacturing, oil and gas, mining and minerals, technology, government, hospitality, health care, retail… you name it. If you have an interest in a particular field, technology is probably part of it.', '2014-12-05 21:20:38', '2014-12-05 21:20:38', 'ESAP'),
(9, 'Graphics Design', 1450, 'Graphic designers are visual communicators who design and develop print and electronic media, such as magazines, television graphics, logos and websites. They may be employed by advertising firms, design companies, publishers and other businesses that need design professionals. This career is results-oriented, and graphic designers are concerned with providing final products that meet clients needs.', '2014-12-05 21:21:28', '2014-12-05 21:21:28', 'ESAP'),
(10, 'Website Design and Development', 1450, 'It''s incredible to think how much the world has changed since 1991, when the World Wide Web first started. The web has taken over our lives and it''s showing no sign of stopping. Who knows where the web will take us in the future? Perhaps in another 10 years every person will have their own website. If one thing''s certain, it''s that a website is a very valuable thing to have these days.', '2014-12-05 21:22:15', '2014-12-05 21:22:15', 'ESAP'),
(11, 'Android Mobile Apps Development', 1450, 'Are you a tech-savvy person and do you love technologies? This insight will make you travel and explore the world of Android as to why Android programming should be learnt. Android is the most popular mobile phone technology in use today. In order to learn Android development one needs to be trained in its fundamentals and methodologies and hence proper training is necessary.', '2014-12-05 21:22:41', '2014-12-05 21:22:41', 'ESAP'),
(12, 'Hardware Engineering', 1450, 'Computers are an integral part of professional environments in the 21st century and not knowing how to do basic troubleshooting can be a drawback for job seekers. In addition, specialized professionals in computer technology, who can design and program computers and fix complicated problems, are imperative for any organization relying heavily on computers. Therefore, basic or advanced knowledge of computer technology can have a number of advantages for your career.', '2014-12-05 21:22:52', '2014-12-05 21:22:52', 'ESAP'),
(13, 'Digital Films and Video Editing', 1450, 'Without film/video editors, movies would last for days, television shows would be completely incoherent, and music videos would look like they were filmed in your parents’ garage. You can’t just take the raw footage from a film shoot, mash it all together and then release it upon the world. Indeed, skilled and experienced film/video editors are required during every single post-production process. Film/video editors use state-of-the-art video editing software, such as Avid Symphony and Montage Extreme, to transform the ingredients of a film (i.e. the sound effects, the CGI, the dialogue and the action) into a refined, tasty treat that the target audience can devour on the screen.', '2014-12-05 21:23:38', '2014-12-05 21:23:38', 'ESAP'),
(14, 'Human Resources Business Professional (HRBP) - HR Certification Course', 1450, 'In the HR business partner model, the human resource department participates in strategic planning to help the business meet present and future goals. Rather than concentrating solely on HR duties such as benefits, payroll and employee relations, HR departments seek to add value to the company by overseeing recruiting, training, advancement and placement of new and current employees.\r\n\r\n', '2014-12-05 21:24:05', '2014-12-05 21:24:05', 'ESAP'),
(15, 'CCTV Technology and Installation', 1450, 'CCTV plays a major role in the security systems in place today and has developed very fast in the last few years.The courses can benefit people either working or intending to work as a: - Security Manager, Security Supervisor, Security Guard, CCTV Control Room Employee, CCTV Sales Person, CCTV Protected Premises Owner/Manager, Potential CCTV Purchaser/Advisor, CCTV System Maintenance Employee, CCTV Installer, CCTV Specifier.', '2014-12-05 21:24:30', '2014-12-05 21:24:30', 'ESAP'),
(16, 'DSTV Technology and Installation', 1450, 'Dish TV has revolutionized the world of satellite television. Through Dish TV in American homes, people can now watch television programs from all across the globe. Be it any sports, you can catch up with all the live action that is available through sports channels on the satellite television network. There are a host of movie channels where you catch up with your favorite genre of movies. No matter whether you are hooked on to the news or are addicted to a particular sitcom, with Dish TV you never have to lose out on your daily dose of entertainment.', '2014-12-05 21:24:58', '2014-12-05 21:24:58', 'ESAP'),
(17, 'Microsoft (MS) Project', 1450, 'Microsoft Project Server 2010 is built on Microsoft SharePoint® Server 2010, and brings together powerful business collaboration platform services with structured execution capabilities to provide flexible work management solutions. Project Server 2010 unifies project and portfolio management to help organizations align resources and investments with strategic priorities, gain control across all types of work, and visualize performance by using powerful dashboards.', '2014-12-05 21:25:29', '2014-12-05 21:25:29', 'ESAP'),
(18, 'Beads, Wireworks and Gele Tying', 1450, 'It was a vocation she ventured into by accident, but now she has managed to turn it into a very successful and highly rewarding enterprise. YemisiOludiran, a graduate of Lagos State University (LASU) went into the business of bead making and wireworks about three years ago, while the Academic Staff Union of Universities (ASUU) embarked on one of its famous industrial actions. ', '2014-12-05 21:26:03', '2014-12-05 21:26:03', 'ESAP'),
(19, 'Cake Designs and Decorations', 1450, 'A cake decorator is a culinary professional that is responsible for turning a simple cake into a mountain of confectionary perfection. Cake decorators use their creative flair to make cake for a client''s wedding day. Having baking skills and a background in the culinary arts benefits aspiring wedding cake decorators. They work closely with clients to gather ideas, so they need to be able to visualize the finished product.A cake decorator designs, bakes, and garnishes wedding cakes for customers.', '2014-12-05 21:26:30', '2014-12-05 21:26:30', 'ESAP'),
(20, 'Events Planning and Management', 1450, 'An event planner organizes and facilitates special events, large gatherings, and functions that are often of a celebratory nature. The events planner may be employed by a large corporation that frequently holds such functions for employees or clients. It is often common that businesses in the hospitality industry also have their own event planner on staff to help individuals and businesses execute these special gatherings.', '2014-12-05 21:26:47', '2014-12-05 21:26:47', 'ESAP');

-- --------------------------------------------------------

--
-- Table structure for table `identification`
--

CREATE TABLE IF NOT EXISTS `identification` (
  `id_key` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `surname` varchar(128) DEFAULT NULL,
  `first_name` varchar(128) DEFAULT NULL,
  `phone_num` varchar(128) DEFAULT NULL,
  `country` varchar(128) DEFAULT NULL,
  `address` tinytext,
  `institution_type` varchar(128) DEFAULT NULL,
  `institution_name` varchar(128) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `course_study` varchar(128) DEFAULT NULL,
  `lga` varchar(128) DEFAULT NULL,
  `state` varchar(128) DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  PRIMARY KEY (`id_key`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `identification`
--

INSERT INTO `identification` (`id_key`, `user_id`, `surname`, `first_name`, `phone_num`, `country`, `address`, `institution_type`, `institution_name`, `level`, `course_study`, `lga`, `state`, `gender`) VALUES
(1, 2, 'Moses', 'Adebayo', '08162886288', 'nig', '23b ezeagu lane, ajegunle lagos', 'University', ' Lead City University ', 100, 'csc', ' Konduga ', 'Borno', 'm'),
(2, 3, 'Akanbi', 'Adedoyin', '08162886288', 'nig', '23b ezeagu lane, ajegunle lagos', 'University', ' Obafemi Awolowo University ', 500, 'csc', ' Mushin ', 'Lagos', 'm'),
(3, 4, 'Akeem', 'Adetunji', '08162886288', 'nig', '23 ligali street', 'University', ' Babcock University ', 100, 'csc', ' Ikole ', 'Ekiti', 'm'),
(4, 5, 'Awoyelu', 'Tolu', '08033126346', 'nig', 'ICT Centre', 'University', ' Osun State University ', 600, 'csc', ' Ife Central ', 'Osun', 'f'),
(5, 6, 'Fakunle', 'Oladeoye', '08162886288', 'nig', '23b ezeagu lane, ajegunle lagos', 'University', ' Babcock University ', 400, 'csc', ' Lagos Mainland ', 'Lagos', 'm'),
(6, 7, 'Ogunwuyi', 'Abisola', '08033126346', 'nig', '23b ezeagu lane, ajegunle lagos', 'College of Education', ' Zuba Federal College of Education ', 400, 'Computer science', ' Lagos Mainland ', 'Lagos', 'f'),
(7, 8, 'Adetunji', 'Moses', '08162886288', 'nig', '23b ezeagu lane, ajegunle lagos', 'University', 'Anambra State University ', 400, 'Computer Science', ' Lagos Island ', 'Lagos', 'm');

-- --------------------------------------------------------

--
-- Table structure for table `information`
--

CREATE TABLE IF NOT EXISTS `information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `description` text,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `information`
--

INSERT INTO `information` (`id`, `title`, `description`, `date_created`, `date_modified`) VALUES
(1, 'ESAP Taraba Season', 'eSAP coming up on the 28th of nov. 2014', '2014-12-05 17:24:08', '2014-12-05 17:24:08'),
(3, 'Beg or be Begged?', 'In this world of today, if you will get ahead in your life and career, academic certificate alone cannot get the job done; otherwise you would have been deceived that good grades from school will earn you a good job when you graduate. If that’s what you have in mind, it’s a big lie!', '2014-12-05 21:46:01', '2014-12-05 21:46:01'),
(6, 'Urgent!!! Hot', 'This is the latest info for you and is very urgent', '2014-12-22 08:09:45', '2014-12-22 08:09:45');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE IF NOT EXISTS `testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `message` text,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `message`, `date_created`, `date_modified`) VALUES
(1, 'Ogunkoya Olanrewaju.', 'This training(SPSS) helped me in analyzing my project data, which I never, ever thought I could do by myself.Mr. Idowu (An e-SAP’s Instructor) is a very articulate and patient teacher in the sense that he doesn’t seem to get a bit frustrated even when students ask questions about what he has explained over and over!', '2014-12-05 17:29:41', '2014-12-05 17:29:41'),
(2, 'Adetunji Ademola E.', 'In short, My expectations were met!', '2014-12-05 21:27:41', '2014-12-05 21:27:41'),
(3, 'Giwa Babatunde Olagoke', 'I see this as encouraging: Mr Mathew is a nice, hardworking and a guru, the best I have seen!', '2014-12-05 21:27:57', '2014-12-05 21:27:57'),
(4, 'Ojedokun Adedolapo Israel', 'The programme is reasonably affordable!', '2014-12-05 21:28:21', '2014-12-05 21:28:21'),
(5, 'Asebiode Tomilayo', 'The short contact was good and impacting!', '2014-12-05 21:28:37', '2014-12-05 21:28:37'),
(6, 'Idowu Tolulope A', 'The amount paid is nothing compared to the lessons learned!', '2014-12-05 21:29:20', '2014-12-05 21:29:20'),
(7, 'Abudu Mojisola', 'I was able to gain something tangible', '2014-12-05 21:29:56', '2014-12-05 21:29:56'),
(8, 'Opeloye Onaopemipo Nafisat', 'Good, wonderful; it’s worth it!', '2014-12-05 21:30:21', '2014-12-05 21:30:21'),
(9, 'Fadiora Jesumbo T.', 'More of trainings like this is needed!', '2014-12-05 21:30:42', '2014-12-05 21:30:42'),
(10, 'Ogunwemimo Hassan I.', 'A great foundation for personal advancement', '2014-12-05 21:31:12', '2014-12-05 21:31:12'),
(11, 'Adekanye Blessing', 'Thanks for helping me become valuable by adding skills! Looking forward to the Season Two. Thumbs up!!', '2014-12-05 21:31:34', '2014-12-05 21:31:34'),
(12, 'Idowu Abosede', 'Thanks for this programme. I am happy I participated. Please give many more people the chance to benefit also', '2014-12-05 21:31:49', '2014-12-05 21:31:49'),
(13, 'Elemosho Toluwani', 'An affordable, out-of-the-box, impactful concept!', '2014-12-05 21:32:03', '2014-12-05 21:32:03'),
(14, 'Olaleye Olusola Francis', 'You people are competent trainers!', '2014-12-05 21:33:12', '2014-12-05 21:33:12'),
(15, 'Bamidele Oluwasola Emmanuel', 'Now I can see myself achieving my dream of becoming a professional project manager', '2014-12-05 21:33:37', '2014-12-05 21:33:37'),
(16, 'Olawoye Gbenga Daniel', 'e-SAP! I am waiting for more seasons!!', '2014-12-05 21:34:08', '2014-12-05 21:34:08'),
(17, 'Olajide Oluwasanmi O', 'The lecturer is superb! At least, as of today, I can call myself a data analyst, without any prior knowledge about SPSS before I came for e-SAP! Please I will like to come for the next season; I need to add many more skill to the one I have had in this season.', '2014-12-05 21:34:31', '2014-12-05 21:34:31'),
(18, 'Are Olakunle Felix', 'My instructor is very good, committed, gives his best and apologizes for any digression. As one of the facilitators, he has very good leadership skill: 93%!', '2014-12-05 21:34:55', '2014-12-05 21:34:55'),
(19, 'Saliu Iswat Adenike', 'My instructor trains well, he is good at what he does. He doesn’t only lecture, he teaches too, He explains thing very well! Sometimes, I did think this hardware engineering would be tough, but with his help and God’s help, I found it quite simple and interesting!', '2014-12-05 21:35:18', '2014-12-05 21:35:18'),
(20, 'Bobmanel Borma Barbra', 'This is one of the best opportunities for one’s career leverage', '2014-12-05 21:35:40', '2014-12-05 21:35:40'),
(21, 'Adaji Ladi', 'Adequate teaching aid, conducive environment!', '2014-12-05 21:36:00', '2014-12-05 21:36:00'),
(22, 'Ezue Alex-Jnr. E', 'It has been a motivational and encouraging experience', '2014-12-05 21:36:33', '2014-12-05 21:36:33'),
(23, 'Kolade Oluwafunmike T', 'The training is highly affordable, was brought nearer to students, fueling in us the desire to aspire for other qualifications beyond the B. Sc. Certificate.', '2014-12-05 21:36:59', '2014-12-05 21:36:59'),
(24, 'Otun Adedamola E', 'The training was enlightening and gave me more than expected – you guys are awesome! Thank you for bringing this programme at such an affordable price. Thanks a lot.', '2014-12-05 21:37:18', '2014-12-05 21:37:18'),
(25, 'Hanson Caleb', 'I see e-SAP that kept to its purpose. Its really impacting, even for personal development. Its important we tell others about it so that they too take part of it.', '2014-12-05 21:37:39', '2014-12-05 21:37:39'),
(26, 'Ogele Timothy', 'I must tell people about this because people need to begin to see beyond G.P', '2014-12-05 21:38:13', '2014-12-05 21:38:13'),
(27, 'Adebisi Ruth Itunuoluwa', 'What we learned was more than what we paid. You kept to your promises and I really enjoyed the classes!', '2014-12-05 21:38:38', '2014-12-05 21:38:38'),
(28, 'Ogunbambi Seun', 'The training is a great eye opener, and as far as I am concerned, it is really easy to take-off from here!', '2014-12-05 21:38:58', '2014-12-05 21:38:58'),
(29, 'Okezie Patience', 'For me, this training is a stepping stone unto greater heights', '2014-12-05 21:39:50', '2014-12-05 21:39:50');

-- --------------------------------------------------------

--
-- Table structure for table `training`
--

CREATE TABLE IF NOT EXISTS `training` (
  `training_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) DEFAULT NULL,
  `location` text,
  `code` varchar(128) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`training_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `training`
--

INSERT INTO `training` (`training_id`, `name`, `location`, `code`, `start_date`, `end_date`, `status`) VALUES
(1, 'eSAP OAU Season 2', 'White house', 'oau', '2014-12-05', '2014-12-12', 1),
(2, 'eSAP Ede Season 1', 'Main campus', 'ede', '2015-01-01', '2015-01-30', 1),
(4, 'xxxxx', 'zxzxzx', 'xx', '2014-12-19', '2014-12-26', 1),
(5, 'eSAP Ibadan Season 1', 'School Premises', 'iba1', '2014-12-26', '2014-12-31', 2);

-- --------------------------------------------------------

--
-- Table structure for table `training_courses`
--

CREATE TABLE IF NOT EXISTS `training_courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `training_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `training_courses`
--

INSERT INTO `training_courses` (`id`, `training_id`, `course_id`, `date_created`, `date_modified`) VALUES
(1, 2, 1, '2014-12-05 18:22:27', '2014-12-05 18:22:27'),
(2, 1, 1, '2014-12-05 21:41:54', '2014-12-05 21:41:54'),
(3, 1, 5, '2014-12-05 21:41:58', '2014-12-05 21:41:58'),
(4, 1, 6, '2014-12-05 21:42:03', '2014-12-05 21:42:03'),
(5, 1, 3, '2014-12-05 21:42:07', '2014-12-05 21:42:07'),
(6, 1, 7, '2014-12-05 21:42:11', '2014-12-05 21:42:11'),
(7, 1, 8, '2014-12-05 21:42:17', '2014-12-05 21:42:17'),
(8, 4, 1, '2014-12-18 11:33:20', '2014-12-18 11:33:20'),
(9, 4, 2, '2014-12-18 11:33:20', '2014-12-18 11:33:20'),
(10, 5, 1, '2014-12-20 19:35:32', '2014-12-20 19:35:32'),
(11, 5, 2, '2014-12-20 19:35:32', '2014-12-20 19:35:32'),
(12, 5, 3, '2014-12-20 19:35:32', '2014-12-20 19:35:32'),
(13, 5, 4, '2014-12-20 19:35:32', '2014-12-20 19:35:32'),
(14, 5, 5, '2014-12-20 19:35:32', '2014-12-20 19:35:32'),
(15, 5, 6, '2014-12-20 19:35:32', '2014-12-20 19:35:32'),
(16, 5, 8, '2014-12-20 19:35:32', '2014-12-20 19:35:32'),
(17, 5, 9, '2014-12-20 19:35:33', '2014-12-20 19:35:33'),
(18, 5, 10, '2014-12-20 19:35:33', '2014-12-20 19:35:33'),
(19, 5, 11, '2014-12-20 19:35:33', '2014-12-20 19:35:33'),
(20, 5, 12, '2014-12-20 19:35:33', '2014-12-20 19:35:33'),
(21, 5, 13, '2014-12-20 19:35:33', '2014-12-20 19:35:33'),
(22, 5, 18, '2014-12-20 19:35:33', '2014-12-20 19:35:33'),
(23, 5, 19, '2014-12-20 19:35:33', '2014-12-20 19:35:33'),
(24, 5, 20, '2014-12-20 19:35:33', '2014-12-20 19:35:33');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_type`) VALUES
(1, 1),
(2, 2),
(3, 2),
(4, 2),
(5, 2),
(6, 2),
(7, 2),
(8, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_auth`
--

CREATE TABLE IF NOT EXISTS `user_auth` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `remember_token` varchar(128) DEFAULT NULL,
  `user_type` int(11) DEFAULT '2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `user_auth`
--

INSERT INTO `user_auth` (`id`, `email`, `password`, `remember_token`, `user_type`) VALUES
(2, 'mos.adebayo@gmail.com', '$2y$10$.YCTLSAo9QszMOibhyElmemn6F6fgK3LtQba6yLClSIulEF57QSFy', 'CQ7gLVtQuzw3i8QuCmolv2F3uzckASpg2ZClh26or3MJ3pJyhX2j3E2LZZCO', 2),
(3, 'admin@esap.com', '$2y$10$QgBMIBV8Qb/b5xMjs0ai2.l/qC7gJ75CJcgVsWW.IPlujUORU76NC', 'IoDZDiMV9u5jhIOZlmDrgXgtUgMbggM6ALhZfidCTFGgmLEk7aHmnOzhH9kT', 1),
(4, 'tester1@gmail.com', '$2y$10$iHUbaFxUJVsCKGLHr9sm4e9kPfnA.C41JBK9w27pBgfX8wfjfUCZ2', 'J4ju3WrJlR1RHfaDlhFqux1fV8rK5XEMODEAZ0vlmKkSAOCXXBguP0D2vQJ7', 2),
(5, 'tolu.awoyelu@gmail.com', '$2y$10$BMK5b7mCG3MK8pu2z6Lv2OMb6rvFs4LISVvd/Ii5JUsLr.Td0UOXO', '3sovWtxq6eruDeux6XhHxnaN9JcgGBma9ZgVOLIVl5zzsYMbSQNtUNHfWZk2', 2),
(6, 'tester2@gmail.com', '$2y$10$irYkP5BXxvgGvPaik0u4ve96s6GcgsUBcwLcJgy8gu9UFX5ue1An.', 'X2wtN9YYSozFnJWICj7eFXAPnRPAb78iQXdtDWDSRMwGCtYM2dWDFZHUfleQ', 2),
(7, 'tester3@gmail.com', '$2y$10$Dlhp21M1zIRIK8izduHVrO04DZaVrVFI2az0YhhWdByLuyVT0ywLG', 'oDyXZ0vQL5dRz01eUBuXeHdcpasOI0B6GcJyQnYhGykNQ7Wg8EfsvDYlrTbx', 2),
(8, 'tester4@gmail.com', '$2y$10$gS.iaKpVKYbhyOQ1PiAbZOqyl4S/Kk7jUyk8X9qXRXiRYKojFE2Bi', NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_balance`
--

CREATE TABLE IF NOT EXISTS `user_balance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `balance` double DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `user_balance`
--

INSERT INTO `user_balance` (`id`, `user_id`, `balance`, `date_created`, `date_modified`) VALUES
(1, 2, 11150, '2014-12-05 17:52:49', '2014-12-16 07:26:53'),
(2, 3, 7350, '2014-12-15 21:32:20', '2014-12-17 21:04:03'),
(3, 4, 5900, '2014-12-16 20:39:41', '2014-12-17 14:46:10'),
(4, 5, 3000, '2014-12-17 15:48:03', '2014-12-17 14:49:06'),
(5, 6, 1450, '2014-12-18 12:55:57', '2014-12-20 18:44:36'),
(6, 7, 100, '2014-12-18 18:41:58', '2014-12-18 17:54:09'),
(7, 8, 100, '2014-12-22 11:13:00', '2014-12-22 10:16:11');

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE IF NOT EXISTS `voucher` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(128) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `price` int(11) DEFAULT '1450',
  `used_by` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_used` datetime DEFAULT NULL,
  `print_status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=241 ;

--
-- Dumping data for table `voucher`
--

INSERT INTO `voucher` (`id`, `code`, `status`, `price`, `used_by`, `date_created`, `date_modified`, `date_used`, `print_status`) VALUES
(1, 'KjO6gf8wbEbcHcgu', 1, 1450, 2, '2014-12-15 23:57:12', '2014-12-15 23:57:12', '2014-12-08 20:23:13', 1),
(2, 'ZJe5gBporxZM9eHX', 1, 1450, 2, '2014-12-15 23:57:12', '2014-12-15 23:57:12', '2014-12-01 20:23:18', 1),
(3, 'ZK9t4PM1EwHvG0Y4', 1, 1450, 4, '2014-12-15 23:57:12', '2014-12-15 23:57:12', '2014-12-16 18:46:21', 1),
(4, 'RGJ5jq9NKT307JJE', 1, 1450, 4, '2014-12-15 23:57:12', '2014-12-15 23:57:12', '2014-12-16 19:42:40', 1),
(5, 'p52nNUI6hn5uy6aO', 1, 1450, 3, '2014-12-15 23:57:12', '2014-12-15 23:57:12', '2014-12-17 14:34:54', 1),
(6, '45r86uZm9nanBV3f', 1, 1450, 4, '2014-12-15 23:57:12', '2014-12-15 23:57:12', '2014-12-17 14:45:55', 1),
(7, 'aZzKZ3QhLiOJzt7K', 1, 1450, 4, '2014-12-15 23:57:12', '2014-12-15 23:57:12', '2014-12-17 14:46:10', 1),
(8, 'kRFrB3GEvOQfQVyO', 1, 1450, 5, '2014-12-15 23:57:12', '2014-12-15 23:57:12', '2014-12-17 14:48:53', 1),
(9, 'lFkRwdVEmuohPwA9', 1, 1450, 5, '2014-12-15 23:57:13', '2014-12-15 23:57:13', '2014-12-17 14:49:06', 1),
(10, 'nV8YkoSvQvZNAZat', 1, 1450, 3, '2014-12-15 23:57:13', '2014-12-15 23:57:13', '2014-12-17 20:58:38', 1),
(11, 'gPL8K8N40JIOfKaQ', 1, 1450, 3, '2014-12-16 00:17:40', '2014-12-16 00:17:40', '2014-12-17 21:04:03', 1),
(12, 'V2jtuwYgguRhqHI4', 1, 1450, 6, '2014-12-16 00:17:40', '2014-12-16 00:17:40', '2014-12-18 11:57:48', 1),
(13, 'a6MR33zC7Ps1Utjl', 1, 1450, 6, '2014-12-16 00:17:40', '2014-12-16 00:17:40', '2014-12-18 11:58:00', 1),
(14, '0FAxo54HL6IYKTzV', 1, 1450, 6, '2014-12-16 00:17:40', '2014-12-16 00:17:40', '2014-12-18 11:58:22', 1),
(15, 'MEvRGjaSRlBaLyO7', 1, 1450, 6, '2014-12-16 00:17:40', '2014-12-16 00:17:40', '2014-12-18 13:08:10', 1),
(16, 'vjRytfpKNgvHjnGz', 1, 1450, 6, '2014-12-16 00:17:40', '2014-12-16 00:17:40', '2014-12-18 13:12:05', 1),
(17, 'KzwL5yb1tHcC0SY5', 1, 1450, 7, '2014-12-16 00:17:40', '2014-12-16 00:17:40', '2014-12-18 17:43:34', 1),
(18, 'F6WZR21oIeJKShnK', 1, 1450, 7, '2014-12-16 00:17:40', '2014-12-16 00:17:40', '2014-12-18 17:52:20', 1),
(19, 'zd7zlaqvBR5bZCEg', 1, 1450, 7, '2014-12-16 00:17:40', '2014-12-16 00:17:40', '2014-12-18 17:54:09', 1),
(20, 'WZMS3gXnuLoiiwn0', 0, 1450, NULL, '2014-12-16 00:17:41', '2014-12-16 00:17:41', NULL, 1),
(21, 'zO66C38uLwV1keHK', 0, 1450, NULL, '2014-12-16 00:18:28', '2014-12-16 00:18:28', NULL, 1),
(22, '27URX1NX5dRK1W8K', 0, 1450, NULL, '2014-12-16 00:18:28', '2014-12-16 00:18:28', NULL, 1),
(23, 'A5kNsVLj4uwUfUIM', 0, 1450, NULL, '2014-12-16 00:18:28', '2014-12-16 00:18:28', NULL, 1),
(24, 'bKXYkh7DE3WQR8Ct', 0, 1450, NULL, '2014-12-16 00:18:28', '2014-12-16 00:18:28', NULL, 1),
(25, 'KNOq3QndjDPmUlws', 0, 1450, NULL, '2014-12-16 00:18:28', '2014-12-16 00:18:28', NULL, 1),
(26, 'bEEgiOIxMqTEzJvf', 0, 1450, NULL, '2014-12-16 00:18:28', '2014-12-16 00:18:28', NULL, 1),
(27, 'MEKR4tbXpRGww5ty', 0, 1450, NULL, '2014-12-16 00:18:28', '2014-12-16 00:18:28', NULL, 1),
(28, 'VxywZ43ZndGN2dmx', 0, 1450, NULL, '2014-12-16 00:18:28', '2014-12-16 00:18:28', NULL, 1),
(29, '9SdKRjQhC2cVEPkl', 0, 1450, NULL, '2014-12-16 00:18:28', '2014-12-16 00:18:28', NULL, 1),
(30, 'kdVW9MwgDLQX3qx5', 0, 1450, NULL, '2014-12-16 00:18:28', '2014-12-16 00:18:28', NULL, 1),
(31, 'Se49NXdSdTspGM3X', 0, 1450, NULL, '2014-12-16 00:18:36', '2014-12-16 00:18:36', NULL, 1),
(32, 'os1KgGKGIsQZY7G2', 0, 1450, NULL, '2014-12-16 00:18:36', '2014-12-16 00:18:36', NULL, 1),
(33, 'P48yxOOM3jVPiee5', 0, 1450, NULL, '2014-12-16 00:18:36', '2014-12-16 00:18:36', NULL, 1),
(34, 'BHPQ9SPSJxWj0S5s', 0, 1450, NULL, '2014-12-16 00:18:36', '2014-12-16 00:18:36', NULL, 1),
(35, 'tA66GDBQNNKeMXZd', 0, 1450, NULL, '2014-12-16 00:18:36', '2014-12-16 00:18:36', NULL, 1),
(36, 'Ic0bKpujH2uuYcSI', 0, 1450, NULL, '2014-12-16 00:18:36', '2014-12-16 00:18:36', NULL, 1),
(37, '5AD6SrUdXoJrd3aE', 0, 1450, NULL, '2014-12-16 00:18:37', '2014-12-16 00:18:37', NULL, 1),
(38, 'YfPKE9j2bBHjL8sw', 0, 1450, NULL, '2014-12-16 00:18:37', '2014-12-16 00:18:37', NULL, 1),
(39, 'FDmoGjg6DkJlb4kq', 0, 1450, NULL, '2014-12-16 00:18:37', '2014-12-16 00:18:37', NULL, 1),
(40, 'lUAFioequKFkJjqC', 0, 1450, NULL, '2014-12-16 00:18:37', '2014-12-16 00:18:37', NULL, 1),
(41, 'izYYyv5dVYkeYIbQ', 0, 1450, NULL, '2014-12-16 18:21:27', '2014-12-16 18:21:27', NULL, 1),
(42, 'KMxbgV9uu9uLA4hY', 0, 1450, NULL, '2014-12-16 18:21:27', '2014-12-16 18:21:27', NULL, 1),
(43, 'MJgSbtbnUKf3YUDH', 0, 1450, NULL, '2014-12-16 18:21:27', '2014-12-16 18:21:27', NULL, 1),
(44, 'Y5LRsPkEBL27SQKD', 0, 1450, NULL, '2014-12-16 18:21:27', '2014-12-16 18:21:27', NULL, 1),
(45, 'c5vIG6ZftcLbumLV', 1, 1450, 6, '2014-12-16 18:21:27', '2014-12-16 18:21:27', '2014-12-20 18:42:25', 1),
(46, '86UezUFzYHgYrWrH', 1, 1450, 6, '2014-12-16 18:21:27', '2014-12-16 18:21:27', '2014-12-20 18:44:21', 1),
(47, 'CyD7s2gbtTsDNdwg', 1, 1450, 6, '2014-12-16 18:21:27', '2014-12-16 18:21:27', '2014-12-20 18:44:36', 1),
(48, 'rc33Qb64H76Xgjlf', 0, 1450, NULL, '2014-12-16 18:21:27', '2014-12-16 18:21:27', NULL, 1),
(49, 'wxPxpVFj4q5FGr0N', 0, 1450, NULL, '2014-12-16 18:21:27', '2014-12-16 18:21:27', NULL, 1),
(50, 'vy1ajTbh7Lsi92VH', 0, 1450, NULL, '2014-12-16 18:21:27', '2014-12-16 18:21:27', NULL, 1),
(51, 'YjveW1uJaj0SZRsq', 0, 1450, NULL, '2014-12-17 10:22:38', '2014-12-17 10:22:38', NULL, 1),
(52, 'uNKMuISJHtqK4HnL', 0, 1450, NULL, '2014-12-17 10:22:38', '2014-12-17 10:22:38', NULL, 1),
(53, '2giwIl78bVaA6FOD', 0, 1450, NULL, '2014-12-17 10:22:38', '2014-12-17 10:22:38', NULL, 1),
(54, 'X0epxNGYnBe0sEkB', 0, 1450, NULL, '2014-12-17 10:22:38', '2014-12-17 10:22:38', NULL, 1),
(55, 'SiN3QWvg0HVBmT3X', 0, 1450, NULL, '2014-12-17 10:22:38', '2014-12-17 10:22:38', NULL, 1),
(56, 'EIzubWKVLUA1ZhYd', 0, 1450, NULL, '2014-12-17 10:22:38', '2014-12-17 10:22:38', NULL, 1),
(57, '1CtID1qzfvv18Lj6', 0, 1450, NULL, '2014-12-17 10:22:38', '2014-12-17 10:22:38', NULL, 1),
(58, 'FRgOdJaxh6aN5ge0', 0, 1450, NULL, '2014-12-17 10:22:39', '2014-12-17 10:22:39', NULL, 1),
(59, 'BDiPCMWfdxhXqS1f', 0, 1450, NULL, '2014-12-17 10:22:39', '2014-12-17 10:22:39', NULL, 1),
(60, 'A1GnSbR8QtRqcAOg', 0, 1450, NULL, '2014-12-17 10:22:39', '2014-12-17 10:22:39', NULL, 1),
(61, 'SigjvgPx6PnTciJv', 0, 1450, NULL, '2014-12-17 10:24:30', '2014-12-17 10:24:30', NULL, 1),
(62, 'iHcfNPShY9XF2YBW', 0, 1450, NULL, '2014-12-17 10:24:30', '2014-12-17 10:24:30', NULL, 1),
(63, 't7HTTXaJuZ6NM1hj', 0, 1450, NULL, '2014-12-17 10:24:30', '2014-12-17 10:24:30', NULL, 1),
(64, 'IqEPM5vRWFEpGO5y', 0, 1450, NULL, '2014-12-17 10:24:30', '2014-12-17 10:24:30', NULL, 1),
(65, 'yX3uv6j5r2QDnWSO', 0, 1450, NULL, '2014-12-17 10:24:30', '2014-12-17 10:24:30', NULL, 1),
(66, 'PmaFe3EV9c96x0EX', 0, 1450, NULL, '2014-12-17 10:24:30', '2014-12-17 10:24:30', NULL, 1),
(67, '7jAR2Ypdnesv6IcE', 0, 1450, NULL, '2014-12-17 10:24:30', '2014-12-17 10:24:30', NULL, 1),
(68, 'n3k84Ow90iuXD4Cz', 0, 1450, NULL, '2014-12-17 10:24:30', '2014-12-17 10:24:30', NULL, 1),
(69, 'heDXOY0Hb4im3cF0', 0, 1450, NULL, '2014-12-17 10:24:30', '2014-12-17 10:24:30', NULL, 1),
(70, 'gjOymdA8J0B3FFQp', 0, 1450, NULL, '2014-12-17 10:24:30', '2014-12-17 10:24:30', NULL, 1),
(71, 'mxvkEzLHaRODtqZa', 0, 1450, NULL, '2014-12-17 10:24:39', '2014-12-17 10:24:39', NULL, 1),
(72, 'cCR0utpgSx7E9pzJ', 0, 1450, NULL, '2014-12-17 10:24:39', '2014-12-17 10:24:39', NULL, 1),
(73, 'kSOEleeX4fe25LrK', 0, 1450, NULL, '2014-12-17 10:24:39', '2014-12-17 10:24:39', NULL, 1),
(74, 'dezyrNAxat10LwyE', 0, 1450, NULL, '2014-12-17 10:24:39', '2014-12-17 10:24:39', NULL, 1),
(75, 'BYjNO65f83i924dO', 0, 1450, NULL, '2014-12-17 10:24:40', '2014-12-17 10:24:40', NULL, 1),
(76, 'QPR5UeHMVaPKfILC', 0, 1450, NULL, '2014-12-17 10:24:40', '2014-12-17 10:24:40', NULL, 1),
(77, 'TS88TVySSUj8SuvQ', 0, 1450, NULL, '2014-12-17 10:24:40', '2014-12-17 10:24:40', NULL, 1),
(78, '8jrXnkyGvDroEHiQ', 0, 1450, NULL, '2014-12-17 10:24:40', '2014-12-17 10:24:40', NULL, 1),
(79, 'YF0X0lM498ehmt1h', 0, 1450, NULL, '2014-12-17 10:24:40', '2014-12-17 10:24:40', NULL, 1),
(80, 'okubsZ6VEEKyFd4J', 0, 1450, NULL, '2014-12-17 10:24:40', '2014-12-17 10:24:40', NULL, 1),
(81, 'DuZeeVfSOI5KV6NE', 0, 1450, NULL, '2014-12-17 10:25:32', '2014-12-17 10:25:32', NULL, 1),
(82, '2bz76KY5KK9FasUO', 0, 1450, NULL, '2014-12-17 10:25:32', '2014-12-17 10:25:32', NULL, 1),
(83, 'KivKypDG0i4aDoOW', 0, 1450, NULL, '2014-12-17 10:25:32', '2014-12-17 10:25:32', NULL, 1),
(84, '1OugsQIeGE72wJmY', 0, 1450, NULL, '2014-12-17 10:25:32', '2014-12-17 10:25:32', NULL, 1),
(85, 'Wp4sUUsjHCRhWBMZ', 0, 1450, NULL, '2014-12-17 10:25:32', '2014-12-17 10:25:32', NULL, 1),
(86, '9XwrhgqwBjegkyPg', 0, 1450, NULL, '2014-12-17 10:25:32', '2014-12-17 10:25:32', NULL, 1),
(87, '65Cmz5Jt5HMvvuGx', 0, 1450, NULL, '2014-12-17 10:25:32', '2014-12-17 10:25:32', NULL, 1),
(88, 'y1wfiMFV0GNQHxJS', 0, 1450, NULL, '2014-12-17 10:25:32', '2014-12-17 10:25:32', NULL, 1),
(89, 'LkTvSoNhwgtxrNBJ', 0, 1450, NULL, '2014-12-17 10:25:32', '2014-12-17 10:25:32', NULL, 1),
(90, 'IrBPtkfnOEoBOkeM', 0, 1450, NULL, '2014-12-17 10:25:32', '2014-12-17 10:25:32', NULL, 1),
(91, '1ZZ1rv94prKn2SO4', 0, 1450, NULL, '2014-12-17 10:26:11', '2014-12-17 10:26:11', NULL, 1),
(92, 'sdJxjdf3LF0rsGKy', 0, 1450, NULL, '2014-12-17 10:26:11', '2014-12-17 10:26:11', NULL, 1),
(93, 'b2o1bf9NQL2ydmic', 0, 1450, NULL, '2014-12-17 10:26:11', '2014-12-17 10:26:11', NULL, 1),
(94, 'z9COFL3HPqUHKPCS', 0, 1450, NULL, '2014-12-17 10:26:11', '2014-12-17 10:26:11', NULL, 1),
(95, 'Z0OufxJrgEpcwoPE', 0, 1450, NULL, '2014-12-17 10:26:12', '2014-12-17 10:26:12', NULL, 1),
(96, 'wr4eQ4TKyWYWAbR3', 0, 1450, NULL, '2014-12-17 10:26:12', '2014-12-17 10:26:12', NULL, 1),
(97, 'VIEn5Ix45cTN4y8q', 0, 1450, NULL, '2014-12-17 10:26:12', '2014-12-17 10:26:12', NULL, 1),
(98, 'NyBQ2ZYdwFfd65wz', 0, 1450, NULL, '2014-12-17 10:26:12', '2014-12-17 10:26:12', NULL, 1),
(99, 'rilQyDAXXlqXgSCw', 0, 1450, NULL, '2014-12-17 10:26:12', '2014-12-17 10:26:12', NULL, 1),
(100, 'VWYO5eixLFiHFc3e', 0, 1450, NULL, '2014-12-17 10:26:12', '2014-12-17 10:26:12', NULL, 1),
(101, 'e9Hrxz11fwKbjMss', 0, 1450, NULL, '2014-12-17 10:26:30', '2014-12-17 10:26:30', NULL, 1),
(102, '2ixUPb26Aih7RtPj', 0, 1450, NULL, '2014-12-17 10:26:30', '2014-12-17 10:26:30', NULL, 1),
(103, 'FqjvlNEHYiWvlfh4', 0, 1450, NULL, '2014-12-17 10:26:30', '2014-12-17 10:26:30', NULL, 1),
(104, 'zJAm6Muma10BWFQk', 0, 1450, NULL, '2014-12-17 10:26:30', '2014-12-17 10:26:30', NULL, 1),
(105, 'bboUvGAwlggiKdgE', 0, 1450, NULL, '2014-12-17 10:26:30', '2014-12-17 10:26:30', NULL, 1),
(106, 'KOYtrEmwwi1DZHt6', 0, 1450, NULL, '2014-12-17 10:26:30', '2014-12-17 10:26:30', NULL, 1),
(107, '4p1tF8EXwU8Pjwrm', 0, 1450, NULL, '2014-12-17 10:26:30', '2014-12-17 10:26:30', NULL, 1),
(108, 'eJZP2ygxuCnue4CR', 0, 1450, NULL, '2014-12-17 10:26:31', '2014-12-17 10:26:31', NULL, 1),
(109, 'yPQZuj2D4nZL32f4', 0, 1450, NULL, '2014-12-17 10:26:31', '2014-12-17 10:26:31', NULL, 1),
(110, '0Ms6dtZRwCZiZZ8M', 0, 1450, NULL, '2014-12-17 10:26:31', '2014-12-17 10:26:31', NULL, 1),
(111, 'e79rlkzztN8q3DrE', 0, 1450, NULL, '2014-12-17 10:27:00', '2014-12-17 10:27:00', NULL, 1),
(112, 'GgX1TK00wAUieezY', 0, 1450, NULL, '2014-12-17 10:27:00', '2014-12-17 10:27:00', NULL, 1),
(113, 'ESWzTZ5564wAUV7I', 0, 1450, NULL, '2014-12-17 10:27:00', '2014-12-17 10:27:00', NULL, 1),
(114, 'CTPZO3VEanrJrGC6', 0, 1450, NULL, '2014-12-17 10:27:00', '2014-12-17 10:27:00', NULL, 1),
(115, '8j9hNx5VTk5ZGFtQ', 0, 1450, NULL, '2014-12-17 10:27:00', '2014-12-17 10:27:00', NULL, 1),
(116, 'XE2EYdhZLAADzP1c', 0, 1450, NULL, '2014-12-17 10:27:00', '2014-12-17 10:27:00', NULL, 1),
(117, 'weQfayt6VIH3WfLf', 0, 1450, NULL, '2014-12-17 10:27:00', '2014-12-17 10:27:00', NULL, 1),
(118, 'vr7YkRZZSN3y1H57', 0, 1450, NULL, '2014-12-17 10:27:00', '2014-12-17 10:27:00', NULL, 1),
(119, 'njUJdJbQUmpwRIAi', 0, 1450, NULL, '2014-12-17 10:27:00', '2014-12-17 10:27:00', NULL, 1),
(120, 'Phyt7D7SRTJ66cOF', 0, 1450, NULL, '2014-12-17 10:27:00', '2014-12-17 10:27:00', NULL, 1),
(121, 'CPk42yhrqmBFWgU0', 0, 1450, NULL, '2014-12-17 13:20:52', '2014-12-17 13:20:52', NULL, 1),
(122, 'lll0L4VCXv5CxQvA', 0, 1450, NULL, '2014-12-17 13:20:52', '2014-12-17 13:20:52', NULL, 1),
(123, 'EDSpPNEaYcf58no1', 0, 1450, NULL, '2014-12-17 13:20:53', '2014-12-17 13:20:53', NULL, 1),
(124, 'RZ8qjGXEpoWLyfvX', 0, 1450, NULL, '2014-12-17 13:20:53', '2014-12-17 13:20:53', NULL, 1),
(125, 'VKBkz8IJ7fOcn8FA', 0, 1450, NULL, '2014-12-17 13:20:53', '2014-12-17 13:20:53', NULL, 1),
(126, '6SzHQ2AHgMyAXM7r', 0, 1450, NULL, '2014-12-17 13:20:53', '2014-12-17 13:20:53', NULL, 1),
(127, 'aFB30qq8oVnveqgI', 0, 1450, NULL, '2014-12-17 13:20:53', '2014-12-17 13:20:53', NULL, 1),
(128, 'bxHJlzO6boF2AZUo', 0, 1450, NULL, '2014-12-17 13:20:53', '2014-12-17 13:20:53', NULL, 1),
(129, 'NOzI4ol8J64ypCKr', 0, 1450, NULL, '2014-12-17 13:20:53', '2014-12-17 13:20:53', NULL, 1),
(130, 'RDiqWJcXvWqQjCQo', 0, 1450, NULL, '2014-12-17 13:20:53', '2014-12-17 13:20:53', NULL, 1),
(131, 'Luj7U5ewwJ8NcRlr', 0, 1450, NULL, '2014-12-17 13:27:02', '2014-12-17 13:27:02', NULL, 1),
(132, 'GRZ3RvAH0rM88n0J', 0, 1450, NULL, '2014-12-17 13:27:02', '2014-12-17 13:27:02', NULL, 1),
(133, 'BtUQilUkaarGadUP', 0, 1450, NULL, '2014-12-17 13:27:02', '2014-12-17 13:27:02', NULL, 1),
(134, 'pRhyaoKO895FGk7L', 0, 1450, NULL, '2014-12-17 13:27:02', '2014-12-17 13:27:02', NULL, 1),
(135, 'aHiKkGTRZyOkR69E', 0, 1450, NULL, '2014-12-17 13:27:02', '2014-12-17 13:27:02', NULL, 1),
(136, 'Lc4LtDfDreZ2qYzJ', 0, 1450, NULL, '2014-12-17 13:27:02', '2014-12-17 13:27:02', NULL, 1),
(137, 'MOxDhI9nWmgSq80p', 0, 1450, NULL, '2014-12-17 13:27:02', '2014-12-17 13:27:02', NULL, 1),
(138, 'HPAvQP3iLNoUB2ld', 0, 1450, NULL, '2014-12-17 13:27:02', '2014-12-17 13:27:02', NULL, 1),
(139, '6HwrgKydLcq5HlEH', 0, 1450, NULL, '2014-12-17 13:27:02', '2014-12-17 13:27:02', NULL, 1),
(140, 'bfdF0Gna1f888aph', 0, 1450, NULL, '2014-12-17 13:27:02', '2014-12-17 13:27:02', NULL, 1),
(141, 'mAMNtiGrps1MjFTd', 0, 1450, NULL, '2014-12-17 13:27:23', '2014-12-17 13:27:23', NULL, 1),
(142, 'bQUBcfwG0tKQVMe0', 0, 1450, NULL, '2014-12-17 13:27:23', '2014-12-17 13:27:23', NULL, 1),
(143, 'eaCasFqFWuQYMIfn', 0, 1450, NULL, '2014-12-17 13:27:23', '2014-12-17 13:27:23', NULL, 1),
(144, 'wctTIo2ejaXNiNIt', 0, 1450, NULL, '2014-12-17 13:27:23', '2014-12-17 13:27:23', NULL, 1),
(145, 'xj611PVXW76ZNDla', 0, 1450, NULL, '2014-12-17 13:27:23', '2014-12-17 13:27:23', NULL, 1),
(146, 'rWmzNRXDqUzCv1wP', 0, 1450, NULL, '2014-12-17 13:27:23', '2014-12-17 13:27:23', NULL, 1),
(147, 'M6vKvc3SxDDlgnfn', 0, 1450, NULL, '2014-12-17 13:27:23', '2014-12-17 13:27:23', NULL, 1),
(148, 'LZAgKeyVZOjiJoaq', 0, 1450, NULL, '2014-12-17 13:27:23', '2014-12-17 13:27:23', NULL, 1),
(149, 'mWqNPuKpJIx7zZB7', 0, 1450, NULL, '2014-12-17 13:27:23', '2014-12-17 13:27:23', NULL, 1),
(150, 'foZmI3nYrNtxfL5T', 0, 1450, NULL, '2014-12-17 13:27:23', '2014-12-17 13:27:23', NULL, 1),
(151, 'ITWt6z5MsCLbNTNH', 0, 1450, NULL, '2014-12-17 13:29:04', '2014-12-17 13:29:04', NULL, 1),
(152, 'rkJsNaLkL5dXGg5e', 0, 1450, NULL, '2014-12-17 13:29:04', '2014-12-17 13:29:04', NULL, 1),
(153, 'byjE90tw4HHevtP8', 0, 1450, NULL, '2014-12-17 13:29:04', '2014-12-17 13:29:04', NULL, 1),
(154, 'SW8tCu4nCO8HY7j4', 0, 1450, NULL, '2014-12-17 13:29:04', '2014-12-17 13:29:04', NULL, 1),
(155, 'EVbDlIlwAwjXtQk4', 0, 1450, NULL, '2014-12-17 13:29:04', '2014-12-17 13:29:04', NULL, 1),
(156, 'p2KEpqAzxgkyTW8T', 0, 1450, NULL, '2014-12-17 13:29:04', '2014-12-17 13:29:04', NULL, 1),
(157, 'DGtrWPxx9NUtcmd2', 0, 1450, NULL, '2014-12-17 13:29:04', '2014-12-17 13:29:04', NULL, 1),
(158, 'oH7j3QnfeNcDfoVE', 0, 1450, NULL, '2014-12-17 13:29:04', '2014-12-17 13:29:04', NULL, 1),
(159, 'mNX8Vfnjs8KEXPgq', 0, 1450, NULL, '2014-12-17 13:29:04', '2014-12-17 13:29:04', NULL, 1),
(160, 'kkwuChAQ8S6YF8mh', 0, 1450, NULL, '2014-12-17 13:29:04', '2014-12-17 13:29:04', NULL, 1),
(161, 'yKnFHCpGLxPk6oRn', 0, 1450, NULL, '2014-12-17 13:31:54', '2014-12-17 13:31:54', NULL, 1),
(162, '8vyTqV21Zayo4MmW', 0, 1450, NULL, '2014-12-17 13:31:54', '2014-12-17 13:31:54', NULL, 1),
(163, '8lB1jLC4MKNpZhOP', 0, 1450, NULL, '2014-12-17 13:31:55', '2014-12-17 13:31:55', NULL, 1),
(164, 'X7Bm57BNDze5SQxg', 0, 1450, NULL, '2014-12-17 13:31:55', '2014-12-17 13:31:55', NULL, 1),
(165, 'brktuW73zMqoUabi', 0, 1450, NULL, '2014-12-17 13:31:55', '2014-12-17 13:31:55', NULL, 1),
(166, '8veue1FtlkXpTdUP', 0, 1450, NULL, '2014-12-17 13:31:55', '2014-12-17 13:31:55', NULL, 1),
(167, 'IreMkj6gB9TBJki6', 0, 1450, NULL, '2014-12-17 13:31:55', '2014-12-17 13:31:55', NULL, 1),
(168, 'KKNprPvHsGthhlJM', 0, 1450, NULL, '2014-12-17 13:31:55', '2014-12-17 13:31:55', NULL, 1),
(169, '1AeB7RlBCnaX7km1', 0, 1450, NULL, '2014-12-17 13:31:55', '2014-12-17 13:31:55', NULL, 1),
(170, 'WK5zkcNNEibU2bob', 0, 1450, NULL, '2014-12-17 13:31:55', '2014-12-17 13:31:55', NULL, 1),
(171, 'hBdEY1KY4PzoG2QB', 0, 1450, NULL, '2014-12-17 13:32:15', '2014-12-17 13:32:15', NULL, 1),
(172, 'Jn52R9hCNUI4aKm7', 0, 1450, NULL, '2014-12-17 13:32:15', '2014-12-17 13:32:15', NULL, 1),
(173, '2PCwr8kH1EGmBdYz', 0, 1450, NULL, '2014-12-17 13:32:15', '2014-12-17 13:32:15', NULL, 1),
(174, 'TDO6PNN61N48Yjh1', 0, 1450, NULL, '2014-12-17 13:32:15', '2014-12-17 13:32:15', NULL, 1),
(175, '4hPeE4ARlvBRhLHw', 0, 1450, NULL, '2014-12-17 13:32:15', '2014-12-17 13:32:15', NULL, 1),
(176, 'oeXMbo30TSAuwp0z', 0, 1450, NULL, '2014-12-17 13:32:15', '2014-12-17 13:32:15', NULL, 1),
(177, 'dGY8jbdVZ6kvtGnY', 0, 1450, NULL, '2014-12-17 13:32:15', '2014-12-17 13:32:15', NULL, 1),
(178, 'vBrfed6Jm07rNkFp', 0, 1450, NULL, '2014-12-17 13:32:15', '2014-12-17 13:32:15', NULL, 1),
(179, 'rzgGSuCDDEcXCXZw', 0, 1450, NULL, '2014-12-17 13:32:15', '2014-12-17 13:32:15', NULL, 1),
(180, 'jPcMewWJXYePCb4E', 0, 1450, NULL, '2014-12-17 13:32:16', '2014-12-17 13:32:16', NULL, 1),
(181, 'zaRA1VmYAgGkkk12', 0, 1450, NULL, '2014-12-17 13:48:30', '2014-12-17 13:48:30', NULL, 1),
(182, 'hzVUVZdtD5TSncM4', 0, 1450, NULL, '2014-12-17 13:48:30', '2014-12-17 13:48:30', NULL, 1),
(183, 'UCxMvLf1GdMkmjz4', 0, 1450, NULL, '2014-12-17 13:48:30', '2014-12-17 13:48:30', NULL, 1),
(184, 'rOuXcamM5L1YIYHl', 0, 1450, NULL, '2014-12-17 13:48:30', '2014-12-17 13:48:30', NULL, 1),
(185, 'oCibKfV7JpjjcpyA', 0, 1450, NULL, '2014-12-17 13:48:31', '2014-12-17 13:48:31', NULL, 1),
(186, 'P9sTL3zrX1fqvSMr', 0, 1450, NULL, '2014-12-17 13:48:31', '2014-12-17 13:48:31', NULL, 1),
(187, 'D7wuB4DlT5fpxXpO', 0, 1450, NULL, '2014-12-17 13:48:31', '2014-12-17 13:48:31', NULL, 1),
(188, 'MPVGZqXp4H3j8Ub9', 0, 1450, NULL, '2014-12-17 13:48:31', '2014-12-17 13:48:31', NULL, 1),
(189, 'oLfaERiYmu3jAAor', 0, 1450, NULL, '2014-12-17 13:48:31', '2014-12-17 13:48:31', NULL, 1),
(190, 'TyMwtDi4gGQqUqSq', 0, 1450, NULL, '2014-12-17 13:48:31', '2014-12-17 13:48:31', NULL, 1),
(191, 'hdKMDHbpcrA1Kz1w', 0, 1450, NULL, '2014-12-17 21:54:13', '2014-12-17 21:54:13', NULL, 1),
(192, 'AoO2SVxLKBFVEqRf', 0, 1450, NULL, '2014-12-17 21:54:13', '2014-12-17 21:54:13', NULL, 1),
(193, 'sys93aVm70OrsWMR', 0, 1450, NULL, '2014-12-17 21:54:13', '2014-12-17 21:54:13', NULL, 1),
(194, '8TcuiWFlNEeXtoZT', 0, 1450, NULL, '2014-12-17 21:54:13', '2014-12-17 21:54:13', NULL, 1),
(195, '06fGI0WRBKLIQMCw', 0, 1450, NULL, '2014-12-17 21:54:14', '2014-12-17 21:54:14', NULL, 1),
(196, 'fWNqdq4l6eCPqJtW', 0, 1450, NULL, '2014-12-17 21:54:14', '2014-12-17 21:54:14', NULL, 1),
(197, 'kZTMLrGgXZhAy33g', 0, 1450, NULL, '2014-12-17 21:54:14', '2014-12-17 21:54:14', NULL, 1),
(198, 'n1jYE6BtbeNhVhdG', 0, 1450, NULL, '2014-12-17 21:54:14', '2014-12-17 21:54:14', NULL, 1),
(199, 'IygLpF6bYsuVpPMZ', 0, 1450, NULL, '2014-12-17 21:54:14', '2014-12-17 21:54:14', NULL, 1),
(200, 'Wyg6IoBSI37WtMCr', 0, 1450, NULL, '2014-12-17 21:54:14', '2014-12-17 21:54:14', NULL, 1),
(201, 'seedllbGzeObysxQ', 0, 1450, NULL, '2014-12-17 22:00:29', '2014-12-17 22:00:29', NULL, 1),
(202, 'BqQ4tKIBLzBFyRrX', 0, 1450, NULL, '2014-12-17 22:00:29', '2014-12-17 22:00:29', NULL, 1),
(203, 'c4GwYrdSOtHx1r3B', 0, 1450, NULL, '2014-12-17 22:00:29', '2014-12-17 22:00:29', NULL, 1),
(204, 'i0hJtGFmyz0HJmIb', 0, 1450, NULL, '2014-12-17 22:00:29', '2014-12-17 22:00:29', NULL, 1),
(205, 'fxjkbJkMt1j7OnER', 0, 1450, NULL, '2014-12-17 22:00:29', '2014-12-17 22:00:29', NULL, 1),
(206, 'SLM0Tb5lc7pPwH7P', 0, 1450, NULL, '2014-12-17 22:00:29', '2014-12-17 22:00:29', NULL, 1),
(207, 'R5vnev6ErI8FJnXe', 0, 1450, NULL, '2014-12-17 22:00:29', '2014-12-17 22:00:29', NULL, 1),
(208, 'vYRW5BuMwloAodH7', 0, 1450, NULL, '2014-12-17 22:00:29', '2014-12-17 22:00:29', NULL, 1),
(209, 'NtWH7SSqOP6msqWK', 0, 1450, NULL, '2014-12-17 22:00:29', '2014-12-17 22:00:29', NULL, 1),
(210, '1plkhYEGDB8r5qBX', 0, 1450, NULL, '2014-12-17 22:00:29', '2014-12-17 22:00:29', NULL, 1),
(211, 'TAWybyV', 0, 1450, NULL, '2014-12-18 09:52:23', '2014-12-18 09:52:23', NULL, 1),
(212, 'MSItwoJ', 0, 1450, NULL, '2014-12-18 09:52:24', '2014-12-18 09:52:24', NULL, 1),
(213, 'S06l7LY', 0, 1450, NULL, '2014-12-18 09:52:24', '2014-12-18 09:52:24', NULL, 1),
(214, 'IiUcQDE', 0, 1450, NULL, '2014-12-18 09:52:24', '2014-12-18 09:52:24', NULL, 1),
(215, 'ImYSQg4', 0, 1450, NULL, '2014-12-18 09:52:24', '2014-12-18 09:52:24', NULL, 1),
(216, 'w9ah6iI', 0, 1450, NULL, '2014-12-18 09:52:24', '2014-12-18 09:52:24', NULL, 1),
(217, '77FEvwO', 0, 1450, NULL, '2014-12-18 09:52:24', '2014-12-18 09:52:24', NULL, 1),
(218, '5AuXRW9', 0, 1450, NULL, '2014-12-18 09:52:24', '2014-12-18 09:52:24', NULL, 1),
(219, '0oqaofs', 0, 1450, NULL, '2014-12-18 09:52:24', '2014-12-18 09:52:24', NULL, 1),
(220, 'IE2Y5Tw', 1, 1450, 8, '2014-12-18 09:52:24', '2014-12-18 09:52:24', '2014-12-22 10:16:11', 1),
(221, 'i6HXpAS', 0, 1450, NULL, '2014-12-22 11:25:28', '2014-12-22 11:25:28', NULL, 0),
(222, '7w262cB', 0, 1450, NULL, '2014-12-22 11:25:29', '2014-12-22 11:25:29', NULL, 0),
(223, 'WJrY30r', 0, 1450, NULL, '2014-12-22 11:25:29', '2014-12-22 11:25:29', NULL, 0),
(224, 'NUxpOrp', 0, 1450, NULL, '2014-12-22 11:25:29', '2014-12-22 11:25:29', NULL, 0),
(225, 'HqmzzV9', 0, 1450, NULL, '2014-12-22 11:25:29', '2014-12-22 11:25:29', NULL, 0),
(226, 'M9rMfqo', 0, 1450, NULL, '2014-12-22 11:25:29', '2014-12-22 11:25:29', NULL, 0),
(227, '3HGUSKQ', 0, 1450, NULL, '2014-12-22 11:25:29', '2014-12-22 11:25:29', NULL, 0),
(228, 'Ss8xZuq', 0, 1450, NULL, '2014-12-22 11:25:29', '2014-12-22 11:25:29', NULL, 0),
(229, 'uFlPMqo', 0, 1450, NULL, '2014-12-22 11:25:29', '2014-12-22 11:25:29', NULL, 0),
(230, 'j1xNVc0', 0, 1450, NULL, '2014-12-22 11:25:29', '2014-12-22 11:25:29', NULL, 0),
(231, 'RlNbK1O', 0, 1450, NULL, '2014-12-22 11:25:37', '2014-12-22 11:25:37', NULL, 0),
(232, 'T08WcYT', 0, 1450, NULL, '2014-12-22 11:25:37', '2014-12-22 11:25:37', NULL, 0),
(233, 'SwgRRx3', 0, 1450, NULL, '2014-12-22 11:25:37', '2014-12-22 11:25:37', NULL, 0),
(234, 'gUWZLo8', 0, 1450, NULL, '2014-12-22 11:25:37', '2014-12-22 11:25:37', NULL, 0),
(235, 'Sfr9b9q', 0, 1450, NULL, '2014-12-22 11:25:37', '2014-12-22 11:25:37', NULL, 0),
(236, 'ZuqFWou', 0, 1450, NULL, '2014-12-22 11:25:37', '2014-12-22 11:25:37', NULL, 0),
(237, 'bDyXyIH', 0, 1450, NULL, '2014-12-22 11:25:37', '2014-12-22 11:25:37', NULL, 0),
(238, 'IU2wp2y', 0, 1450, NULL, '2014-12-22 11:25:37', '2014-12-22 11:25:37', NULL, 0),
(239, '3VOKCl8', 0, 1450, NULL, '2014-12-22 11:25:37', '2014-12-22 11:25:37', NULL, 0),
(240, 'rZwJU1I', 0, 1450, NULL, '2014-12-22 11:25:37', '2014-12-22 11:25:37', NULL, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

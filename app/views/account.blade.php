@extends('layouts.page')

@section('styles')
@parent
@stop()

@section('scripts')
@parent
{{HTML::script('js/Account.js') }}

@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container" id="page-main">
    <div class="row">
        <div class="col-md-7">
            <div class="acc_pane">

                <h5 class="text-right small">Hi! <?php echo ucwords($user->surname." ".$user->first_name)?></h5>

                <h3 class="pg-title">Account Balance</h3>
                <p class="large_font">
                    <strong class="<?php echo ($account->balance < 2000) ? "text-danger" : "theme-color"; ?>"><?php echo Olajuwon::format_money($account->balance) ?></strong>
                </p>
                   @if(empty($active_app))
                        <p class="text-muted">No active application</p>
                   @else
                   <h3 class="pg-title">Active Applications</h3>
                        <ul>
                            @foreach($active_app as $app)
                               <li>
                                    <a class="black-text" href="{{ URL::to("apply/".$app->code) }}">
                                        {{ $app->name }}
                                        <p class="small text-info">Starts on {{$app->start_date}}</p>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                   @endif

                   @if(!empty($past_app))
                        <h3 class="pg-title">Past Trainings</h3>
                        <ul>
                            @foreach($past_app as $app)
                               <li>
                                        {{ $app->name }}
                                        <p class="small">
                                            {{$app->start_date}}
                                            @if($app->end_date !== '0000-00-00')
                                                till {{  $app->end_date }}
                                            @endif
                                        </p>
                                </li>
                            @endforeach
                        </ul>
                    @endif
            </div>
        </div>

        <div class="col-md-5">
            <div class="acc_pane acc_pane_odd">

                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#by_voucher" data-toggle="tab">Voucher</a>
                    </li>
                    <li>
                        <a href="#by_atm" data-toggle="tab">ATM</a>
                    </li>
                    <li>
                        <a href="#by_deposit" data-toggle="tab">Teller</a>
                    </li>
                </ul>
                    {{--FORM ACTIONS RESULT --}}
                 <div>
                     <div class="loading hidden">
                         <img src="{{ asset('images/loading.gif') }}">
                     </div>
                         <div class="no-margin alert alert-success text-center hidden" id="account-success"></div>
                         <div class="no-margin alert alert-danger text-center hidden" id="account-error"></div>
                 </div>

                <div class="tab-content">
                    <div class="tab-pane fade in active" id="by_voucher">

                        <form role="form" action="#" method="post" id="form_voucher">
                            <div class="form-group">
                                <label for="voucher">Enter voucher digit <span class="small text-danger">(case sensitive)</span></label>
                                <input required id="voucher" class="form-control" type="text" name="voucher">
                            </div>
                            <button type="submit" class="large_button btn my-btn">Recharge</button>
                        </form>
                    </div>

                    <div class="tab-pane fade" id="by_atm">
                         <div class="alert alert-danger" style="font-size: 1.3em">
                            <span class="fa fa-pulse fa-spinner">&nbsp;</span> This feature is coming soon!
                        </div>
                    </div>
                    <div class="tab-pane fade" id="by_deposit">
                        <form role="form" action="#" method="post" id="form_deposit">
                           <div class="form-group">
                               <label for="tellerNumber">Enter Teller Number <span class="small text-danger">(case sensitive)</span> </label>
                               <input required id="tellerNumber" class="form-control" type="text" name="teller_number">
                               <label for="amount">Amount Deposited</label>
                               <select name="amount" id="amount">
                                   <option value="2500">2,500</option>
                                   <option value="5000">5,000</option>
                                   <option value="7500">7,500</option>
                                   <option value="10000">10,000</option>
                                   <option value="12500">12,500</option>
                                   <option value="15000">15,000</option>
                                   <option value="17500">17,500</option>
                               </select>
                           </div>
                           <button type="submit" class="large_button btn my-btn">Recharge</button>
                       </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
@stop()


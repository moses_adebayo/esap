@extends('layouts.page')

@section('styles')
@parent
{{HTML::style('css/events.css') }}
@stop()

@section('scripts')
@parent
{{HTML::script('js/Login.js') }}
{{HTML::script('js/SocialApi.js') }}
@stop()
@section('social_auth')
<script src="https://apis.google.com/js/client:platform.js" async defer></script>
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')

<div class="container" id="page-main">
    <div class="row">

        <div class="col-md-push-2 col-sm-8">
            <div class="login_form" id = "login_phase2">
                <div class="alert alert-info">
                    If You are new to this site, kindly <a href="{{url('registration') }}"><strong>Register</strong></a> to begin your registration process
                </div>

                {{Form::open(array('url'=>'user_login', 'id'=>'login_form_data')) }}
                <div class="loading hidden">
                    <img src="{{ asset('images/loading.gif') }}">
                </div>
                <?php
                if(isset($_REQUEST['email'])){
                    ?>
                    <div id="log-error" class="alert alert-danger">Invalid credentials</div>
                <?php
                }
                ?>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" name="email" value="<?php if(isset($_REQUEST['email'])){echo $_REQUEST['email'];} ?>">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
                <div class="form-group">
                    <input type="submit" class="btn my-btn" value="send">
                    <span>&nbsp;&nbsp;Are you new? {{HTML::link('registration', 'Register') }} </span>
                </div>
                {{Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop()


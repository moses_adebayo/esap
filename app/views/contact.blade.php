@extends('layouts.page')

@section('styles')
@parent
{{HTML::style('css/events.css') }}
@stop()

@section('scripts')
@parent
{{HTML::script('js/jquery.refineslide.js') }}
{{HTML::script('js/Slider.js') }}
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container" id="page-main">
    <div class="row">
        <div class="col-md-6">
            <h3 class="pg-title">Contact e-SAP</h3>
            <p>

            </p>
            <?php
            if(isset($response)){
                if($response['status']){
                    ?>
                    <div class="alert alert-success">
                        Message sent successfully
                    </div>
                <?php

                }else{
                    if(isset($response['msg'])){
                        ?>
                        <div class="alert alert-danger">
                            {{ $response['msg'] }}
                        </div>
                    <?php
                    }
                    ?>
                <?php
                }
            }
            ?>

        <form action="{{ route('send_mail') }}" method="post">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" value="<?php  if(isset($_POST['name']) && !$response['status']) echo $_POST['name'];?>">
                <p class="text-danger">
                    <?php
                    if(isset($form_errors['name'])) echo $form_errors['name'];
                    ?>
                </p>
            </div>
            <div class="form-group">
                <label for="email">E-mail</label>
                <input type="text" class="form-control" id="email" name="email" value="<?php if(isset($_POST['email']) && !$response['status']) echo $_POST['email']; ?>">
                <p class="text-danger">
                    <?php
                    if(isset($form_errors['email'])) echo $form_errors['email'];
                    ?>
                </p>
            </div>
            <div class="form-group">
                <label for="subject">Subject</label>
                <input type="text" class="form-control" id="subject" name="subject" value="<?php if(isset($_POST['subject']) && !$response['status']) echo $_POST['subject']; ?>">
                <p class="text-danger">
                    <?php
                    if(isset($form_errors['subject'])) echo $form_errors['subject'];
                    ?>
                </p>
            </div>
            <div class="form-group">
                <label for="message">Message</label>
                <textarea class="form-control" rows="3" id="message" name="message"><?php if(isset($_POST['message']) && !$response['status']) echo $_POST['message'] ?></textarea>
                <p class="text-danger">
                    <?php
                    if(isset($form_errors['message'])) echo $form_errors['message'];
                    ?>
                </p>
            </div>
            <div class="form-group">
                <input type="submit" class="btn my-btn" value="send">
            </div>
            {{ Form::token() }}
        </form>
    </div>

    <div class="col-md-5 col-sm-offset-1 acc_pane">
       <div class="col-md-6">
            <h4>Head Office</h4>
            <p>
                Km 2, Gbongan-Osogbo Expressway,
                Opposite Aregbesola Campaign Office,
                Osogbo, Osun State,
                Nigeria.
            </p>
            <h4>OAU Office</h4>
            <p>
                Havilah-Gold Conference Centre,
                Block A and B Attics, Postgraduate Hall,
                Obafemi Awolowo University, Ile-Ife,
                Nigeria.
            </p>
            <h4>Ile-Ife Office</h4>
            <p>
                No. 7, Zone 5, Parakin Layout,
                Behind Living Hope Hospital,
                Parakin, Ile-Ife,
                Osun State, Nigeria.
            </p>
            <h4>Ibadan Office</h4>
            <p>
                Suite 1, Al-Barakah Plaza,
                Behind Favors Supermarket,
                Rasheed Adesokan Way,
                New Bodija Estate, Ibadan,
                Oyo State, Nigeria.
            </p>

       </div>
       <div class="col-md-6">
           <h4> <span class="fa fa-phone"></span> <span class="sr-only">Phone-</span> +2348052275346</h4>
           <h4> <span class="fa fa-phone"></span> <span class="sr-only">Phone-</span> +2348036472392</h4>
           <h4> <span class="fa fa-phone"></span> <span class="sr-only">Phone-</span>  +2347085478682</h4>
           <h4> <span class="fa fa-phone"></span> <span class="sr-only">Phone-</span>  +2347059865948</h4>
         <br/>

         <h4><span class="fa fa-envelope-square"></span> E-mail Addresses</h4>
         <p>
            esapfoundations@gmail.com
            info@esapfoundations.org
            trainings@esapfoundations.org
            internship@esapfoundations.org
            careers@esapfoundations.org
         </p>
         <br/>
         <h4><span class="fa fa-whatsapp theme-color">&nbsp;</span><span class="sr-only">Whatsapp- </span> +2348052275346</h4>
         <h4><span class="fa fa-whatsapp theme-color">&nbsp;</span><span class="sr-only">Whatsapp- </span> +2348036472392</h4>
         <h4>BBM: 7E104788</h4>
         <h4><span class="fa fa-facebook-official facebook">&nbsp;</span><span class="sr-only">Facebook</span> /esapfoundations</h4>
         <h4><span class="fa fa-twitter-square twitter">&nbsp;</span><span class="sr-only">twitter</span>@esapfoundations</h4>

       </div>
    </div>
</div>



</div>
@stop()


@extends('layouts.page')

@section('styles')
    @parent
    {{HTML::style('css/events.css') }}
@stop()

@section('scripts')
    @parent
    {{HTML::script('js/Courses.js') }}
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
    <div class="container-fluid" id="page-main">
        <div class="row">
            <div class="col-md-7">
                <div class="acc_pane">
                    {{--fresh application--}}
                    @if($user['new'])

                        <h2 style="margin-top: 0;">{{ucwords($training['name'])}}</h2>
                        <div class="alert alert-info">
                            <h4>Available Courses</h4>
                        </div>
                        @foreach($training['courses'] as $course)
                            <div class="form-group">
                                <div class="course checkbox">
                                    <label>
                                        <input type="checkbox" class="select_course" id="{{$course['course_id'];}}" data-options='{"cost":"{{$course['reg_cost']}}", "name":"{{ $course['course_name'] }}", "course_id":"{{$course['course_id']; }}"}'>
                                        {{$course['course_name']}}
                                    </label>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="alert alert-info">
                            <h3>You applied for this training</h3>
                        </div>
                        <h2 class="text-info">{{ucwords($training['name'])}}</h2>

                        <h4>
                            <strong>Registration Number:&nbsp;</strong> {{ $user['reg_num'] }}
                        </h4>
                        <h3>
                            <span class="fa fa-calendar text-danger">&nbsp;</span>{{date("F j, Y", strtotime($training['start'])) ." <strong>-</strong> ". date("F j, Y", strtotime($training['end'])) }}
                        </h3>
                        <h3>
                            <span class="fa fa-home text-danger">&nbsp;</span><?php echo ucwords($training['location']); ?>
                        </h3>
                        <div class="margin-all padding-all text-center">
                            <a class="btn_link" href="<?php echo URL::to('slip/'.$user['application_id']); ?>" target="_blank">
                                <button class="btn my-btn btn-lg">Print slip</button>
                            </a>
                            &nbsp;&nbsp;
                            <a class="btn_link" href="<?php echo URL::to('apply/edit/'.$user['application_id']); ?>" >
                                <button class="btn btn-warning btn-lg">Update</button>
                            </a>
                        </div>
                    @endif
                </div>
            </div>

            <div class="col-md-4 col-md-offset-1">
                <div style="position: fixed; width: 400px;">
                    <div class="acc_pane acc_pane_odd">
                        <a target="_blank" href="{{ URL::to('timetable') }}" class="theme-color">
                            <button class="btn my-btn btn-sm">Training Time-Table</button>
                        </a>
                        <h3>Total Cost</h3>
                        <div class="alert alert-success">
                            <div id="cost"><?php if(!$user['new']){ echo $user['record'][0]->app_cost;}else { echo 0;} ?></div>
                        </div>
                        <h3>Courses Selected</h3>
                        <div id="courses_selected">
                            <?php
                            if(!$user['new']){
                            ?>
                            <div class="alert alert-success">
                                <ul>
                                    <?php
                                    foreach($user['record'] as $rec){
                                    ?>
                                    <li class="selected_course"><?php echo $rec->course_name ?></li>
                                    <?php
                                    } ?>
                                </ul>
                            </div>
                            <?php
                            }else{
                            ?>
                            <div class="alert alert-danger">
                                <?php
                                echo "No course selected yet";
                                }
                                ?>
                            </div>
                        </div>

                    </div>
                    <div class="padding-all text-center">
                        <button class="btn my-btn hidden large_button" id="apply" data-training_id = "<?php echo $training['training_id'] ?>">Apply</button>
                        <div id="app-success" class="margin-all alert alert-success hidden text-center"></div>
                        <div id="app-error" class="margin-all alert alert-danger hidden text-center"></div>
                    </div>
                    <div class="loading hidden">
                        <img src="{{ asset('images/loading.gif') }}">
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop()

@section('footer')
    <script>
        $("[data-toggle='tooltip']").tooltip();
    </script>
@stop()


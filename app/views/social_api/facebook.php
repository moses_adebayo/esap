<button class="btn btn-primary" id="fb-signupButton"><span class="fa fa-facebook-f">&nbsp;</span> Register with Facebook</button>
<script>
    $('#fb-signupButton').unbind('click').bind('click', function(){
        FB.login(function(response){
            // Handle the response object, like in statusChangeCallback() in our demo
            // code.
            statusChangeCallback(response);
        });
    });
    // This is called with the results from from FB.getLoginStatus().
    function statusChangeCallback(response) {

        if (response.status === 'connected') {
            // Logged into your app and Facebook.
            directAPI();
        } else if (response.status === 'not_authorized') {
            $('#reg-error').empty().html('Please log into this app ');
        } else {
            $('#reg-error').empty().html('Please log into into Facebook.');
        }
    }

    // This function is called when someone finishes with the Login
    // Button.  See the onlogin handler attached to it in the sample
    // code below.
    function checkLoginState() {
       //re-verify authentication
        FB.logout(function(response) {
        });

        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });
    }

    window.fbAsyncInit = function() {
        FB.init({
            appId      : '781546761930849',
            cookie     : true,  // enable cookies to allow the server to access
                                // the session
            xfbml      : true,  // parse social plugins on this page
            version    : 'v2.2' // use version 2.2
        });

//        FB.getLoginStatus(function(response) {
//            statusChangeCallback(response);
//        });

    };

//    // Load the SDK asynchronously
//    (function(d, s, id) {
//        var js, fjs = d.getElementsByTagName(s)[0];
//        if (d.getElementById(id)) return;
//        js = d.createElement(s); js.id = id;
//        js.src = "//connect.facebook.net/en_US/sdk.js";
//        fjs.parentNode.insertBefore(js, fjs);
//    }(document, 'script', 'facebook-jssdk'));

    // Here we run a very simple test of the Graph API after login is
    // successful.  See statusChangeCallback() for when this call is made.
    function directAPI() {
//        console.log('Welcome!  Fetching your information.... ');
        FB.api('/me', function(response) {
            var user = {'first_name' : response.first_name, 'surname': response.last_name, 'gender': response.gender, 'email': response.email};
            processUserRegWithSocial(user);
        });
    }
</script>

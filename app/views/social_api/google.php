<button class="btn btn-danger" id="signupButton"><span class="fa fa-google">&nbsp;</span> Register with Google</button>
<script>
    /* Executed when the APIs finish loading */

    function render() {
        // Additional params including the callback, the rest of the params will
        // come from the page-level configuration.
        var additionalParams = {
            'callback': signinCallback
        };

        // Attach a click listener to a button to trigger the flow.
        var signinButton = document.getElementById('signupButton');
        signinButton.addEventListener('click', function() {
            gapi.auth.signIn(additionalParams); // Will use page level configuration
        });
    }
    function signinCallback(authResult) {
        if (authResult['status']['signed_in']) {
            // Update the app to reflect a signed in user
            // Hide the sign-in button now that the user is authorized, for example:
//            document.getElementById('signinButton').setAttribute('style', 'display: none');
//            console.log(authResult['status']['signed_in'])
            gapi.client.load('plus', 'v1', apiClientLoaded);

        } else {
            // Update the app to reflect a signed out user
            // Possible error values:
            //   "user_signed_out" - User is signed-out
            //   "access_denied" - User denied access to your app
            //   "immediate_failed" - Could not automatically log in the user
            $('#reg-error').empty().html('Sign-in state: ' + authResult['error']);
        }
    }
    function apiClientLoaded(){
        gapi.client.plus.people.get({userId: 'me'}).execute(handleEmailResponse);
    }
    function handleEmailResponse(resp) {
        var primaryEmail;
        for (var i=0; i < resp.emails.length; i++) {
            if (resp.emails[i].type === 'account') primaryEmail = resp.emails[i].value;
        }
        var user = {'first_name' : resp.name.givenName, 'surname' : resp.name.familyName, 'gender' : resp.gender, 'email' : primaryEmail};
        processUserRegWithSocial(user);
    }

    render();
</script>

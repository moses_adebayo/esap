@extends('layouts.page')

@section('styles')
@parent
{{HTML::style('css/events.css') }}
@stop()

@section('scripts')
@parent
{{HTML::script('js/Courses.js') }}
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container-fluid" id="page-main">
    <div class="row">
        <div class="col-md-7">

            <div class="acc_pane">
                <?php
                //            var_dump($user);
                //            var_dump($_SESSION);
                //                                        var_dump($training);
                //                var_dump($training['courses']);
                ?>

                <?php
                //fresh application
                if($user['new']){
                    ?>
                    <div class="alert alert-danger text-center">
                        <p>Oops! Registration for this training has been closed.</p>
                    </div>
                    <h2>{{ ucwords($training['name']) }}</h2>
                    <h2>
                        <span class="fa fa-home">&nbsp;</span><?php echo ucwords($training['location']); ?>
                    </h2>
                    <span class="large_font"><?php echo $training['start']; ?></span><span>&nbsp;&nbsp;till&nbsp;&nbsp;</span><span class="large_font"><?php echo $training['end']; ?></span>
                <?php
                }else{
                    //                var_dump($user);
                    //                    var_dump($training);
                    ?>
                    <div class="alert alert-info">
                        <h3>You applied for this training</h3>
                    </div>
                    <h2>{{ ucwords($training['name']) }}</h2>

                    <p>
                        <strong>Regisration Number:&nbsp;</strong> {{ $user['reg_num'] }}
                    </p>
                    <p>

                        <span class="large_font"><?php echo $training['start']; ?></span><span>&nbsp;&nbsp;till&nbsp;&nbsp;</span><span class="large_font"><?php echo $training['end']; ?></span>
                    </p>
                    <h2>
                        <span class="fa fa-home">&nbsp;</span><?php echo ucwords($training['location']); ?>
                    </h2>
                    <div class="margin-all padding-all text-center">
                        <button class="btn my-btn large_button"><a class="btn_link" href="<?php echo URL::to('slip/'.$user['application_id']); ?>" target="_blank">Print slip</a></button>
                    </div>
                <?php
                }
                ?>
            </div>

        </div>

        <div class="col-md-4 col-md-offset-1">
            <div class="acc_pane acc_pane_odd">
                @if(!$user['new'])
                <h3>Total Cost</h3>
                <div class="alert alert-success">
                    <div id="cost">{{ $user['record'][0]->app_cost;}}</div>
                </div>
                <h3>Courses Selected</h3>
                <div id="courses_selected">

                    <div class="alert alert-success">
                        <ul>
                            @foreach($user['record'] as $rec)
                                <li>{{ $rec->course_name }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            @else
            <div class="alert">
                <p>
                        e-SAP is an acronym for Extramural Skills Acquisition Programme. e-SAP Foundations is a Non-Governmental Organization with a mandate and an assignment to show every Nigerian youth, especially those who care to listen the way out of the shackles of unemployment that have bound millions of people within and outside this country. Our message is very simple and direct: “YOU DON’T HAVE TO TASTE UNEMPLOYMENT ONE DAY OF YOUR LIFE!” Our campaign is against unemployment.

                </p>
            </div>
            @endif

        </div>
        <div class="padding-all text-center">
            <button class="btn my-btn hidden large_button" id="apply" data-training_id = "<?php echo $training['training_id'] ?>">Apply</button>
            <div id="app-success" class="margin-all alert alert-success hidden text-center"></div>
            <div id="app-error" class="margin-all alert alert-danger hidden text-center"></div>
        </div>
        <div class="loading hidden">
            <img src="{{ asset('images/loading.gif') }}">
        </div>

    </div>
</div>

</div>
@stop()


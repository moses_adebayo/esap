@extends('layouts.page')

@section('styles')
@parent
{{HTML::style('css/events.css') }}
@stop()

@section('scripts')
@parent
{{HTML::script('js/SocialShare.js') }}
{{HTML::script('js/training.js') }}
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container" id="page-main">
    <div class="col-md-12">
        <?php
        //check for active training
        if(sizeof($training) == 0){
            echo "<h2 class='text-muted'>No Active training at the moment";
        }else{
            ?>
            <ul class="event-list">

                <?php
                foreach($training as $t){
                    $format_date = Olajuwon::format_date($t->start_date);
                    ?>
                    <li>
                        <time datetime="<?php echo $t->start_date ?>">
                            <span class="day"><?php echo $format_date['day']; ?></span>
                            <span class="month"><?php echo $format_date['month']; ?></span>
                            <span class="year"><?php echo $format_date['year']; ?></span>
                        </time>
                        <div class="info">
                            <h1 class="title"><?php echo $t->name ?></h1>
                            <p class='desc small text-muted'>Training holds between  </p>
                            <h2>{{date("F j, Y", strtotime($t->start_date)) ." <strong>-</strong> ". date("F j, Y", strtotime($t->end_date))}}</h2>
                            <div class="title">
                                <a href="{{URL::to('apply/'.$t->code)}}">
                                    <button class="btn btn-warning">Apply</button>
                                </a>
                                <?php if(!is_null($t->timetable)){
                                ?>
                                 <a target="_blank" href="{{ URL::asset('pdf/' .$t->timetable.'.pdf') }}">
                                    <button class="btn btn-default">View Time-Table</button>
                                 </a>
                                <?php
                                } ?>
                             </div>
                        </div>
                        <div class="social">
							<ul>
								<li class="facebook" style="width:33%;" data-url = "{{URL::to('apply/'.$t->code)}}"><a href="#facebook" >&nbsp;&nbsp;&nbsp;<span class="fa fa-facebook"></span></a></li>
								<li class="twitter" style="width:34%;" data-url = "{{URL::to('apply/'.$t->code)}}"><a href="#twitter">&nbsp;&nbsp;&nbsp;<span class="fa fa-twitter"></span></a></li>
								<li class="google-plus" style="width:33%;" data-url = "{{URL::to('apply/'.$t->code)}}"><a href="#google-plus">&nbsp;&nbsp;&nbsp;<span class="fa fa-google-plus"></span></a></li>
							</ul>
						</div>
                    </li>
                <?php
                }
                ?>
            </ul>
        <?php
        }
        ?>
    </div>
</div>
@stop()


@extends('layouts.page')

@section('styles')
@parent
{{HTML::style('css/events.css') }}
@stop()

@section('scripts')
@parent
{{HTML::script('js/jquery.refineslide.js') }}
{{HTML::script('js/Slider.js') }}
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container" id="page-main">
    <div class="row">
        <div class="col-md-6">
            <h3>About eSAP</h3>

            <p>
                e-SAP is an acronym for Extra-mural Skills Acquisition Programme. e-SAP Foundations is a Social Entrepreneurship Organization with a mandate to show every Nigerian youth the way out of the grip of UNEMPLOYMENT that has bound millions of people within and outside the country. Our message is very simple and direct: “YOU DON’T HAVE TO TASTE UNEMPLOYMENT ONE DAY OF YOUR LIFE!” Our campaign is against unemployment! The Target audiences of e-SAP are undergraduates and fresh graduates.
            </p>
            <h3>Vision Statement</h3>
            <p>
                To totally eradicate the pervasive “unemployment syndrome” among Nigerian Graduates by 2025.
            </p>

            <h3>Mission Statement</h3>
            <p>
                To supply, to Undergraduates and young graduates, a mentality and mindset of personal value building and self-development through early acquisition of critical value-adding skills, beyond academics; and to help them (undergraduates especially) acquire such skills while they are still in school, such that at graduation, their values have heightened greatly beyond that of contemporary Nigerian graduates, who are mostly jobless or unemployed BECAUSE THEY KNOW NOTHING ELSE APART FROM WHAT THEY WERE TAUGHT IN THE CLASSROOM.
            </p>

            <h3>Our Slogan</h3>
            <ol>
                <li>e-SAP: Becoming more valuable by adding skills.</li>
                <li>Unemployment is not real; it is just a wrong mindset!</li>
                <li>e-SAP: Overcoming unemployment ahead of graduation.</li>
            </ol>

            <p class="small">
                <strong>Please Note:</strong> We are not against the pursuit of academic excellence. We are only saying, “Don’t let academic certificate be your only achievement for the four or five years you’ll spend in school; the 21st century students cannot afford not to multi-task their brains”.

            </p>

            <h3>Aims & Objectives</h3>
            <p>
                <strong>1.</strong>
                To bring various extra-curriculum skills that are in high demand in the labour market to campuses at an affordable cost or no cost at all, so that students don’t have to wait until after graduation before they begin to pursue such skill and pay hugely to acquire them as this is the time that they have time.
            </p>
            <p>
                <strong>2.</strong>
                To create various platforms where students can be trained and equipped to be ‘job ready’ in order to function effectively in any organization without having to queue in the job market after graduation.
            </p>
            <p>
                <strong>3.</strong>
                To help students bag ‘working experiences’ concurrently while they still pursue academic excellence since most employers would always ask for ‘working experience’, a dilemma which most graduates don’t know how to deal with.
            </p>
            <p>
                <strong>4.</strong>
                To make students valuable materials to any employer/organization ever before they leave school.
            </p>
            <p>
                <strong>5.</strong>
                To fashion a synergy between academics and skills acquisition.
            </p>
            <p>
                <strong>6.</strong>
                To help students discover who they really are so that their efforts in life can be channeled towards the direction (career) that will optimally maximize their chances of success in life.
            </p>
            <p>
                <strong>7.</strong>
                To help Nigerian students imbibe correct reason for pursuing academic excellence
            </p>
            <p>
                <strong>8.</strong>
                To help Nigerian student come to a place where they can stand shoulders up on any ground or field with any other students/graduate in other parts of the world, without inferiority, incompetency, or any such things.
            </p>
            <p>
                <strong>9.</strong>
                To create avenues where students can build their confidence and abilities to deliver on organizational tasks and job functions, ahead of graduation or job entry.
            </p>
            <p>
                <strong>10.</strong>
                To disciple Nigerian Undergraduates/graduate to become ‘idea generators’ rather than being dependents or lay-abouts.
            </p>
            <p>
                <strong>11.</strong>
                To tutor Nigerian youths on personal development and self-branding in order to attract and keep their dream jobs.
            </p>
            <p>
                <strong>12.</strong>
                To guide students on the pathway to financial success and self-dependence.
            </p>
            <p>
                <strong>13.</strong>
                To run students through the acquisition of highly needed leadership qualities for life and career success, while still in school.
            </p>
            <p>
                <strong>14.</strong>
                To make students sellable and have no problem looking for jobs.
            </p>
            <p>
                <strong>15.</strong>
                To help students gain financial freedom and learn critical financial principles while still in school
            </p>
            <p>
                <strong>16.</strong>
                To keep ‘loading’ students up with values and requisite skills that will make it difficult or impossible to ignore or replace them in any place of function they may find themselves after graduation.
            </p>
            <p>
                <strong>17.</strong>
                To make students stand out academically while unleashing their hidden potentials right from school
            </p>
            <p>
                <strong>18.</strong>
                To inculcate in them the virtue of self-reliance and job creations while on campus.
            </p>
            <p>
                <strong>19.</strong>
                To make students learn as many skills as possible irrespective of what they are studying or studied.
            </p>


        </div>

        <div class="col-md-4 col-sm-offset-2">
            {{-- INFORMATION PANEL --}}
            @include('utilities.info_panel')
        </div>
    </div>



</div>
@stop()


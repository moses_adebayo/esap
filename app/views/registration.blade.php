@extends('layouts.page')

@section('styles')
@parent
{{HTML::style('css/events.css') }}
@stop()

@section('scripts')
@parent
{{HTML::script('js/Registration.js') }}
{{HTML::script('js/SocialApi.js') }}
{{HTML::script('js/state_lga.js') }}
{{HTML::script('js/nig_schools.js') }}

@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<script src="https://apis.google.com/js/client:platform.js" async defer></script>

<div class="container-fluid" id="page-main">
    <div class="row">
        <div class="col-md-7 content">
            <div class="my-heading">
                <h3 class="pg-title">eSAP Registration</h3>
            </div>

            <div class="loading hidden">
                <img src="{{ asset('images/loading.gif') }}">
            </div>

            <div class="padding-all">
                <div class="text-success text-center" id="reg-success"></div>
                <div class="text-danger text-center" id="reg-error"></div>
            </div>

            {{Form::open(array('register', 'id'=>'reg_form', 'class'=>'form-horizontal', 'role'=>'form')) }}
            <div class="form-group">
                <label for="email" class="col-sm-3">Email</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="email"  name="email" placeholder="Enter a valid email address">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-3">Password</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" id="password" name="password">
                </div>
            </div>
            <div class="form-group">
                <label for="c_pswrd" class="col-sm-3">Confirm password</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" id="c_pswrd" name="c_password">
                </div>
            </div>
            <div class="form-group">
                <label for="surname" class="col-sm-3">Surname</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="surname" name="surname">
                </div>
            </div>
            <div class="form-group">
                <label for="o_names" class="col-sm-3">Other names</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="o_names" name="first_name" placeholder="first & middle name">
                </div>
            </div>
            <div class="form-group">
                <label for="gender" class="col-sm-3">Gender</label>
                <div class="col-sm-9">
                    <select name="gender">
                        <option value="m">Male</option>
                        <option value="f">Female</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="p_number" class="col-sm-3">Phone No</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="p_number" name="phone_num">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3">Country</label>
                <div class="col-sm-9">
                    <select name="country">
                        <option value="nig">Nigeria</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3">State</label>

                <div class="col-sm-9">
                    <select style="cursor: pointer;" id="state" name="state">
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3">L.G.A</label>
                <div class="col-sm-9">
                    <select style="cursor: pointer;" id="lga" name="lga">
                    </select>
                </div>
                <script language="javascript">
                    populateState("state", "lga");
                </script>
            </div>
            <div class="form-group">
                <label class="col-sm-3">Address</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="address">
                </div>
            </div>
            <hr/>
            <div class="form-group">
                <label for="inst" class="col-sm-3">Institution Type</label>
                <div class="col-sm-9">
                    <select style="cursor: pointer;" id="inst_type" name="institution_type">
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="inst" class="col-sm-3">Institution Name</label>
                <div class="col-sm-9">
                    <select style="cursor: pointer;" id="inst_name" name="institution_name">
                    </select>
                </div>
                <script language="javascript">
                    populateSchoolType("inst_type", "inst_name");
                </script>
            </div>

            <div class="form-group">
                <label for="inst" class="col-sm-3">Level</label>
                <div class="col-sm-9">
                    <select name="level">
                        <option value="100">100</option>
                        <option value="200">200</option>
                        <option value="300">300</option>
                        <option value="400">400</option>
                        <option value="500">500</option>
                        <option value="600">600</option>
                        <option value="-1">Not Applicable</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="inst" class="col-sm-3">Course of study</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="course_study">

                </div>
            </div>
            <div class="form-group">
                <input type="submit" class="btn my-btn col-sm-offset-3" value="Submit">
            </div>
            {{Form::close() }}
        </div>

        <div class="col-md-4 col-md-offset-1">
            {{-- INFORMATION PANEL --}}
            @include('utilities.info_panel')
        </div>
    </div>
    </div>
    @stop()


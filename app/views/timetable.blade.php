@extends('layouts.page')

@section('styles')
@parent
@stop()

@section('scripts')
@parent
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container" id="page-main">
    <div class="row">
        <div class="col-md-8">
            <div class="acc_pane">
                <div class="info">
                     <h1 class="text-uppercase">AVAILABLE TIME-TABLE</h1>

                      <p class="alert alert-info">
                        List of available time-table, click to download any correspoding to the training you applied for
                      </p>

                      <ol>
                          <li>
                              <a href="{{ URL::asset('pdf/oau6.pdf') }}">
                                  Obafemi Awolowo University Season 6
                              </a>
                          </li>
                          <li>
                              <a href="{{ URL::asset('pdf/oau4.pdf') }}">
                                  Obafemi Awolowo University Season 4
                              </a>
                          </li>
                       <li>
                         <a href="{{ URL::asset('pdf/s1_s1.pdf') }}">
                         Federal Polytechnic, Ede Season 1
                         </a>
                        </li>
                       <li>
                         <a href="{{ URL::asset('pdf/oau3.pdf') }}">
                         Obafemi Awolowo University Season 3
                         </a>
                        </li>
                      </ol>
                </div>
                </div>
  </div>

        <div class="col-md-4">
            @include('utilities.info_panel')
        </div>
 </div>

</div>
@stop()


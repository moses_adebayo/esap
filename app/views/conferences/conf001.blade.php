@extends('layouts.page')

@section('styles')
@parent
{{HTML::style('css/events.css') }}
@stop()

@section('scripts')
@parent
{{HTML::script('js/Conferences.js') }}
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container" id="page-main">
    <div class="row">
        <div class="col-md-8">
            <div class="acc_pane">
                <div class="info">
                     <h1 class="text-uppercase">e-SAP Students' Conference</h1>
                     {{--<h4>--}}
                        {{--<span class="fa fa-calendar text-warning">&nbsp;&nbsp;</span> 28th of Mar. 2015--}}
                     {{--</h4>--}}
                     {{--<h4>--}}
                        {{--<span class="fa fa-clock-o text-danger">&nbsp;&nbsp;</span> 9am prompt--}}
                     {{--</h4>--}}
                     {{--<h4>--}}
                        {{--<span class="fa fa-home text-info">&nbsp;&nbsp;</span> Oduduwa Hall--}}
                     {{--</h4>--}}
                      <p class="text-muted text-center">
                        Why do people graduate and cannot get jobs? What should yo do so as not to join the unemployment queue? Attend e-SAP Students’ Conference to learn how not to enter the net of unemployment when you graduate. If you are a graduate and you are still jobless, you need to be at this conference. Again, if you know anyone who has issue with unemployment, you should not hesitate to tell them about this conference because it will be a major help and a long awaited assistance for them. Register for the conference today!
                      </p>
                          <button class="btn btn-lg my-btn2 pull-right center-block">Coming soon</button>
                     <div class="conf-details">
                          <div class="col-md-6">
                            <div class="conf-img" id="fac1">
                            <div class="clear-fix"></div>
                                <p><strong>OPEYEMI</strong> Awoyemi<br/><em>MD, Jobberman</em></p>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="conf-img" id="fac2">
                                <p><strong>ADEDOYIN</strong> Matthew<br/><em>Director e-SAP</em></p>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="conf-img" id="fac3">
                                <p><strong>ODENIYI</strong> Kehinde<br/><em>University of Reading, UK</em></p>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="conf-img" id="fac4">
                                <p><strong>ADETILOYE</strong> Dayo<br/><em>CEO, 100/5 Academy</em></p>
                            </div>
                          </div>
                     </div>
                     <div class="clearfix"></div>
                </div>
                </div>


  </div>


        <div class="col-md-4">
            @include('utilities.info_panel')
        </div>
 </div>

</div>
@stop()


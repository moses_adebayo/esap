@extends('layouts.page')

@section('styles')
@parent
{{HTML::style('css/events.css') }}
@stop()

@section('scripts')
@parent
{{HTML::script('js/jquery.refineslide.js') }}
{{HTML::script('js/Slider.js') }}
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container" id="page-main">
    <div class="row">
        <div class="col-md-6">
            {{--<h3 class="pg-title">Become  <span class="text-lowercase">e</span>-SAP Volunteer</h3>--}}

        <iframe src="https://docs.google.com/forms/d/17CIGiyQJCYQd876IIlhS0aK9HW_txeqRXg-xEBNBVKc/viewform?embedded=true" width="650" height="1720" frameborder="0" marginheight="0" marginwidth="0"><span class="fa fa-spinner fa-pulse"></span> </iframe>
        </div>

        <div class="col-md-4 col-sm-offset-2 acc_pane">
        <h3 class="text-uppercase pg-title">Join the Team</h3>
           <p>
                The vision and task of eradicating unemployment in Nigeria is not, and cannot be the job of a single person or institution. There is hardly anybody you’ll ask who doesn’t know someone looking for job somewhere!</p>
             <p>
             Since unemployment is so widespread and pervasive, all hands must be on deck in fighting the monster off.
           </p>
        </div>
    </div>

</div>



</div>
@stop()


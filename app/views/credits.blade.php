@extends('layouts.page')

@section('styles')
@parent
{{HTML::style('css/events.css') }}
@stop()

@section('scripts')
@parent

@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container" id="page-main">
    <div class="text-center">


        <h3 class="pg-title">OUR CREDITS</h3>

        <?php
        if($data['status']){

        ?>
    </div>

    <ul class="credits list-inline">
        <?php
        foreach($data['info'] as $d){
            ?>
            <li>
                <h2><span class="fa fa-user">&nbsp;</span><?php echo $d->name ?></h2>
                <p class="credits-text"><?php  echo $d->message;?></p>
            </li>
        <?php
        }
        ?>
    </ul>
    <?php
        echo $data['info']->links();
    }
    ?>


</div>
@stop()


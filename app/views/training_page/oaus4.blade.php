@extends('layouts.page')

@section('styles')
@parent
{{HTML::style('css/events.css') }}
@stop()

@section('scripts')
@parent
{{HTML::script('js/Conferences.js') }}
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container" id="page-main">
    <div class="row">
        <div class="col-md-8">
            <div class="acc_pane">
                <div class="info">
                     <h1 class="text-uppercase text-success">OAU Season 4</h1>
                     <h3>Registration Procedure</h3>
                      <p class="text-muted text-center">
                        <stong>Note:&nbsp;</stong>This registration procedure does not apply to the students of Obafemi Awolowo University, Ile-Ife. Only point number 7 applies to OAU students.
                      </p>
                     <div class="conf-details">
                         <ol>
                            <li>
                                Visit our website: <a href="{{route('homepage')}}">e-SAP</a> to download the timetable (or send a
                                mail to esapfoundations@gmail.com to request the timetable) in order to make
                                informed decisions about the courses to enrol for. This is essentially important for
                                you not to register for two courses that are clashing on the time-table.
                            </li>
                             <li>
                                 Pay a one-time, non-refundable registration fee (N1,500). This is required and will
                                 cater for your training souvenirs and kits.
                             </li>
                             <li>
                                 Pay the tuition/course fee (5,000 per course) equivalent to the number of courses
                                 you wish to register for.
                             </li>
                             <li>
                                 All payments are to be made using the following account details:
                                 <blockquote>
                                     <strong>Bank: Diamond Bank</strong><br/>
                                     <strong>Account Number: 0045129260</strong><br/>
                                     <strong>Account Name: Havilah-Gold Technologies</strong><br/>
                                 </blockquote>
                             </li>
                             <li>
                                 Send the following details to <strong>07085478682</strong>
                                 <blockquote>
                                     <strong>Name, Phone number, Amount paid, Course(s) paid for, Date of payment, State of residence</strong><br/>
                                 </blockquote>
                             </li>
                             <li>
                                 Our representative will call you within 24 hours to acknowledge your payment and
                                 notify you of your registration status.
                             </li>
                             <li>
                                 All participants within Obafemi Awolowo University community should go to
                                 designated registration points to pick their registration forms.
                             </li>
                             <li>
                                 For enquiries, contact any of these numbers: 07059865948, 07085478682,
                                 08036472392, 08052275346.
                             </li>
                         </ol>
                     </div>
                     <div class="clearfix"></div>
                </div>
                </div>


  </div>


        <div class="col-md-4">
{{--            @include('utilities.info_panel')--}}
           <div id="info_content" class="text-center" style=" padding: 4px 7px 20px; -webkit-box-shadow: 2px 2px 2px #eee;
           -moz-box-shadow: 2px 2px 2px #eee ;
           box-shadow: 2px 2px 2px #eee ;">
               <br/>
               <h3 class="text-center">OAU Season Four timetable is out</h3>
               <a href="{{asset('pdf/oau4.pdf')}}">
                <button class="btn btn-info"><span class="fa fa-file-pdf-o">&nbsp;&nbsp;</span>Download Timetable</button>
               </a>
           </div>
        </div>
 </div>

</div>
@stop()



@extends('layouts.page')

@section('styles')
@parent
{{HTML::style('css/events.css') }}
@stop()

@section('scripts')
@parent
{{HTML::script('js/jquery.refineslide.js') }}
{{HTML::script('js/Slider.js') }}
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container" id="page-main">
    <div class="col-md-12">
        <?php
        if(sizeof($data) != 0){
            foreach($data as $d){
                ?>
                <div class="small_div">
                    <div class="title">
                        <h4 class="text-center">eSAP Voucher</h4>
                        <p class="small text-center"><em>Becoming more valuable by adding skills</em></p>
                    </div>
                    <p style="margin: 0; font-size: 1.6em">
                        <strong style="font-family: "lucida console", monospace">Code:&nbsp;</strong><?php echo $d->code; ?></p>
                     <p style="font-size: 1.6em">
                        <strong>Price:&nbsp;</strong><em>#1,500</em>
                    </p>
                    <ol class="small">
                         <li>Go to www.esapfoundations.org</li>
                         <li>Create an account & load voucher</li>
                         <li>Apply for training under OAU (not another school)</li>
                         <li>Print out your slip</li>
                    </ol>
                    <p class="small text-center">
                        <em>
                            Code is case sensitive. Keep your card safe
                        </em><br/>
                        <em>Helpline: 08036472392</em>
                    </p>

                </div>
            <?php
            }
        }else{
            echo "<h2>All voucher are printed </h2>";
            ?>
            <button class="large_button btn my-btn2"><a href="{{URL::to('admin')}}" class="white-text">Admin</a> </button>
        <?php
        }
        ?>
    </div>
</div>
@stop()


@extends('layouts.admin_page')

@section('scripts')
@parent
{{--    {{ HTML::style('css/styles1.css')}}--}}
 {{HTML::script('js/constants.js') }}
@stop()
@section('styles')
@parent
{{ HTML::style('css/datatable.css')}}
{{ HTML::style('css/dataTables.tableTools.css')}}

@stop()

<?php
/**
 * Created by PhpStorm.
 * User: olajuwon
 * Date: 3/5/2015
 * Time: 8:07 PM
 */
?>
@section('content')
<div class="container">
<div class="row">
<br/><br/><br/>
@if(sizeof($users) !== 0)
 <table class="table table-bordered">
    <thead>
        <tr>
            <th>Name</th>
            <th>Phone Number</th>
            <th>Email</th>
            <th>Institution Name</th>
            <th>State</th>
            <th>Course of Study</th>
            <th>Level</th>
        </tr>
    </thead>
    <tbody>
       @foreach($users as $user)
       <tr>
           <td>{{ ucwords($user->surname. ' '.$user->first_name) }}</td>
           <td>{{ $user->phone_num }}</td>
           <td>{{ $user->email }}</td>
           <td>{{ ucwords($user->institution_name) }}</td>
           <td>{{ ucwords($user->state)}}</td>
           <td>{{ ucwords($user->course_study)}}</td>
           <td>{{ ($user->level == -1) ? 'not applicable' : $user->level}}</td>
       </tr>
       @endforeach
    </tbody>
 </table>
@else
   <p class="alert-danger"> No student yet</p>
@endif
</div>
{{HTML::script('js/datatable.js') }}
{{HTML::script('js/dataTables.tableTools.js') }}
{{HTML::script('js/report.js') }}

<script>
doTable();
 </script>
</div>
 @stop
{{--    {{ HTML::style('css/styles1.css')}}--}}
 {{HTML::script('js/constants.js') }}

<style>
   *{
   font-family: "Segoe UI", "Helvetica Neue", Helvetica, Arial, sans-serif
   }
   .alert-danger{
   color: #dd0000;
   }
   th{
    text-align: left;
   }

</style>

{{ HTML::style('css/datatable.css')}}
{{ HTML::style('css/dataTables.tableTools.css')}}

{{ HTML::style('css/datatable.css')}}
{{ HTML::style('css/dataTables.tableTools.css')}}

<?php
/**
 * Created by PhpStorm.
 * User: olajuwon
 * Date: 3/5/2015
 * Time: 8:07 PM
 */

?>

@if(!$data['empty'])

 <table class="table table-bordered">
    <thead>
        <tr>
        <th>Name</th>
        <th>Reg Number</th>
        <th>Course</th>
        <th>Phone number</th>
        </tr>
    </thead>
    <tbody>
       @foreach($data['students'] as $student)
       <tr>
       <td>{{ $student['name'] }}</td>
       <td>{{ $student['reg_num'] }}</td>
       <td>{{ $student['course_name'] }}</td>
       <td>{{ $student['phone_num']}}</td>
       </tr>
       @endforeach
    </tbody>
 </table>
@else
   <p class="alert-danger"> No student yet</p>
@endif

 {{HTML::script('js/jquery.min.js') }}
{{HTML::script('js/datatable.js') }}
{{HTML::script('js/dataTables.tableTools.js') }}
{{HTML::script('js/report.js') }}

<script>
doTable();
 </script>
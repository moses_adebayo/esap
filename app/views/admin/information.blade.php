@extends('layouts.admin_page')

@section('styles')
@parent

@stop()

@section('scripts')
@parent

@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container-fluid" id="page-main">
    <?php
        $show_history = false;
    if(isset($_GET['page'])){
       $show_history = true;
    }
    ?>
    <div class="row">
        <div class="col-md-4">
            <div class="acc_pane">
                <ul class="list-unstyled">
                    <li class="list">
                        <a href="{{ route('admin_panel') }}">Generate Voucher</a>
                    </li>
                    <li class="list">
                         <a href="{{URL::to('teller_form') }}">Enter Teller </a>
                     </li>
                    <li class="list">
                        <a href="{{ route('form_training') }}">Training </a>
                    </li>
                    <li class="list">
                        <a href="{{ route('form_conference') }}">Conference </a>
                    </li>
                    <li class="list active">
                        <a href="{{ route('new_info') }}">Announcement </a>
                    </li>
                     <li class="list">
                        <a href="{{ route('all-users') }}">All Users </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-md-7 col-md-offset-1">
            <div class="acc_pane acc_pane_odd">

                <ul class="nav nav-tabs">
                    <li class="<?php echo ($show_history) ? "" : "active" ?>">
                        <a href="#create" data-toggle="tab">Create</a>
                    </li>
                    <li class="<?php echo ($show_history) ? "active" : "" ?>">
                        <a href="#history" data-toggle="tab">History</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="<?php echo ($show_history) ? "tab-pane fade in" : "tab-pane fade in active" ?>" id="create">
                        <div>
                            @if(isset($response))
                                @if($response['status'])
                                    <div class="alert alert-success">
                                        Information posted successfully
                                    </div>
                                @else
                                     <div class="alert alert-danger">
                                         Information not sent, try again
                                     </div>
                                @endif
                            @endif
                            <form action="{{route('post_info')}}" method="post" class="form-horizontal" role="form">
                                {{Form::token() }}
                                <div class="form-group">
                                    <label for="title" class="col-sm-3">Title</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="title"  name="title" required>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="message" class="col-sm-3">Message</label>
                                    <div class="col-sm-9">
                                        <textarea name="message" class="form-control" required></textarea>
                                        <div>
                                            <br/>
                                            <input type="submit" class="btn my-btn form-control" value="Post">
                                        </div>
                                    </div>
                                </div>
                                {{ Form::close() }}

                        </div>
                    </div>

                    <div class="<?php echo ($show_history) ? "tab-pane fade in active" : "tab-pane fade" ?>"" id="history">
                        @if(sizeof($info) == 0))
                        <h2 class="emptyRecord">No information yet</h2>
                        @else
                        @foreach($info as $i)
                        <div class="training_list">
                             <h4>{{  $i->title; }} </h4>
                            <p>
                                {{ $i->description }}
                            </p>
                                <form action="{{route('delete_info')}}" method="post" class="form-horizontal" role="form">
                                {{ Form::token() }}
                                <input type="hidden" name="info_id" value="{{ $i->id }}">
                                <input type="submit" class="btn my-btn" value="Delete"/>
                                {{Form::close() }}
                        </div>
                        @endforeach
                            {{ $info->links(); }}

                        @endif
                    </div>


                </div>


            </div>

        </div>

    </div>

</div>
@stop()


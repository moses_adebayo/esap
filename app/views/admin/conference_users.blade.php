<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    @section('styles')
    {{ HTML::style('css/bootstrap.min.css')}}
    {{ HTML::style('fonts/fonts.css')}}
    {{ HTML::style('fonts/font-awesome.min.css')}}
    {{ HTML::style('css/extra-small-devices.css')}}
    {{ HTML::style('css/editBootstrap.css')}}
    {{ HTML::style('css/styles1.css')}}

    @show()

    @section('scripts')
    {{HTML::script('js/jquery.min.js') }}
    {{HTML::script('js/bootstrap.min.js') }}
    {{HTML::script('js/Page.js') }}

    @show()

</head>

<body style="padding-top: 0">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="acc_pane acc_pane_odd">

                @if(empty($data))
                <h2>No registered student yet!</h2>
                @else
                <div class="text-center">
                    <h2 class="text-center">{{ $data['training_name'] }}</h2>
                    <p class="small text-center">Date Printed: {{ date("l, F j, Y") }} </p>
                </div>

                <div>
                    @foreach($data['courses'] as $c)
                        <h3>{{ $c['course_name'] }}</h3>
                    <ol>
                        @foreach($c['students'] as $reg=>$name)
                          <li>{{ $name }}</li>
                        @endforeach

                    </ol>
                    @endforeach
                </div>
                @endif
                <div class="text-center small">
                    Date Printed: {{ date("l, F j, Y") }}
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    window.print();
</script>
</body>
</html>

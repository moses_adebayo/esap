<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    @section('styles')
    {{ HTML::style('css/bootstrap.min.css')}}
    {{ HTML::style('fonts/font-awesome.min.css')}}
    {{ HTML::style('css/extra-small-devices.css')}}
    {{ HTML::style('css/editBootstrap.css')}}
    {{ HTML::style('css/styles1.css')}}

    @show()

    @section('scripts')
    {{HTML::script('js/jquery.min.js') }}
    {{HTML::script('js/bootstrap.min.js') }}
    {{HTML::script('js/Page.js') }}

    @show()

</head>

<body style="padding-top: 0">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="acc_pane acc_pane_odd">

            @if(!empty($data))
                <h2 class="text-center">{{ $data[0]->name }}</h2>
                <div class="col-md-6">
                    <select class="sel_course form-control" id="change_course">
                        <option data-course-id= -1 data-training-id= {{$data[0]->training_id}} >ALL COURSES</option>
                    @foreach($data as $course)
                        <option data-course-id= {{$course->course_id}} data-training-id="{{ $course->training_id }}">{{ $course->course_name }}</option>
                    @endforeach
                    </select>
                </div>
                <iframe  height="900" src="{{ URL::to('training_students/'. $data[0]->training_id .'/-1') }}" id="iframe" style="width: 100%;" seamless="seamless" frameborder="0">
                  <h1>iFrames are not supported in this browser.</h1>
                </iframe>
            @endif
            </div>
        </div>
    </div>

</div>
 {{HTML::script('js/constants.js') }}

<script>
 $('.sel_course').on('change', function(){

    iframe = $('#iframe');
        course_id = $('option:selected', '#change_course').attr('data-course-id');
        training_id = $('option:selected', '#change_course').attr('data-training-id');
        $(iframe).attr('src', origin + "training_students/" + training_id +"/" + course_id);
    });
</script>

</body>
</html>

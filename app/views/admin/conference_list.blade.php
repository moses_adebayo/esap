
@extends('layouts.admin_page')

@section('styles')
@parent
{{HTML::style('css/datepicker.css') }}
@stop()

@section('scripts')
@parent
{{HTML::script('js/admin.js')}}
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container-fluid" id="page-main">
    <div class="row">
        <div class="col-md-5">
            <div class="acc_pane">
                <button class="btn my-btn">
                    {{ HTML::link('training/all_users/'.$details->training_id, 'Print All', array('class'=>'white-text', 'target'=>'_blank')) }}
                </button>
                <h2>{{ $details->name }}</h2>
                <p class="small text-muted">
                    {{ $details->start_date  }} till {{ $details->end_date }}
                </p>
                <p  class="text-primary">
                    <span class="fa fa-location-arrow">&nbsp;</span>{{ $details->location }}
                </p>
                <code>CODE NAME- {{ $details->code }}</code>
            </div>
        </div>

        <div class="col-md-6 col-md-offset-1">
            <div class="acc_pane acc_pane_odd">

                @if(sizeof($students) == 0)
                    <h2>No student yet</h2>
                @else
                <h2>{{ $students['course_name'] }}&nbsp;&nbsp;<span class="badge text-success">{{ $students['total'] }}</span></h2>
                <div class="overflow_list">
                    <ol>
                        @foreach($students['list'] as $s)
                        <li><a target="_blank" href="{{URL::to('user/slip/'.$s['application_id'])}}"> <strong>{{ $s['name']}}</strong> </a>
                            <code>&nbsp;{{ $s['reg'] }}</code>
                        </li>
                        @endforeach

                    </ol>
                </div>


                @endif
            </div>
        </div>
    </div>

</div>
@stop()


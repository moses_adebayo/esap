@extends('layouts.admin_page')

@section('styles')
@parent

@stop()

@section('scripts')
@parent
{{HTML::script('js/Account.js') }}

@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container-fluid" id="page-main">

    <div class="row">
        <div class="col-md-4">
            <div class="acc_pane">
                <ul class="list-unstyled">
                    <li class="list ">
                        <a href="{{ route('admin_panel') }}">Generate Voucher</a>
                    </li>
                    <li class="list active">
                        <a href="{{URL::to('teller_form') }}">Enter Teller </a>
                    </li>
                    <li class="list">
                        <a href="{{ route('form_training') }}">Training </a>
                    </li>
                    <li class="list">
                        <a href="{{ route('form_conference') }}">Conference </a>
                    </li>
                    <li class="list">
                        <a href="{{ route('new_info') }}">Announcement </a>
                    </li>
                     <li class="list">
                        <a href="{{ route('all-users') }}">All Users </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-md-7 col-md-offset-1">
            <div class="acc_pane acc_pane_odd">

                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#confirm_teller" data-toggle="tab">Confirmation</a>
                    </li>
                    <li>
                        <a href="#usage" data-toggle="tab">History</a>
                    </li>
                </ul>
                <div class="tab-content">
                {{--FORM ACTIONS RESULT --}}
                     <div>
                         <div class="loading hidden">
                             <img src="{{ asset('images/loading.gif') }}">
                         </div>
                             <div class="no-margin alert alert-success text-center hidden" id="account-success"></div>
                             <div class="no-margin alert alert-danger text-center hidden" id="account-error"></div>
                     </div>
                    <div class="tab-pane fade in active"  id="confirm_teller">
                        <form role="form" action="#" method="post" id="confirm_deposit">
                           <div class="form-group">
                               <label for="tellerNumber">Enter Teller Number <span class="small text-danger">(case sensitive)</span> </label>
                               <input required id="tellerNumber" class="form-control" type="text" name="teller_number">
                               <label for="amount">Amount Deposited</label>
                               <select name="amount" id="amount">
                                <option value="2500">2,500</option>
                                <option value="5000">5,000</option>
                                <option value="7500">7,500</option>
                                <option value="10000">10,000</option>
                                <option value="12500">12,500</option>
                                <option value="15000">15,000</option>
                                <option value="17500">17,500</option>
                               </select>
                           </div>
                           <button type="submit" class="large_button btn my-btn">Confirm</button>
                        </form>
                    </div>

                    <div class="tab-pane fade"  id="usage">
                        @if(empty($history))
                            <h3>No teller verification yet!</h3>
                        @else
                            @foreach($history as $data)
                                <p>
                                    <strong>
                                        {{ ucwords($data->surname . " ". $data->first_name)  }}
                                    </strong>
                                    verified <code> {{ $data->tellerNumber }}</code> on <cite> {{ $data->date_used }}</cite>
                                </p>
                            @endforeach
                        @endif

                        {{ $history->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop()


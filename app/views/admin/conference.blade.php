@extends('layouts.admin_page')

@section('styles')
@parent
{{HTML::style('css/datepicker.css') }}
@stop()

@section('scripts')
@parent
{{HTML::script('js/extended_bootstrap/bootstrap-datepicker.min.js')}}
{{HTML::script('js/admin.js')}}
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container-fluid" id="page-main">

<div class="row">
<div class="col-md-4">
    <div class="acc_pane">
        <ul class="list-unstyled">
            <li class="list">
                <a href="{{ route('admin_panel') }}">Generate Voucher</a>
            </li>
            <li class="list">
                <a href="{{URL::to('teller_form') }}">Enter Teller </a>
            </li>
            <li class="list">
                <a href="{{ route('form_training') }}">Training </a>
            </li>
            <li class="list active">
                <a href="{{ route('form_conference') }}">Conference</a>
            </li>
            <li class="list">
                <a href="{{ route('new_info') }}">Announcement </a>
            </li>
             <li class="list">
                <a href="{{ route('all-users') }}">All Users </a>
            </li>
        </ul>
    </div>

</div>

<div class="col-md-7 col-md-offset-1">
    <div class="acc_pane acc_pane_odd">

        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#new" data-toggle="tab">Create</a>
            </li>
            <li>
                <a href="#inactive" data-toggle="tab">Pending</a>
            </li>
            <li>
                <a href="#active" data-toggle="tab">Active</a>
            </li>
            <li>
                <a href="#running" data-toggle="tab">Running</a>
            </li>
            <li>
                <a href="#all" data-toggle="tab">All Training</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="new">
                <div>

                    @if(isset($server))
                    @if($server['status'])
                    <div class="alert alert-success">
                        <p class="text-center">
                            Conference created successfully
                        </p>
                    </div>
                    @else
                    <div class="alert alert-danger">
                        <p class="text-center">
                            {{ $server['msg'] }}
                        </p>
                    </div>
                    @endif
                    @endif

                    @if(isset($form_errors['course']))
                    <div class="text-danger text-center padding-all">{{ $form_errors['course'] }}</div>
                    @endif

                    <form action="{{route('create_conference')}}" method="post" class="form-horizontal" role="form">
                        {{Form::token() }}
                        <div class="form-group">
                            <label for="name" class="col-sm-3">Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="name"  name="conference_name" placeholder="Conference Thenme">
                                @if(isset($form_errors['training_name']))
                                <div class="text-danger">{{ $form_errors['training_name'] }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="code" class="col-sm-3">Conference code</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="code" name="code">
                                @if(isset($form_errors['code']))
                                <div class="text-danger">{{ $form_errors['code'] }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="start" class="col-sm-3">Start Date</label>
                            <div class="col-sm-9">
                                <input name="start_date" class="date-picker form-control cursor" readonly id="id-date-picker-1" type="text" placeholder="Date of birth" data-date-format="yyyy-mm-dd"{{ (Input::old('birth_date')) ? ' value="' . e(Input::old('birth_date')) . '"' : '' }}/>
                                @if(isset($form_errors['start_date']))
                                <div class="text-danger">{{ $form_errors['start_date'] }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="end" class="col-sm-3">End Date</label>
                            <div class="col-sm-9">
                                <input name="end_date" class="date-picker form-control cursor" readonly id="id-date-picker-1" type="text" placeholder="Date of birth" data-date-format="yyyy-mm-dd"{{ (Input::old('birth_date')) ? ' value="' . e(Input::old('birth_date')) . '"' : '' }}/>
                                @if(isset($form_errors['end_date']))
                                <div class="text-danger">{{ $form_errors['end_date'] }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="end" class="col-sm-3">Time</label>
                            <div class="col-sm-9">
                                <input name="time" class="form-control"   type="text" placeholder="Time of conference" value="{{ (Input::old('time')) ? ' value="' . e(Input::old('time')) . '"' : '' }}"/>
                                @if(isset($form_errors['end_date']))
                                <div class="text-danger">{{ $form_errors['time'] }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cost" class="col-sm-3">Cost</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="cost" name="cost" placeholder="Cost of conference">
                                @if(isset($form_errors['cost']))
                                <div class="text-danger">{{ $form_errors['cost'] }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="location" class="col-sm-3">Location</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="location" name="location" placeholder="Main venue">
                                @if(isset($form_errors['location']))
                                <div class="text-danger">{{ $form_errors['location'] }}</div>
                                @endif
                            </div>
                        </div>
                        <div>
                            <br/>
                            <input type="submit" class="btn large_button my-btn form-control col-md-offset-3" value="Create">
                        </div>
                    </form>
                </div>
            </div>

            <div class="tab-pane fade" id="active">
                @if(sizeof($active_training) == 0)
                <h2 class="emptyRecord">No active training</h2>
                @else
                @foreach($active_training as $training)
                <div class="training_list">
                    <a href='{{URL::to("training/list/$training->training_id") }}'> <h4>{{ $training->name;}}[ {{ $training->code; }} ]</h4>  </a>
                    <p><strong><span class="text-success">{{  $training->start_date }}</span>&nbsp;&nbsp;till&nbsp;&nbsp;<span class="text-danger"> {{ $training->end_date }}</span></strong></p>
                    <p><span class="fa fa-location-arrow">&nbsp;</span> {{ $training->location }}</p>

                    <form action="{{route('close_conference')}}" method="post" class="form-horizontal" role="form">
                        {{ Form::token() }}
                        <input type="hidden" name="t_id" value="{{ $training->training_id; }}">
                        <input type="submit" class="btn my-btn" value="Close"/>
                        {{Form::close() }}
                </div>
                @endforeach
                @endif

            </div>

            <div class="tab-pane fade" id="inactive">
                @if(sizeof($inactive_training) == 0)
                <h2 class="emptyRecord">No pending conference </h2>
                @else
                @foreach($inactive_training as $training)
                <div class="training_list">
                    <a href='{{URL::to("conference/list/$training->training_id") }}'> <h4>{{ $training->name }}</h4></a>
                    <p><strong><span class="text-success">{{ $training->start_date }}</span>&nbsp;&nbsp;till&nbsp;&nbsp;<span class="text-danger">{{ $training->end_date }}</span></strong></p>
                    <p><span class="fa fa-location-arrow">&nbsp;</span> {{ $training->location }} </p>

                    <form action="{{route('open_conference')}}" method="post" class="form-horizontal" role="form">
                        {{Form::token()}}
                        <input type="hidden" name="t_id" value="{{ $training->training_id }}">
                        <input type="submit" class="btn my-btn" value="Open"/>
                        {{Form::close() }}
                </div>
                @endforeach
                @endif

            </div>

            <div class="tab-pane fade" id="running">
                @if(sizeof($running_training) == 0)
                <h2 class="emptyRecord">No conference running currently</h2>
                @else
                @foreach($running_training as $training)
                <div class="training_list">
                    <a href='{{URL::to("conference/list/$training->training_id") }}'> <h4>{{$training->name; }}[{{ $training->code; }} ]</h4> </a>
                    <p><strong><span class="text-success">{{ $training->start_date }}</span>&nbsp;&nbsp;till&nbsp;&nbsp;<span class="text-danger">{{ $training->end_date }}</span></strong></p>
                    <p><span class="fa fa-location-arrow">&nbsp;</span>{{ $training->location }} </p>
                    <form action="{{route('terminate_conference')}}" method="post" class="myForm-inline" role="form">
                        {{Form::token()}}
                        <input type="hidden" name="t_id" value="{{ $training->training_id }}">
                        <input type="submit" class="btn btn-danger" value="Dismiss"/>
                        {{Form::close() }}

                        <form action="{{route('open_training')}}" method="post" class="myForm-inline" role="form">
                            {{Form::token()}}
                            <input type="hidden" name="t_id" value="{{ $training->training_id }}">
                            <input type="submit" class="btn my-btn2" value="Open"/>
                            {{Form::close() }}
                </div>
                @endforeach
                @endif

            </div>

            <div class="tab-pane fade" id="all">
                @if(sizeof($all_training) == 0))
                <h2 class="emptyRecord">No conference created yet</h2>
                @else
                @foreach($all_training as $training)
                <div class="training_list">
                    <a href='{{URL::to("conference/list/$training->training_id") }}'> <h4>{{$training->name; }}[{{ $training->code; }} ]</h4> </a>
                    <p><strong><span class="text-success">{{ $training->start_date }}</span>&nbsp;&nbsp;till&nbsp;&nbsp;<span class="text-danger">{{ $training->end_date }}</span></strong></p>
                    <p><span class="fa fa-location-arrow">&nbsp;</span>{{ $training->location }} </p>
                </div>
                @endforeach
                @endif

            </div>
        </div>
    </div>
</div>
</div>

</div>
@stop()


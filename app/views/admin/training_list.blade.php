
@extends('layouts.admin_page')

@section('styles')
@parent
{{HTML::style('css/datepicker.css') }}
@stop()

@section('scripts')
@parent
{{HTML::script('js/extended_bootstrap/bootstrap-datepicker.min.js')}}
{{HTML::script('js/admin.js')}}
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container-fluid" id="page-main">
    <div class="row">
        <div class="col-md-4">
            <div class="acc_pane">
                <button class="btn my-btn">
                    {{ HTML::link('training/all_users/'.$t_id, 'Print All', array('class'=>'white-text', 'target'=>'_blank')) }}
                </button>
                <ul class="list-unstyled">
                    @foreach($courses as $course)
                        <li class="list">{{ HTML::link('training/course/list/'.$t_id.'/'.$course->course_id, $course->course_name) }}</li>
                    @endforeach
                </ul>

            </div>
        </div>

        <div class="col-md-7 col-md-offset-1">
            <div class="acc_pane acc_pane_odd">

                @if($students['empty'])
                    <h2>No student yet</h2>
                @else
                <h2>{{ $students['course_name'] }}&nbsp;&nbsp;<span class="badge text-success">{{ $students['total'] }}</span></h2>
                <div class="overflow_list">
                    <ol>
                        @foreach($students['list'] as $s)
                        <li><a target="_blank" href="{{URL::to('user/slip/'.$s['application_id'])}}"> <strong>{{ $s['name']}}</strong> </a>
                            <code>&nbsp;{{ $s['reg'] }}</code>
                        </li>
                        @endforeach

                    </ol>
                </div>


                @endif
            </div>
        </div>
    </div>

</div>
@stop()


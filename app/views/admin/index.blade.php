@extends('layouts.admin_page')

@section('styles')
@parent

@stop()

@section('scripts')
@parent

@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container-fluid" id="page-main">
    <?php
        $show_history = false;
    if(isset($_GET['page'])){
       $show_history = true;
    }
    ?>
    <div class="row">
        <div class="col-md-4">
            <div class="acc_pane">
                <ul class="list-unstyled">
                    <li class="list  active">
                        <a href="{{ route('admin_panel') }}">Generate Voucher</a>
                    </li>
                    <li class="list">
                        <a href="{{URL::to('teller_form') }}">Enter Teller </a>
                    </li>
                    <li class="list">
                        <a href="{{ route('form_training') }}">Training </a>
                    </li>
                    <li class="list">
                        <a href="{{ route('form_conference') }}">Conference </a>
                    </li>
                    <li class="list">
                        <a href="{{ route('new_info') }}">Announcement </a>
                    </li>
                     <li class="list">
                        <a href="{{ route('all-users') }}">All Users </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-md-7 col-md-offset-1">
            <div class="acc_pane acc_pane_odd">

                <ul class="nav nav-tabs">
                    <li class="<?php echo ($show_history) ? "" : "active" ?>">
                        <a href="#by_voucher" data-toggle="tab">Generate</a>
                    </li>
                    <li class="<?php echo ($show_history) ? "active" : "" ?>">
                        <a href="#usage" data-toggle="tab">History</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="<?php echo ($show_history) ? "tab-pane fade in" : "tab-pane fade in active" ?>" id="by_voucher">
                        <div>
                            <?php
                            //                            var_dump($info);
                            if(isset($v_response)){
                                if($v_response){
                                    ?>
                                    <p class="alert alert-success text-center">
                                        Voucher created successfully
                                    </p>
                                <?php
                                }else{
                                    ?>
                                    <p class="text-danger">
                                        An error occur
                                    </p>
                                <?php
                                }
                            }
                            ?>

                            <h3>There are <span class="text-danger"> <?php echo $info['unused'] ?></span>  Vouchers</h3>
                            <h3>Value of unused vouchers is <span class="text-danger"><?php echo Olajuwon::format_money($info['unused_value']) ?> </span> </h3>

                            <form class="myForm-inline" action="{{route('generate_voucher') }}" method="post">
                                <button class="large_button btn my-btn">Generate</button>
                            </form>
                              &nbsp;&nbsp;
                            <form class="myForm-inline" action="{{route('print_voucher') }}" method="post">
                                {{Form::token() }}
                                <button class="large_button btn my-btn2">Print</button>
                            </form>

                        </div>
                    </div>

                    <div class="<?php echo ($show_history) ? "tab-pane fade in active" : "tab-pane fade" ?>"" id="usage">
                        <?php
//                        var_dump($history);
                        foreach($history as $h){
                            ?>
                        <p>
                            <strong><?php echo ucwords($h->surname . " ".$h->first_name)?></strong>
                            loaded <code><?php echo $h->code ?></code> on <cite><?php echo $h->date_used ?></cite>
                        </p>
                        <?php
                        }
                        ?>

<!--                        //Links to Pagination-->
                        <?php
                            echo $history->links();
                        ?>
                    </div>


                </div>


            </div>

        </div>

    </div>

</div>
@stop()


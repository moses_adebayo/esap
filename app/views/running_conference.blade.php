@extends('layouts.page')

@section('styles')
@parent
{{HTML::style('css/events.css') }}
@stop()

@section('scripts')
@parent
{{HTML::script('js/Conferences.js') }}
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container-fluid" id="page-main">

    <div class="row">
        <div class="col-md-7">
            <div class="acc_pane">
                <blockquote>
                     <h1>{{ $conference->name }}</h1>
                    <h4>
                        <span class="fa fa-calendar text-warning">&nbsp;&nbsp;</span> {{ $conference->start_date  }}
                         @if($conference->end_date !== '0000-00-00')
                            {{ " till ". $conference->end_date; }}
                         @endif
                    </h4>
                     <h4>
                        <span class="fa fa-clock-o text-danger">&nbsp;&nbsp;</span> {{ $conference->time  }}
                    </h4>
                    <p class="text-primary">
                        <span class="fa fa-home text-info">&nbsp;&nbsp;</span> {{ $conference->location }}
                    </p>

                    @if($user['new'])
                        @if($conference->status == Constants::ACTIVE_TRAINING)
                              <button class="btn btn-lg btn-warning center-block" id="apply" data-conference-id ="{{ $conference->training_id }}">Register</button>
                        @else
                            <p class="alert alert-danger">Registration is closed for this conference</p>
                        @endif
                    @elseif(!$user['new'])
                                <p class="alert alert-info text-center">You register for this conference
                                        <br/>
                                        Registration code is <code>{{$record->reg_num}}</code>
                                </p>
                          <button class="btn btn-lg btn-warning center-block"><a class="btn_link" href="<?php echo URL::to('slip2/'.$record->id); ?>" target="_blank">Print slip</a></button>

                    @endif

                    <div class="loading hidden">
                        <img src="{{ asset('images/loading.gif') }}">

                    </div>
                     <div id="app-success" class="margin-all alert alert-success hidden text-center"></div>
                    <div id="app-error" class="margin-all alert alert-danger hidden text-center"></div>
                </blockquote>
            </div>
        </div>

        <div class="col-md-4 col-md-offset-1">
            <div class="acc_pane acc_pane_odd">
                <h2>CONFERENCE COST </h2>
                <h3 class="text-danger"><span class="naira">N</span>{{$conference->cost}}</h3>
            </div>

        </div>


    </div>
</div>

</div>
@stop()


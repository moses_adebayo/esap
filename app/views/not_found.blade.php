@extends('layouts.page')

@section('styles')
@parent
{{HTML::style('css/events.css') }}
@stop()

@section('scripts')
@parent
{{HTML::script('js/jquery.refineslide.js') }}
{{HTML::script('js/Slider.js') }}
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container" id="page-main">
    <div class="col-xs-12">
       <h3>The page you are trying to access does not exist</h3>
    </div>


</div>
@stop()


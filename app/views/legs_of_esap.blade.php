@extends('layouts.page')

@section('styles')
@parent
{{HTML::style('css/events.css') }}
@stop()

@section('scripts')
@parent
{{HTML::script('js/jquery.refineslide.js') }}
{{HTML::script('js/Courses.js') }}
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container-fluid" id="page-main">
<div class="row" >
<div class="col-md-8">
<h3 class="pg-title">Legs of <span class="text-lowercase">e</span>-SAP</h3>
    <p class="text-center">
        The e-SAP project and initiative stand on three major legs of operations. In a bid to achieve our Vision and Objectives, there are three (3) major approaches and directions e-SAP Foundations takes. These three legs of e-SAP are as follows:
    </p>
    <ol class="list-group">
       <li class="list-group-item">
                <h3 style="margin: 0 0 0.4em" class="text-uppercase text-success">Sensitization and Awareness</h3>
                <p>
                    This is actually the most important and the most tasking aspect of what we do. Our first responsibility was to conduct a study on why do many people graduate and find it difficult to get jobs? One of the most significant reasons discovered as the major causes of unemployment are ignorance and wrong mindsets. Many are sincerely unaware that the education in the 21st century is clearly different from it used to be in the past and that nowadays, that it is wrong and dangerous for them to focus on academics alone while they are in school because gone are the days when employers celebrate great academic grades alone. Education in the past (especially tertiary education) could be trusted and relied upon to help individuals develop the other aspects of their lives, beyond academics. However today, our education system has lost certain values and as such, it has become the responsibility of every individual Nigerian student to find a means to make up for the deficiencies in the education system otherwise, on graduation, such a student will suddenly find out that he lacks what actually makes life to work and respond.
                </p>
                <p>
                    Our sensitization and awareness programme is realized through seminars, students’ conferences, campaigns, talk shows, rallies, street campaigns, etc. Click <a href="{{URL::to('invite')}}"> here </a> to invite e-SAP for a sensitization and awareness programme in your school or community.
                </p>
       </li>
       <li class="list-group-item">
               <h3 style="margin: 0 0 0.4em" class="text-uppercase text-success">TRAININGS</h3>
               <p>
                   Every human being is normal until training is applied. The major difference between failures, the average, and the excellent and exceptional human beings is training. e-SAP trainings are in various categories; they include Professional courses, Vocational courses, Entrepreneurship courses, Information Technology courses, Life courses and specialized training for unemployed graduates.
               </p>
               <p>
               The training sub-channel of what we do is where we identify certain skills that are capable of making our youths become highly valuable individuals, especially those that are in high demand in the employment market ad bring such skills (that are usually very expensive to acquire) down to various campuses so that students can acquire them and have more than mere academic achievement during their stay on campus.
               </p>
               <p>
                       Our trainings are in various categories; they include but not limited to:
               </p>
               <ol>
                   <li>Vocational courses</li>
                   <li>Entrepreneurship courses</li>
                   <li>Information Technology courses</li>
                   <li>Corporate courses</li>
                   <li>Professional courses</li>
                   <li>Life courses</li>
                   <li>Specialized training for the unemployed graduates</li>
               </ol>
               <p>


      </li>
      <li class="list-group-item">
         <h3 style="margin: 0 0 0.4em" class="text-uppercase text-success">Internship</h3>
         <p>
             As part of our goal to eradicate unemployment by the end of year 2025, we link our trainees up with organizations where they can gain practical knowledge and experiences such that at graduation, the difference is very clear between them and other students who only focused on academics while in school. This is an avenue for them to put the knowledge they have acquired through any of our trainings into practical use. e-SAP members are given the opportunity to submit their credentials online and information about internship opportunities and job offers are sent to members from time to time.
         </p>
       </li>
    </ol>

</div>

<div class="col-md-4">
    {{-- INFORMATION PANEL --}}
    @include('utilities.info_panel')
</div>
</div>
</div>
@stop()


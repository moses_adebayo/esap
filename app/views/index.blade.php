@extends('layouts.page')

@section('styles')
@parent
{{--{{HTML::style('css/refineslide.css') }}--}}
{{HTML::style('css/jssorFix.css') }}
{{HTML::style('css/extend.css') }}
{{HTML::style('css/index.css') }}
@stop()

@section('scripts')
@parent
{{--{{HTML::script('js/jquery.refineslide.js') }}--}}
{{--{{HTML::script('js/Slider.js') }}--}}
{{HTML::script('js/Testimonial.js') }}
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container">
    <div class="container">
        <br>
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
                <li data-target="#myCarousel" data-slide-to="4"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="{{asset('img/thenewsliders/1.jpg')}}" alt="ESAP Foundation">
                </div>

                <div class="item">
                    <img src="{{asset('img/thenewsliders/2.jpg')}}" alt="ESAP Foundation">
                </div>

                <div class="item">
                    <img src="{{asset('img/thenewsliders/3.jpg')}}" alt="ESAP Foundation">
                </div>

                <div class="item">
                    <img src="{{asset('img/thenewsliders/4.jpg')}}" alt="ESAP Foundation">
                </div>
                <div class="item">
                    <img src="{{asset('img/thenewsliders/5.jpg')}}" alt="ESAP Foundation">
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="fa fa-chevron-circle-left slider-pointer" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="fa fa-chevron-circle-right slider-pointer" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>

</div>
<div class="container" id="page-main">
    <div class="row">
        <div class="panel panel-info">
            <div class="panel-body text-center">
                <h2 class="text-info text-center">
                    e-SAP OAU Season Six !
                </h2>
                <a href="{{URL::to('registration')}}"><button class="btn btn-primary btn-sm">Register Now</button></a>&nbsp;&nbsp;
                <a download href="{{asset('pdf/oau6.pdf')}}"><button class="btn btn-info btn-sm"><span class="fa fa-file-pdf-o">&nbsp;&nbsp;</span>Download Timetable</button></a>
                <div class="col-sm-offset-2 col-sm-8" style="background: red; color: #fff; padding: 10px; border-radius: 5px; margin-top: 15px;">
                    <p class="text-center">
                        <strong>ATTENTION</strong> Please ensure that you check the time-table <strong>BEFORE</strong> picking courses online. You
                        <strong>SHOULD NOT</strong> register for two or more courses that are clashing on the time-table.
                    </p>
                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-6 heading-title">
            <div class="heading-info">
                @if(Auth::check())
                    <h3 class="text-capitalize"><span class="fa fa-user">&nbsp;</span> Welcome, {{User::userInfo(Auth::id())->surname}}</h3>
                    <p>
                        You have the priiledge to participate in any of our programmes especially the trainings and conferences
                    </p>
                    <a href="{{URL::to('account')}}"><button class="btn theme-background white-text">My Account</button></a>
                @else
                    <h3><span class="fa fa-user-plus">&nbsp;</span>Register Now</h3>
                    <p>
                        To participate in any of our programmes, you need to create account with us. You have access to our various trainings. This also qualifies you to enjoy help from our Career and Internship platforms. If you become a member, you have access to a wide variety of opportunities that we provide
                    </p>
                    <a href="{{URL::to('registration')}}"><button class="btn theme-background white-text">SIGN UP NOW</button></a>
                @endif
            </div>
        </div>
        <div class="col-md-6 heading-title">
            <div class="heading-info">
                 <h3><span class="fa fa-question-circle text-success">&nbsp;</span>What is e-SAP?</h3>
                <p>
                    <strong>e-SAP</strong> is an acronym for <strong>Extra-mural Skills Acquisition Programme</strong>. <strong>e-SAP Foundations</strong> is a Social Entrepreneurship Organization with a mandate to show every Nigerian youth <a href="#"><strong>the way out</strong></a> of the grip of unemployment that has bound millions of people within and outside the country. <a href="#"><strong>Our message</strong></a> is very simple and direct: <strong>“You don’t have to taste unemployment one day of your life!”</strong> <a href="#"><strong>Our campaign</strong></a> is against unemployment! The target audiences of e-SAP are undergraduates and fresh graduates.
                </p>
            </div>
        </div>
     </div>
     <div class="row">
        <div class="col-md-6 heading-title">
            <div class="heading-info">
                <h3> <span class="fa fa-university">&nbsp;</span>Trainings</h3>
                <p>
                  Every human being is normal until training is applied. The major difference between failures, the average, and the excellent and exceptional human beings is training.  <strong>e-SAP</strong> trainings are in various categories; they include Professional courses, Vocational courses, Entrepreneurship courses, Information Technology courses, Life courses and specialized training for unemployed graduates.
                </p>
                <a href="{{URL::to('esap_training')}}"> <button class="btn btn-sm theme-background white-text">Read More</button></a>
            </div>
        </div>
        <div class="col-md-6 heading-title">
            <div class="heading-info">
                <h3><span class="fa fa-book">&nbsp;</span>Courses</h3>
                <p>
                    There are currently over 25 free courses which are designed to add specific and special values to anyone who undergo them. These courses have been so structured to achieve our aims and objectives and as such, anyone who go through these courses will become very valuable to employers.
                </p>
                <a href="{{URL::to('courses')}}"><button class="btn btn-sm theme-background white-text">Read More</button></a>
            </div>
        </div>
    </div>
    <div class="row" >
        <div class="col-md-6 heading-title">
            <div class="heading-info">
                <h3><span class="fa fa-street-view">&nbsp;</span>Careers and Internship</h3>
                <p>
                    As part of our goal to eradicate unemployment by the end of year 2025, we link our trainees up with organizations where they can gain practical knowledge and experiences such that at graduation, the difference is very clear between them and other students who only focused on academics while in school.
                </p>
                <a href="{{URL::to('careers')}}"><button class="btn btn-sm theme-background white-text">Read More</button></a>
            </div>
        </div>
        <div class="col-md-6 heading-title">
            <div class="heading-info">
                <h3><span class="fa fa-flag">&nbsp;</span> Three Legs of e-SAP</h3>
                <p>
                    The e-SAP project and initiative stand on three major legs of operations. In a bid to achieve our <strong><a href="#">Vision</a></strong> and <strong><a href="#">Objectives</a></strong>, there are three (3) major approaches and directions e-SAP Foundations takes. These three legs of e-SAP are as follows: Sensitization and awareness, Trainings, Career and Internship
                </p>
                <a href="{{ URL::to('legs_of_esap') }}"><button class="btn btn-sm theme-background white-text">Read more</button></a>
            </div>
        </div>
    </div>

    <div class="double-border"></div>

      {{-- ARTICLES AND INFORMATIONS --}}
      <div class="row">
            <div class="col-md-4 info-panel">
                    <div class="info-img">
                       <img src="{{asset('images/reason.jpg') }}" alt="Info Desk" width="330" height="170"/>
                     </div>
                     <h2>101 Reasons why you won't get a good Job after Graduation</h2>
                     <p class="small">
                            Why do people graduate and cannot get jobs? What should yo do so as not to join the unemployment queue?
                             Attend <strong>e-SAP</strong> Conference to learn how not to enter the net of unemployment when you graduate...
                     </p>
                        <a href="{{route('showConf')}}">
                            <button class="btn btn-sm theme-background white-text">READ MORE</button>
                        </a>
            </div>
            <div class="col-md-4 info-panel">
                    <div class="info-img">
                       <img src="{{asset('images/invite.jpg') }}" alt="Info Desk" width="330"  height="170" />
                     </div>
                     <h2>Invite <strong>e-SAP</strong> to your School or Community</h2>
                     <p class="small">
                           At e-SAP Foundations, our goal is to ensure that every higher institution in Nigeria benefit from our value adding and life changing packages with the aim of achieving our set objectives.
                       </p>
                     <a href="{{URL::to('invite')}}">
                         <button class="btn btn-sm theme-background white-text">Invite e-SAP</button>
                      </a>
            </div>
            <div class="col-md-4 info-panel">
                    <div class="info-img">
                       <img src="{{asset('images/volunteer.jpg') }}" alt="Info Desk" width="330"  height="170" />
                     </div>
                     <h2>Become <strong>e-SAP</strong> volunteer</h2>
                     <p class="small">
                            The vision and task of eradicating unemployment in Nigeria is not, and cannot be the job of a single person or institution. There is hardly anybody you’ll ask who doesn’t know someone looking for job somewhere! Since unemployment is so widespread and pervasive, all hands must be on deck in fighting the monster off.
                       </p>
                     <a href="{{URL::to('volunteer')}}">
                         <button class="btn btn-sm theme-background white-text">JOIN NOW</button>
                    </a>
            </div>
        </div>

    <div class="double-border"></div>

{{--EVENTS SLIDER--}}
        <div class="row info-panel">
            <div class="row-heading">
                PHOTO NEWS
            </div>
             <ul id="flexiselDemo3">
                <li>
                        <a target="_blank" href="{{ URL::to("https://www.flickr.com/photos/131652997@N06/") }}" data-toggle="tooltip"  title="Accounting Peach Tree students at eSAP OAU Season II">
                        <img src="{{asset('images/gallery/gallery1.jpg') }}" /><p class="text-muted small gal_event">
                                Accounting Peach Tree class
                        </p>
                        </a>
                </li>
                <li>
                        <a target="_blank" href="{{ URL::to("https://www.flickr.com/photos/131652997@N06/") }}" data-toggle="tooltip"  title="Students engage in coding challenge at eSAP OAU Season II">
                        <img src="{{asset('images/gallery/gallery2.jpg') }}" /><p class="text-muted small gal_event">Coding challenge</p>
                        </a>
                </li>
                <li>
                        <a target="_blank" href="{{ URL::to("https://www.flickr.com/photos/131652997@N06/") }}" data-toggle="tooltip"  title="Accounting Peach Tree students at eSAP OAU Season II">
                        <img src="{{asset('images/gallery/gallery3.jpg') }}" /><p class="text-muted small gal_event">Accounting Peach Tree students </p>
                        </a>
                </li>
                {{--<li>--}}
                        {{--<a target="_blank" href="{{ URL::to("https://www.flickr.com/photos/131652997@N06/") }}" data-toggle="tooltip"  title="e-SAP Director addressing the students">--}}
                        {{--<img src="{{asset('images/gallery/gallery4.jpg') }}" /><p class="text-muted small gal_event">e-SAP Director</p>--}}
                        {{--</a>--}}
                {{--</li>--}}
                <li>
                        <a target="_blank" href="{{ URL::to("https://www.flickr.com/photos/131652997@N06/") }}" data-toggle="tooltip"  title="Human Resources Business Professional (HRBP)">
                        <img src="{{asset('images/gallery/gallery7.jpg') }}" /><p class="text-muted small gal_event">Cross-section of HRBP class</p>
                        </a>
                </li>
                <li>
                        <a target="_blank" href="{{ URL::to("https://www.flickr.com/photos/131652997@N06/") }}" data-toggle="tooltip"  title="Accounting Peach Tree instructor">
                        <img src="{{asset('images/gallery/gallery8.jpg') }}" /><p class="text-muted small gal_event">e-SAP Instructor</p>
                        </a>
                </li>
                <li>
                        <a target="_blank" href="{{ URL::to("https://www.flickr.com/photos/131652997@N06/") }}" data-toggle="tooltip"  title="Students at e-SAP training">
                        <img src="{{asset('images/gallery/gallery9.jpg') }}"/><p class="text-muted small gal_event">Training Session</p>
                        </a>
                </li>
             </ul>
                 <a class="pull" target="_blank" href="{{ URL::to("https://www.flickr.com/photos/131652997@N06/") }}">
                     <button class="btn btn-xs theme-background white-text">VIEW ALL PHOTOS</button>
                 </a>
        </div>

    {{--    TESTIMONIALS --}}
    <div class="row">
        <div class="col-md-12 testimonial">
            <p class="text-center" id="t_info">
                <span >{{HTML::image('images/loading.gif') }}</span>
            </p>
            <h3 class="text-center" id="t_name"></h3>
        </div>
    </div>

</div>
<script>
     $("[data-toggle='tooltip']").tooltip();
</script>

{{HTML::script('js/jquery.flexisel.js') }}
{{HTML::script('js/FlexiScroll.js') }}
@stop()


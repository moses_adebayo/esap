@extends('layouts.page')

@section('styles')
@parent
{{HTML::style('css/events.css') }}
@stop()

@section('scripts')
@parent
{{HTML::script('js/jquery.refineslide.js') }}
{{HTML::script('js/Slider.js') }}
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container" id="page-main">
    <div class="row">
        <div class="col-md-6">

       <iframe src="https://docs.google.com/forms/d/1Ld9zZJOTCnrpBqDBiSwLbUI6Tyy2hNNmCkEh4IvQtcg/viewform?embedded=true" width="650" height="1150" frameborder="0" marginheight="0" marginwidth="0"><span class="fa fa-spinner fa-pulse"></span></iframe>
    </div>

    <div class="col-md-4 col-sm-offset-2 acc_pane">
        <h3 class="pg-title">Invite <span class="text-lowercase">e</span>-SAP</h3>
       <p>
           At e-SAP Foundations, our goal is to ensure that every higher institution in Nigeria benefit from our value adding and life changing packages with the aim of achieving our set objectives. However, it is impossible to get to everywhere all at once. Therefore, while working to keep including more and more higher institutions on our list of provision, we seek to give precedence to institutions where we are specifically invited.
       </p>
       <p>
           The implication is that you will be our ambassador in your school. When you become an e-SAP Student Ambassador (ESA), you are entitled to be taken through certain trainings. Once you qualify, you will be required to raise a team of people who can conveniently work with you and thereafter, every resources you need to operate as e-SAP ambassador and to extend the vision of e-SAP in your school will be provided by us.
       </p>
    </div>
</div>



</div>
@stop()


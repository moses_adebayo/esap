<!DOCTYPE html>
<html>
<head>
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="google-signin-clientid" content="939699820790-etki06urbqddflcbkqsces79qa9mquvp.apps.googleusercontent.com" />
    <meta name="google-signin-scope" content="email" />
    <meta name="google-signin-cookiepolicy" content="single_host_origin" />
    <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <title>e-SAP &reg;</title>
    @section('styles')
    {{ HTML::style('css/bootstrap.min.css')}}
    {{ HTML::style('fonts/font-awesome.min.css')}}
    {{ HTML::style('css/extra-small-devices.css')}}
    {{ HTML::style('css/editBootstrap.css')}}
    {{ HTML::style('css/styles1.css')}}

    @show()

    @section('scripts')
    {{HTML::script('js/jquery.min.js') }}
    {{HTML::script('js/bootstrap.min.js') }}
    {{HTML::script('js/Page.js') }}

    @show()
    @section('social_auth')
    @show()
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=150708721791059&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

@include('layouts.navbar')

@yield('content')

{{--FOOTER--}}
@section('footer')
<div id="footer">
    <div class="social-links">
        <a href="https://www.facebook.com/esapfoundations" target="_blank"> <span class="fa fa-facebook-square"></span> </a>
        <a href="https://twitter.com/esapfoundations" target="_blank"><span class="fa fa-twitter-square"></span> </a>
        <a href="https://www.youtube.com/channel/UCni8AmW0qHW97RaQsdA7g2g" target="_blank"><span class="fa fa-youtube-square"></span> </a>
        <a href="https://plus.google.com/100996150966323725272/about" target="_blank"><span class="fa fa-google-plus-square"></span> </a>
        <a href="http://linkd.in/1xhYwnZ" target="_blank"><span class="fa fa-linkedin-square"></span> </a>
    </div>

    <div class="row">
        <div class="col-md-4">
            <ul class="list-group list-unstyled ">
                <li class="list-group-item"><a href="{{ URL::to('/') }}">Home</a></li>
                <li class="list-group-item"><a href="{{ URL::to("about") }}">About</a></li>
                <li class="list-group-item"><a href="{{ URL::to("courses") }}">Courses</a></li>
                <li class="list-group-item"><a href="{{ URL::to("training") }}">Trainings</a></li>
                <li class="list-group-item"><a href="{{ URL::to("conference") }}">Conference</a></li>
                <li class="list-group-item"><a href="{{ URL::to('invite') }}">Invite e-SAP</a></li>

            </ul>
        </div>
        <div class="col-md-4">
            <div>
                <ul class="list-group list-unstyled ">
                    <li class="list-group-item"><a target="_blank" href="{{ URL::to("https://www.flickr.com/photos/131652997@N06/") }}">Gallery</a></li>
                    <li class="list-group-item"><a href="{{ URL::to("legs_of_esap") }}">Three Legs of e-SAP</a></li>
                    <li class="list-group-item"><a href="{{ URL::to("credits") }}">Testimonials</a></li>
                    <li class="list-group-item"><a href="{{ URL::to("volunteer") }}">Become e-SAP Volunteer</a></li>
                    <li class="list-group-item"><a href="{{ URL::to("information") }}">Announcements</a></li>
                    <li class="list-group-item"><a href="{{ URL::to("contact") }}">Contact</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-4">
            <div class="fb-like-box" data-href="https://www.facebook.com/esapfoundations" data-width="400" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
        </div>
    </div>
            <br/>

    <div class="modal-footer">
        <strong>e-SAP</strong> Copyright &nbsp; 2014 All Right Reserved
    </div>
</div>
@show()
</body>
</html>

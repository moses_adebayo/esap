<?php
$path = Request::path();

//get the number of active training

$t = new Training();

$t = array();
//$num_active_training = sizeof($t->active_training(TRAINING));
$num_active_training = 0;
//$num_active_conference = sizeof($t->active_training(CONFERENCE));
$num_active_conference = 0;
?>
<nav class="navbar navbar-default  navbar-fixed-top m_nav visible-on" role="navigation">
    <div class="navbar-header">
        <button class="navbar-toggle" data-toggle="collapse" data-target="#menu">
            <span class="sr-only">Menu</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <span class="navbar-brand">
            <a href="{{URL::to('/')}}" >
             <img src="{{asset('images/logo.png') }}" class="hidden-xs" width="80" >
                <span class="visible-xs"><strong>e-SAP</strong></span>
           </a>
        </span>


    </div>
    <div class="clearfix visible-xs"></div>
    <div class="collapse navbar-collapse theme-background" id="menu">
        <a href="{{URL::to('/')}}">
         <img id="logo-descp" src="{{asset('images/esapDesp.png') }}"  width="130" >
        </a>


        <ul class="nav navbar-nav list-unstyled my-menu pull-right">
            <li class="{{ ($path === 'home' || $path === '/') ? 'selected' : ''}}"> {{ HTML::link('/', 'Home') }}</li>
            <?php
                    $active_app = ($num_active_training > 0) ? "Trainings&nbsp;<span class='badge alert-danger'>$num_active_training</span>" : "Trainings";
                    $active_conf = ($num_active_conference > 0) ? "Conferences&nbsp;<span class='badge alert-danger'>$num_active_conference</span>" : "Conferences";

            ?>
            <li class="{{ ($path === 'training') ? 'selected' : ''}}">
                <a href="{{url('training') }}">
                    {{ $active_app }}
                </a>
            </li>
            <li class="{{ ($path === 'conference') ? 'selected' : ''}}">
                <a href="{{url('conference') }}">
                    {{ $active_conf }}
                </a>
            </li>
            <li class="{{ ($path === 'credits') ? 'selected' : ''}}">
                {{ HTML::link('credits', 'Testimonials') }}
            </li>
            <li> <a href="https://www.flickr.com/photos/131652997@N06/" target="_blank">Gallery</a> </li>
            <li class="{{ ($path === 'contact') ? 'selected' : ''}}"> {{ HTML::link('contact', 'Contact') }} </li>
            <?php
                if(Auth::check()){
                    ?>
            <li class="dropdown">
               <a href="#" class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown">
                   <span class="fa fa-user"></span> <span class="text-capitalize">{{User::userInfo(Auth::id())->surname}}</span>
               </a>
                   <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                       <li role="presentation">{{ HTML::link('logout', 'Logout', array('role'=>'menuitem', 'tabindex'=>'-1')) }}</li>
                       <li role="presentation">{{ HTML::link('account', 'My Account', array('role'=>'menuitem', 'tabindex'=>'-1')) }}</li>
                   </ul>
            </li>
             <?php
                }else{
                ?>
            <li class="{{ ($path === 'login') ? 'selected' : ''}}">
                {{ HTML::link('login', 'Login') }}
            </li>
            <?php
                }
            ?>
        </ul>
    </div>
    <div class="clearfix"></div>
</nav>


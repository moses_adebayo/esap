
<nav class="navbar navbar-default  navbar-fixed-top m_nav visible-on" role="navigation">
    <div class="navbar-header">
        <button class="navbar-toggle" data-toggle="collapse" data-target="#menu">
            <span class="sr-only">Menu</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <span class="navbar-brand">
                   <img src="{{asset('images/logo.png') }}" width="80" >
        </span>
    </div>
    <div class="clearfix visible-xs"></div>
    <div class="collapse navbar-collapse theme-background" id="menu">
            <img id="logo-descp" src="{{asset('images/esapDesp.png') }}"  width="150" >

        <ul class="nav navbar-nav list-unstyled my-menu pull-right">
           <li> {{ HTML::link('admin', 'Home') }}</li>
            <li>{{ HTML::link('logout', 'Logout') }}</li>
        </ul>

    </div>

    <div class="clearfix"></div>
</nav>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>

    @section('styles')

    {{ HTML::style('css/bootstrap.min.css')}}
    {{ HTML::style('fonts/font-awesome.min.css')}}
    {{ HTML::style('css/extra-small-devices.css')}}
    {{ HTML::style('css/editBootstrap.css')}}
    {{ HTML::style('css/styles1.css')}}

    @show()

    @section('scripts')
    {{HTML::script('js/jquery.min.js') }}
    {{HTML::script('js/bootstrap.min.js') }}
    {{HTML::script('js/Page.js') }}

    @show()

</head>

<body>
@include('layouts.admin_navbar')

@yield('content')

{{--FOOTER--}}
<div id="footer">
    <div class="social-links">
        <a href="https://www.facebook.com/esapfoundations" target="_blank"> <span class="fa fa-facebook-square"></span> </a>
        <a href="https://www.twitter.com/esapfoundations" target="_blank"><span class="fa fa-twitter-square"></span> </a>
        <a href="#" target="_blank"><span class="fa fa-google-plus-square"></span> </a>
    </div>
    <strong>e-SAP</strong> Copyright &nbsp; 2014 All Right Reserved
</div>
</body>
</html>

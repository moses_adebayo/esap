@extends('layouts.page')

@section('styles')
@parent
{{HTML::style('css/events.css') }}
@stop()

@section('scripts')
@parent
{{HTML::script('js/Courses.js') }}
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container-fluid" id="page-main">
<div class="row">
<div class="col-md-8">
<h3 class="pg-title"><span class="text-lowercase">e</span>-SAP courses</h3>

<div class="list-group padding-all">

<!--                FIRST PAGE-->
<div class="courses-list" id="page_1">

<div class="list-group-item">
    <h4 class="list-group-item-heading">Accounting PeachTree</h4>
    <p class="list-group-item-text">
        As a very lucrative course and a highly demanded knowledge area, this programme is designed to assist students in understanding how to computerize accounting information. It will give students the ability to facilitate greater efficiency in management and financial reporting.
    </p>
    <p class="list-group-text">
        <a href="" data-target="#acc_pea" data-toggle="modal"><strong>Get more</strong></a>
    </p>
    <div class="modal fade" id="acc_pea" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>Accounting PeachTree</strong>
                    <span class="close" data-dismiss="modal" arial-hidden="true">&times</span>
                </div>
                <div class="modal-body">
                    <dl class="dl-horizontal">
                        <dt> Number of Sessions:</dt>
                        <dd>11 Sessions.</dd>
                        <dt> Course Duration</dt>
                        <dd>21 Hours</dd>
                        <dt> Cost</dt>
                        <dd> FREE </dd>
                        <dt> Registration Fee</dt>
                        <dd>&#8358; 2,500</dd>
                    </dl>
                    <h5><strong>CAREER FOCUS</strong> </h5>

                    As a very lucrative course and a highly demanded knowledge area, this programme is designed to assist students in understanding how to computerize accounting information. It will give students the ability to facilitate greater efficiency in management and financial reporting.

                    If you are an accountant, bookkeeper, or small business owner, you will learn how to set up your complete financial system, build and customize a chart of accounts, establish a general ledger, and set your preferences to enter and display information as you need it.PeachTree knowledge will give you edge in any platform of operations/service over and above other accountants without such knowledge, therefore the course is HIGHLY RECOMMENDED.

                    <h5><strong>EDUCATIONAL OBJECTIVES</strong></h5>
                    <div class="padding-all">
                        <ul>
                            <li>
                              You will be taught by expert tutors with many years of accounting and PeachTree knowledge.
                              </li>
                            <li>
                                You will work with actual software using a factitious company we created with real business transactions
                            </li>
                            <li>
                                You will be provided with a PeachTree manuals and relevant study materials to accelerate your understanding.
                            </li>
                            <li>
                                You will be exposed to amazing tricks and tips to ensure greater speed.
                            </li>
                            <li>
                                Once you are trained, you can save lots of time, energy, money and other resources, as you will be able to effectively maintain efficient accounting records without expensive human resources.
                            </li>
                            <li>
                                Peachtree® accounting helps the Accountant / Accounts Personnel's to better manage their accounting & business.
                            </li>
                            <li>
                                Packed with all of the basic invoicing, bill paying, in-depth inventory tracking, payroll, order entry and over 100 customizable reports.
                            </li>
                            <li>
                                Peachtree Accounting gives you the insight behind your numbers.
                            </li>
                            <li>
                                With Peachtree today, the Business Resource Center, you get assistance snapshot of your business financials, along with other functions.
                            </li>
                            <li>
                                And with several new web-related features, Peachtree Accounting Software can help you to establish an internet presence for your business
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="list-group-item">
    <h4 class="list-group-item-heading">French Language</h4>
    <p class="list-group-item-text">
        Knowledge of a second language is essential in over 60 occupations.When you know French, you could become a French teacher, an interpreter or a translator.More than 200 million people speak French on the five continents. French is the second most widely learned foreign language after English, and the ninth most widely spoken language in the world. French is also the only language, alongside English, that is taught in every country in the world.
    </p>
    <p class="list-group-text">
        <a href="" data-target="#fre_lan" data-toggle="modal"><strong>Get more</strong></a>
    </p>
    <div class="modal fade" id="fre_lan" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>French Language</strong>
                    <span class="close" data-dismiss="modal" arial-hidden="true">&times</span>
                </div>
                <div class="modal-body">
                    <dl class="dl-horizontal">
                        <dt> Number of Sessions:</dt>
                        <dd>11 Sessions.</dd>
                        <dt> Course Duration</dt>
                        <dd>21 Hours</dd>
                        <dt> Cost</dt>
                        <dd> FREE</dd>
                        <dt> Registration Fee</dt>
                        <dd>&#8358; 2,500</dd>
                    </dl>

                    <h5><strong>CAREER FOCUS</strong></h5>
                    Knowledge of a second language is essential in over 60 occupations.When you know French, you could become a French teacher, an interpreter or a translator.More than 200 million people speak French on the five continents. French is the second most widely learned foreign language after English, and the ninth most widely spoken language in the world. French is also the only language, alongside English, that is taught in every country in the world.

                    An ability to speak French and English is an advantage on the international job market. A knowledge of French opens the doors of French companies in France and other French-speaking parts of the world (Canada, Switzerland, Belgium, and North and sub-Saharan Africa). As the world’s fifth biggest economy and number-three destination for foreign investment, France is a key economic partner.


                    Speaking French opens up study opportunities at renowned French universities and business schools, ranked among the top higher education institutions in Europe and the world. Students with a good level of French are eligible for French government scholarships to enroll in postgraduate courses in France in any discipline and qualify for internationally recognized French degrees.


                    If you have always believed that you have an international destiny and that your life is not supposed to start and end in Nigeria alone, you should learn French language. French is both a working language and an official language of the United Nations, the European Union, UNESCO, NATO, the International Olympic Committee, the International Red Cross and international courts. If you have ever dreamed of working with an international organization, inability to speak French will shortchange you!

                    <h5><strong>EDUCATIONAL OBJECTIVES</strong></h5>
                    <div class="padding-all">
                        <ul>
                            <li>
                                Understand basic greetings and introductions such as“Bonjour” and“je m'appelle”.
                            </li>
                            <li>
                                Learn various French concepts with visuals and modeling hands-on tools.
                            </li>
                            <li>
                                Get supporting and exciting materials and tools to aid learning.
                            </li>
                            <li>
                                Enjoy a highly interactive and interesting experience.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="list-group-item">
    <h4 class="list-group-item-heading">German Language</h4>
    <p class="list-group-item-text">
        People who speak a second language have better skills in their native language as well, and are also better critical thinkers who are superior problem solvers.
        It's becoming more common for employers to look for language skills on prospective employees' resumes. Having a language like German on your resume gives you an edge over other job candidates.
    </p>
    <p class="list-group-text">
        <a href="" data-target="#ger_lan" data-toggle="modal"><strong>Get more</strong></a>
    </p>
    <div class="modal fade" id="ger_lan" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>German Language</strong>
                    <span class="close" data-dismiss="modal" arial-hidden="true">&times</span>
                </div>
                <div class="modal-body">
                    <dl class="dl-horizontal">
                        <dt>Number of Sessions</dt>
                        <dd>11 Sessions</dd>
                        <dt>Course Duration</dt>
                        <dd>21 Hours</dd>
                        <dt>Registration Fee</dt>
                        <dd>&#8358; 2,500</dd>
                        <dt>Cost</dt>
                        <dd>FREE</dd>
                    </dl>

                    <h5><strong>CAREER FOCUS</strong></h5>
                    People who speak a second language have better skills in their native language as well, and are also better critical thinkers who are superior problem solvers.
                    It's becoming more common for employers to look for language skills on prospective employees' resumes. Having a language like German on your resume gives you an edge over other job candidates. Many companies pay higher salaries to bilingual employees, or offer bonuses based on language skills and proficiency. The more benefit you can bring to a company, the more attractive you are in the job market.Knowledge of German increases your job opportunities with German and foreign companies in your own country and abroad. Proficiency in German helps you to function productively for an employer with global business connections.
                    Germany awards a generous number of scholarships and other support to study in Germany. Working holiday visas are available for young foreigners from a range of countries, and special visas are offered to skilled workers and professionals.A wide range of exchange programs exists for both school and university students between Germany and many countries in the world.
                    German is the second most commonly used scientific language. Germany is the third largest contributor to research and development and offers research fellowships to scientists from abroad.

                    <h5><strong>EDUCATIONAL OBJECTIVES</strong></h5>
                    <div class="padding-all">
                        <ul>
                            <li>
                                Sing songs, play games, and share of personal experiences in German
                            </li>
                            <li>Conduct basic conversations and write simple paragraphs about everyday topics.</li>
                            <li>Get a firm grammatical grasp of the most basic concepts including pronunciation, counting, simple interrogatives, verb conjugation present tense, negation, personal & possessive pronouns, basic word order, etc.</li>
                            <li>You can read, understand and converse on a broad range of topics with greater confidence and precision.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="list-group-item">
    <h4 class="list-group-item-heading">AutoCAD 2D and 3D Modelling</h4>
    <p class="list-group-item-text">
        Individuals who work in or are currently pursuing careers in the architecture, mechanical or engineering fields will discover many benefits of using AutoCAD.AutoCAD is a computer-aided drafting (CAD) software application that enables drafters, architects, engineers, and other professionals to create two-dimensional (2D) and three-dimensional (3D) models of mesh and solid surfaces. Prior to computer-aided drafting, manual hand drafting tools such as drafting boards and pencils, inking pens, parallel rules, compasses, and triangles only offered a subset of what can now be done with programs such as AutoCAD.
    </p>
    <p class="list-group-text">
        <a href="" data-target="#autocad" data-toggle="modal"><strong>Get more</strong></a>
    </p>
    <div class="modal fade" id="autocad" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>AutoCAD 2D and 3D Modelling</strong>
                    <span class="close" data-dismiss="modal" arial-hidden="true">&times</span>
                </div>
                <div class="modal-body">
                    <dl class="dl-horizontal">
                        <dt>Number of Sessions</dt>
                        <dd>11 Sessions</dd>
                        <dt>Course Duration</dt>
                        <dd>21 Hours</dd>
                        <dt>Registration Fee</dt>
                        <dd>&#8358; 2,500</dd>
                        <dt>Cost</dt>
                        <dd>FREE</dd>
                    </dl>
                    <h5><strong>CAREER FOCUS</strong></h5>
                    Individuals who work in or are currently pursuing careers in the architecture, mechanical or engineering fields will discover many benefits of using AutoCAD.AutoCAD is a computer-aided drafting (CAD) software application that enables drafters, architects, engineers, and other professionals to create two-dimensional (2D) and three-dimensional (3D) models of mesh and solid surfaces. Prior to computer-aided drafting, manual hand drafting tools such as drafting boards and pencils, inking pens, parallel rules, compasses, and triangles only offered a subset of what can now be done with programs such as AutoCAD.

                    Since its original release in 1982, AutoCAD quickly became the most widely used CAD program in the world because of its robust set of automated drafting tools and features. AutoCAD allows you to visually design and explore your conceptual design ideas, modify your designs using 3D free-form design tools, generate intelligent model documentation, transform your designs into 3D renderings, and turn them into cinematic-quality animated presentations.

                    Nowadays, employers in the architectural, engineering and drawing-related fields would consider any employee without CAD knowledge as useless to them. Acquire this skill today, you need it!

                    <h5><strong>EDUCATIONAL OBJECTIVES</strong></h5>
                    Be taught by an expert who will not confuse you but will rather simplify AutoCAD modeling for you beyond your wildest imaginations.
                    <div class="padding-all">
                        <strong>Learn how to</strong>
                        <ul>
                            <li>Save time and money and reduce errors with the dynamic engineering model.</li>
                            <li>Reduce purchase, deployment, and support costs with one complete solution.</li>
                            <li>Take full advantage of existing AutoCAD skills to get up to speed quickly.</li>
                            <li>Create production sheets faster.</li>
                            <li>Be sure that production drafting is always in sync with your design.</li>
                            <li>Complete projects faster and reduce the chance of coordination errors using the Civil 3D project environment.</li>
                            <li>Exploit data compatibility.</li>
                            <li>Build a foundation for your custom solution.</li>
                            <li>Clearly communicate design intent and complete final proposals with realistic 3D renderings.</li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="list-group-item">
    <h4 class="list-group-item-heading">Business and Product Branding</h4>
    <p class="list-group-item-text">
        Branding is the process involved in creating a unique name and image for a product in the consumers' mind, mainly through advertising campaigns with a consistent theme. Branding aims to establish a significant and differentiated presence in the market that attracts and retains loyal customers.
    </p>
    <p class="list-group-text">
        <a href="" data-target="#biz-product" data-toggle="modal"><strong>Get more</strong></a>
    </p>
    <div class="modal fade" id="biz-product" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>Business and Product Branding</strong>
                    <span class="close" data-dismiss="modal" arial-hidden="true">&times</span>
                </div>
                <div class="modal-body">
                    <dl class="dl-horizontal">
                        <dt>Number of Sessions</dt>
                        <dd>11 Sessions</dd>
                        <dt>Course Duration</dt>
                        <dd>21 Hours</dd>
                        <dt>Registration Fee</dt>
                        <dd>&#8358; 2,500</dd>
                        <dt>Cost</dt>
                        <dd>FREE</dd>
                    </dl>
                    <h5><strong>CAREER FOCUS</strong></h5>
                    Branding is the process involved in creating a unique name and image for a product in the consumers' mind, mainly through advertising campaigns with a consistent theme. Branding aims to establish a significant and differentiated presence in the market that attracts and retains loyal customers.  Identity is important for any product in today's competitive marketplace. Brand managers are concerned with creating a lasting impression among consumers and improving product sales and market share.

                    A brand manager monitors market trends and oversees advertising and marketing activities to ensure the right message is delivered for their product or service. They work closely with many teams, including product developers, researchers, marketing personnel and creative agencies to make sure their company brand values and image are followed. They work both for consultancies and in-house marketing departments.

                    <h5><strong>BENEFITS</strong></h5>
                    Employment of advertising and marketing managers, a category that may include brand managers, will grow steadily in the coming years. Job growth will occur as the number of products and services increases, making it necessary for brands to stand out in order to succeed. Between 2012 and 2022, job opportunities in the employment category that includes management positions in marketing, promotions and advertising are projected to increase by 12%, according to the Bureau of Labor Statistics. As of May 2013, the average annual salary for marketing managers nationwide was $133,700, with the top 25% earning $166,250 or more, the BLS reported.

                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!--                2nd PAGE      -->
<div class="courses-list hidden" id="page_2">
    <div class="list-group-item">
        <h4 class="list-group-item-heading">Digital Marketing</h4>
        <p class="list-group-item-text">
            SPSS means “Statistical Package for the Social Sciences”. SPSS is a widely used program for statistical analysis in social science. It is also used by market researchers, health researchers, survey companies, government, education researchers, marketing organizations, data miners, and others.
        </p>
        <p class="list-group-text">
            <a href="" data-target="#digi-mar" data-toggle="modal"><strong>Get more</strong></a>
        </p>
        <div class="modal fade" id="digi-mar" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <strong>Digital Marketing</strong>
                        <span class="close" data-dismiss="modal" arial-hidden="true">&times</span>
                    </div>
                    <div class="modal-body">
                        <dl class="dl-horizontal">
                            <dt>Number of Sessions</dt>
                            <dd>11 Sessions</dd>
                            <dt>Course Duration</dt>
                            <dd>21 Hours</dd>
                            <dt>Registration Fee</dt>
                            <dd>&#8358; 2,500</dd>
                            <dt>Cost</dt>
                            <dd>FREE</dd>
                        </dl>
                        <h5><strong>CAREER FOCUS</strong></h5>
                        Digital marketing is marketing that makes use of electronic devices (computers) such as personal computers, smartphones, cellphones, tablets and game consoles to engage with stakeholders. A component of Digital marketing is Digital Brand Engagement. Digital marketing applies technologies or platforms such as websites, e-mail, apps (classic and mobile) and social networks. According to the Digital Marketing Institute, Digital Marketing is the use of digital channels to promote or market products and services to consumers and businesses.

                        Digital marketing skills are in serious demand and the digital skills gap is set to widen, the job market is booming (and quite frankly bursting at the seams) and brands are putting more of a focus on digital marketing than ever before. Bigger budgets, increased pay and more career choice are just some of the benefits digital marketing professionals can look forward to this year and beyond.
                        <h5><strong>COURSE CONTENTS</strong></h5>
                        <ol>
                            <li>Online marketing</li>
                            <li>Google AdSense</li>
                            <li>Google AdWord</li>
                            <li>Social Media Marketing</li>
                            <li>Email Marketing</li>
                            <li>Search Engine Optimization (SEO)</li>
                            <li>Pay per click (PPC)</li>
                            <li>Affiliate Marketing</li>
                            <li>Text messaging</li>
                            <li>Blogging, RSS & News Feeds</li>
                            <li>Viral marketing</li>
                            <li>Content Marketing</li>
                        </ol>


                        <h5><strong>BENEFITS</strong></h5>
                        <div class="padding-all">
                            <ul>
                                <li>Become an In-Demand Professional
                                There’s 150,000 digital jobs predicted by 2020 and not enough digital professionals to fill them. This provides those studying digital marketing with a unique competitive advantage – you’re gearing yourself up for a career where demand exceeds supply. Always a good move.
</li>
                                <li>Benefit from More Career Choice
                                Many of the world’s leading digital giants like Google, LinkedIn and Twitter provides a wealth of job opportunities for digital professionals to choose from. Rarely a week goes by without a new onslaught of digital jobs announced for multinationals, national brands and even SMEs. With such a wide array of new opportunities available digital marketers can afford to get picky about the type of company they’d like to work for. We’d encourage you to take advantage of this choice and think of what kind of business best suits your career needs.
                                </li>
                                <li>Get Paid More Than Your Peers
                                We’ve already talked about how demand for digital marketing professionals is exceeding supply. In traditional economics we all know what happens next – the value of the product increases along with the price. When you’re working in a fruitful industry with a large skills shortage you can think of yourself like a commodity – and negotiate your salary accordingly. A recent survey by Prosperity found that digital salaries are rising faster in 2014 than in the previous five years. What’s more, the pace of growth is set to fasten. For example, the average salary for a digital marketing manager in Dublin is €68,000. And it’s not just managerial level digital marketing positions experiencing a salary surge – entry level PPC positions have jumped from €25,000 last year to €28,000 this year.
                                </li>
                                <li>You Can KickStart Your Own Career
                                In more traditional careers like advertising you’d have to wait for a coveted internship or graduate placement to open up so you can gain experience and create your own portfolio. The digital marketing world, however, provides a host of opportunities for you to kickstart your own career before you even set foot in a workplace. There have been plenty of examples of people who have been hired after showcasing their talent and abilities through social media. For example, Dublin based fashion illustrator, Holly Shortall got noticed when she tweeted her illustrations to celebrities online. A number of glossy magazines and top fashion brands then approached her and commissioned her work.
</li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="list-group-item">
        <h4 class="list-group-item-heading">SPSS Statistical Package</h4>
        <p class="list-group-item-text">
            SPSS means “Statistical Package for the Social Sciences”. SPSS is a widely used program for statistical analysis in social science. It is also used by market researchers, health researchers, survey companies, government, education researchers, marketing organizations, data miners, and others.
        </p>
        <p class="list-group-text">
            <a href="" data-target="#spss" data-toggle="modal"><strong>Get more</strong></a>
        </p>
        <div class="modal fade" id="spss" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <strong>SPSS Statistical Package</strong>
                        <span class="close" data-dismiss="modal" arial-hidden="true">&times</span>
                    </div>
                    <div class="modal-body">
                        <dl class="dl-horizontal">
                            <dt>Number of Sessions</dt>
                            <dd>11 Sessions</dd>
                            <dt>Course Duration</dt>
                            <dd>21 Hours</dd>
                            <dt>Registration Fee</dt>
                            <dd>&#8358; 2,500</dd>
                            <dt>Cost</dt>
                            <dd>FREE</dd>
                        </dl>
                        <h5><strong>CAREER FOCUS</strong></h5>
                        SPSS means “Statistical Package for the Social Sciences”. SPSS is a widely used program for statistical analysis in social science. It is also used by market researchers, health researchers, survey companies, government, education researchers, marketing organizations, data miners, and others.
                        Every organization, not matter how big or small, work with data and every employer is always looking for a way of integrating predictive analytics with decision management, scoring and optimization in the organization's processes and operational systems, and make the right decision every time. If you have the knowledge of SPSS, you’ll always be the right person for most of the important statistical and data analysis job both home and abroad.
                        Decision management capabilities enable the integration of predictive analytics and business rules into an organization’s processes to optimize and automate high-volume decisions at the point of impact. This capability will give you an untold edge in the midst of the hottest and most severe competition out there. Go for it!

                        <h5><strong>EDUCATIONAL OBJECTIVES</strong></h5>
                        <div class="padding-all">
                            <ul>
                                <li>Automate and optimize transactional decisions by combining predictive analytics, rules and scoring to deliver recommended actions in real time.</li>
                                <li>Analyze almost all data. Conduct analysis regardless of where the data is stored.</li>
                                <li>Solve a variety of business problems with an extensive range of analytics.</li>
                                <li>Use SPSS more easily and learn more quickly.</li>
                                <li>Meet your needs with flexible deployment. Choose the deployment that best suits your requirements to optimize your existing IT investment.</li>
                                <li>Use a variety of modeling approaches in a single run and then compare the results of the different modeling methods. Select which models to use in deployment, without having to run them all individually and then compare performance.</li>
                                <li>Go beyond the analysis of structured numerical data and include information from unstructured text data, such as web activity, blog content, customer feedback, emails and social media comments. Capture key concepts, themes, sentiments and trends and ultimately improve the accuracy of your predictive models.</li>
                                <li>Learn how identity resolution is vital in a number of fields, including customer relationship management, national security, fraud detection and prevention of money laundering. Entity analytics improves the coherence and consistency of data by resolving like entities even when the entities do not share any key values.</li>
                                <li>Discover how deployment bridges the gap between analytics and action by providing results to people and processes on a schedule or in real time, and enables organizations to realize the full benefit of predictive analytics.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="list-group-item">
        <h4 class="list-group-item-heading">Java Enterprise Programming Language</h4>
        <p class="list-group-item-text">
            Java programming is a lot easier than it looks. There are thousands of programmers who have used their Java skills to get high-paying jobs in software development, Internet programming, and e-commerce. The last thing any of them wants is for the boss to know that anyone with persistence and a little free time can learn the language, the most popular programming language in use today.Java is one of the best languages to learn because it's a useful, powerful, modern technology that's being used by thousands of programmers around the world.
        </p>
        <p class="list-group-text">
            <a href="" data-target="#java_ent" data-toggle="modal"><strong>Get more</strong></a>
        </p>
        <div class="modal fade" id="java_ent" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <strong>Java Enterprise Programming Language</strong>
                        <span class="close" data-dismiss="modal" arial-hidden="true">&times</span>
                    </div>
                    <div class="modal-body">
                        <dl class="dl-horizontal">
                            <dt>Number of Sessions</dt>
                            <dd>11 Sessions</dd>
                            <dt>Course Duration</dt>
                            <dd>21 Hours</dd>
                            <dt>Registration Fee</dt>
                            <dd>&#8358; 2,500</dd>
                            <dt>Cost</dt>
                            <dd>FREE</dd>
                        </dl>
                        <h5><strong>CAREER FOCUS</strong></h5>
                        Java programming is a lot easier than it looks. There are thousands of programmers who have used their Java skills to get high-paying jobs in software development, Internet programming, and e-commerce. The last thing any of them wants is for the boss to know that anyone with persistence and a little free time can learn the language, the most popular programming language in use today.Java is one of the best languages to learn because it's a useful, powerful, modern technology that's being used by thousands of programmers around the world.

                        This training provides an introduction to the Java language and computer programming itself, especially for people who are using it to learn software development for the first time.
                        <h5><strong>EDUCATIONAL OBJECTIVES</strong></h5>
                        <div class="padding-all">
                            <ul>
                                <li>Get started quickly: Although the Java programming language is a powerful object-oriented language, it's easy to learn, especially for programmers already familiar with C or C++.</li>
                                <li>Get hands-on knowledge and experience with plenty of step-by-step examples of working programs.</li>
                                <li>Develop programs more quickly: The Java programming language is simpler than C++, and as such, your development time could be up to twice as fast when writing in it.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="list-group-item">
        <h4 class="list-group-item-heading">Project Management Professional (PMP Certification Course)</h4>
        <p class="list-group-item-text">
            Project management is one of the essential processes of an organization for the simple reason that it answers a lot of your questions and adds order to the company. With this, project management training is vital to ensure that you have the right skills and knowledge when it comes to managing a project.  Project management training can help you become a better person as you will have better sense of your time and resources.Project management training carries with it several benefits for those that desire to learn the art and science of better management of projects and goals of the organization.
        </p>
        <p class="list-group-text">
            <a href="" data-target="#pro_mgt" data-toggle="modal"><strong>Get more</strong></a>
        </p>
        <div class="modal fade" id="pro_mgt" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <strong>Project Management Professional (PMP Certification Course)</strong>
                        <span class="close" data-dismiss="modal" arial-hidden="true">&times</span>
                    </div>
                    <div class="modal-body">
                        <dl class="dl-horizontal">
                            <dt>Number of Sessions</dt>
                            <dd>6 Sessions</dd>
                            <dt>Course Duration</dt>
                            <dd>11 Hours</dd>
                            <dt>Registration Fee</dt>
                            <dd>&#8358; 2,500</dd>
                            <dt>Cost</dt>
                            <dd>FREE</dd>
                        </dl>
                        <h5><strong>CAREER FOCUS</strong></h5>
                        Project management is one of the essential processes of an organization for the simple reason that it answers a lot of your questions and adds order to the company. With this, project management training is vital to ensure that you have the right skills and knowledge when it comes to managing a project.  Project management training can help you become a better person as you will have better sense of your time and resources.Project management training carries with it several benefits for those that desire to learn the art and science of better management of projects and goals of the organization.
                        Proper project management techniques help managers meet the needs of their business by ensuring their projects remain on time, within budget and under control. Many project managers learn lessons the hard way – this can cost the organization dearly in wasted effort, cost, poor customer reputation, stress and failure to deliver the full benefits. Professional development and training courses can fast track the development of the competencies required to deliver successful projects.
                        <h5><strong>EDUCATIONAL OBJECTIVES</strong></h5>
                        <div class="padding-all">
                            <ul>
                                <li>Acquire enhanced effectiveness and better efficiency in delivering services.</li>
                                <li>Learn how to create Improved, increased, enhanced customer satisfaction, which makes you a great asset to any organization.</li>
                                <li>Learn how to improve growth and development within your team</li>
                                <li> Obtain greater standing and competitive edge: there is nothing like superior performance to secure your place in the marketplace.</li>
                                <li>Project management training will teach you the importance of time and setting of goals and objectives. You will be able to prioritize these goals and it will as well assist you in making great use of your time.</li>
                                <li>You will know the remaining resources and needs of the company. This could be in the form of time, money or manpower. You can make use of project management training to help you determine how to estimate the resources that you will need in future projects and set realistic budgets.</li>
                                <li>Project management training will provide you with the skills that you need in producing well documented project completion proofs.</li>
                                <li>You will be prepared toward the PMP professional exam, which makes you a hot cake in the job market once you pass the exam and are certified.</li>
                                <li>With project management training you can work better with information systems. Project management is not done manually – in this day and age! You need to make use of several applications and programs such as MS Project (Link to the course, “MS Project”)to keep track of the mile stones and progress of your projects.</li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="list-group-item">
        <h4 class="list-group-item-heading">Networking & Networks Administration</h4>
        <p class="list-group-item-text">
            Industries across the career spectrum and around the world depend on computer networking to keep employees connected and business flowing. Networking skills give you an edge and an opportunity to make a career in almost any sector you can imagine: financial services, education, transportation, manufacturing, oil and gas, mining and minerals, technology, government, hospitality, health care, retail… you name it. If you have an interest in a particular field, technology is probably part of it.
        </p>
        <p class="list-group-text">
            <a href="" data-target="#net_adm" data-toggle="modal"><strong>Get more</strong></a>
        </p>
        <div class="modal fade" id="net_adm" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <strong>Networking & Networks Administration</strong>
                        <span class="close" data-dismiss="modal" arial-hidden="true">&times</span>
                    </div>
                    <div class="modal-body">
                        <dl class="dl-horizontal">
                            <dt>Number of Sessions</dt>
                            <dd>11 Sessions</dd>
                            <dt>Course Duration</dt>
                            <dd>21 Hours</dd>
                            <dt>Registration Fee</dt>
                            <dd>&#8358; 2,500</dd>
                            <dt>Cost</dt>
                            <dd>FREE</dd>
                        </dl>

                        <h5><strong>Career Focus</strong></h5>
                        Industries across the career spectrum and around the world depend on computer networking to keep employees connected and business flowing. Networking skills give you an edge and an opportunity to make a career in almost any sector you can imagine: financial services, education, transportation, manufacturing, oil and gas, mining and minerals, technology, government, hospitality, health care, retail… you name it. If you have an interest in a particular field, technology is probably part of it.


                        People with ICT and networking skills are in short supply worldwide. The US Department of Labor estimates the number of jobs for network systems and data communication analysts will grow by 53 percent from 2008 to 2018. In Brazil, these types of jobs will grow from about 60,000 today to more than 115,000 jobs by 2015. The story is the same in country after country from continent to continent. As organizations and institutions invest in mobile devices, cloud computing, social media and big data, they depend on a workforce with networking technology experience. The current number of people working and studying technology simply won't match the expected demand. Individuals who choose to add networking to their studies or professional skills can transform their lives.


                        Networking standards are global. That means your skills and certifications are recognized anywhere in the world your career takes you. Cisco certified professionals have worked their way up through global corporations in places all over the world. They live in every sized community, supporting small businesses, schools, and social services in every town or village where someone connects to the Internet.

                        <h5><strong>Educational objectives</strong></h5>

                        <div class="padding-all">
                            <ul>
                                <li> Get on the road to bagging professional certifications such as CCNA, CCNP, etc.</li>
                                <li>Challenge your skills, open a new career branch that's an inroad to nearly any industry</li>
                                <li>The Network Administration will teach you the fundamental skills for building a solid foundation for a career in the data communications and computer networking field.</li>
                                <li>You will obtain hands-on laboratory experience with network devices including switches, routers, etc.</li>
                                <li>You will learn about local area networks, wide area networks, virtual private networks, intranets, and the Internet</li>
                                <li>You'll be able to apply what you learn in class while working on actual networks and latest technology.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--            3RD PAGE-->
<div class="courses-list hidden" id="page_3">
    <div class="list-group-item">
        <h4 class="list-group-item-heading">Graphics Design</h4>
        <p class="list-group-item-text">
            Graphic designers are visual communicators who design and develop print and electronic media, such as magazines, television graphics, logos and websites. They may be employed by advertising firms, design companies, publishers and other businesses that need design professionals. This career is results-oriented, and graphic designers are concerned with providing final products that meet clients' needs.
        </p>
        <p class="list-group-text">
            <a href="" data-target="#gra_des" data-toggle="modal"><strong>Get more</strong></a>
        </p>
        <div class="modal fade" id="gra_des" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <strong>Graphics Design</strong>
                        <span class="close" data-dismiss="modal" arial-hidden="true">&times</span>
                    </div>
                    <div class="modal-body">
                        <dl class="dl-horizontal">
                            <dt>Number of Sessions</dt>
                            <dd>11 Sessions</dd>
                            <dt>Course Duration</dt>
                            <dd>21 Hours</dd>
                            <dt>Registration Fee</dt>
                            <dd>&#8358; 2,500</dd>
                            <dt>Cost</dt>
                            <dd>#15, 000, now FREE (Courtesy Diamond Bank)</dd>
                        </dl>
                        <h5><strong>Career Focus</strong></h5>
                        Graphic designers are visual communicators who design and develop print and electronic media, such as magazines, television graphics, logos and websites. They may be employed by advertising firms, design companies, publishers and other businesses that need design professionals. This career is results-oriented, and graphic designers are concerned with providing final products that meet clients' needs.

                        According to the U.S. Bureau of Labor Statistics (BLS), employment of graphic designers was expected to increase 13% from 2010-2020 (www.bls.gov). This growth will be the effect of the rising number of advertising and electronic design firms, as well as increasing numbers of products and services that need to be advertised. Graphic designers with expertise in Web design, interactive media and animation were expected to see the greatest job opportunities

                        Probably the most important benefit of being a graphic artist is the ability to express your creativity, while still earning a great living.Being a graphic artist is very rewarding and you can still work in fine art and other areas. Digital media is a crucial building block in a graphic design career and there is great satisfaction when you enable people to move forward in their desired path.

                        <h5><strong>Educational objectives</strong></h5>
                        <div class="padding-all">
                            <ul>
                                <li>Start with principles of design.</li>
                                <li>Build skills with hands-on tutorials.</li>
                                <li>Get into your client's head.</li>
                                <li>Grow your freelance business.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="list-group-item">
        <h4 class="list-group-item-heading">Website Design and Development</h4>
        <p class="list-group-item-text">
            It's incredible to think how much the world has changed since 1991, when the World Wide Web first started. The web has taken over our lives and it's showing no sign of stopping. Who knows where the web will take us in the future? Perhaps in another 10 years every person will have their own website. If one thing's certain, it's that a website is a very valuable thing to have these days.
        </p>
        <p class="list-group-text">
            <a href="" data-target="#web_des" data-toggle="modal"><strong>Get more</strong></a>
        </p>
        <div class="modal fade" id="web_des" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <strong>Website Design and Development</strong>
                        <span class="close" data-dismiss="modal" arial-hidden="true">&times</span>
                    </div>
                    <div class="modal-body">
                        <dl class="dl-horizontal">
                            <dt>Number of Sessions</dt>
                            <dd>11 Sessions</dd>
                            <dt>Course Duration</dt>
                            <dd>21 Hours</dd>
                            <dt>Registration Fee</dt>
                            <dd>&#8358; 2,500</dd>
                            <dt>Cost</dt>
                            <dd>#17,500, now FREE (Courtesy Diamond Bank)</dd>
                        </dl>
                        <h5><strong>Career Focus</strong></h5>
                        It's incredible to think how much the world has changed since 1991, when the World Wide Web first started. The web has taken over our lives and it's showing no sign of stopping. Who knows where the web will take us in the future? Perhaps in another 10 years every person will have their own website. If one thing's certain, it's that a website is a very valuable thing to have these days.

                        The demand for coders far exceeds the supply, so you'll have no troubles finding a job. In fact, it's projected to grow at a rate of 30% between 2010 and 2020. That's twice as fast as most other jobs.The world is practically crying out for more codersand because the supply of coders around the world is so low, the pay is quite attractive too. Google and Facebook employees, for instance, are paid a base salary of $125K.

                        Web Design training courses offer instructions in the basic methods involved in the creation of effective web webpages. The courses equip the student with the technical knowledge, as nicely as an understanding of the mechanical and artistic elements of modern web website design.

                        <h5><strong>Educational objectives</strong></h5>
                        <div class="padding-all">
                            <ul>
                                <li>Increase your employment potential by acquiring this skill</li>
                                <li>Enhance yourlikelihood of havingjuicy job opportunities after graduation.No matter what course you studied.</li>
                                <li>Stay Up To Date With Technologies and Trends.</li>
                                <li>Differentiate Yourself from the Crowd.</li>
                                <li>Under-Promise and Over-Deliver to your clients.</li>
                                <li>End up as a successful web designer.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="list-group-item">
        <h4 class="list-group-item-heading">Android Mobile Apps Development</h4>
        <p class="list-group-item-text">
            Are you a tech-savvy person and do you love technologies? This insight will make you travel and explore the world of Android as to why Android programming should be learnt. Android is the most popular mobile phone technology in use today. In order to learn Android development one needs to be trained in its fundamentals and methodologies and hence proper training is necessary.
        </p>
        <p class="list-group-text">
            <a href="" data-target="#and_mob" data-toggle="modal"><strong>Get more</strong></a>
        </p>
        <div class="modal fade" id="and_mob" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <strong>Android Mobile Apps Development</strong>
                        <span class="close" data-dismiss="modal" arial-hidden="true">&times</span>
                    </div>
                    <div class="modal-body">
                        <dl class="dl-horizontal">
                            <dt>Number of Sessions</dt>
                            <dd>11 Sessions</dd>
                            <dt>Course Duration</dt>
                            <dd>21 Hours</dd>
                            <dt>Registration Fee</dt>
                            <dd>&#8358; 2,500</dd>
                            <dt>Cost</dt>
                            <dd>FREE</dd>
                        </dl>
                        <h5><strong>Career Focus</strong></h5>
                        Are you a tech-savvy person and do you love technologies? This insight will make you travel and explore the world of Android as to why Android programming should be learnt. Android is the most popular mobile phone technology in use today. In order to learn Android development one needs to be trained in its fundamentals and methodologies and hence proper training is necessary.

                        Students with IT background and basic knowledge of programming are just fit in here. However, it could be a wonderful start of adventure for any other person who wishes to delve into the arena of Mobile Application Development. This training can help anyone build a bright future in the Android market which is on the ‘boom’ today.

                        <h5><strong>Educational objectives</strong></h5>
                        <div class="padding-all">
                            You will get clear with the fundamentals of Mobile Application Development which is a highly demanded profession and knowledge areas in the world today.
                            Start your highly promising into the programming world where you can start making things happen!
                            At the completion of this course, you will be able to:
                            <ul>
                                <li>Define the main characteristics and functionality of Android devices</li>
                                <li>Reproduce the installation of the Android Eclipse SKD</li>
                                <li>Define the Android user interface</li>
                                <li>Define Android user input, variables, and operations</li>
                                <li>Customize icons</li>
                                <li>Use decision-making controls</li>
                                <li>Define lists and arrays</li>
                                <li>Implement audio in Android apps</li>
                                <li> Develop Android apps</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="list-group-item basic_info" id="change_email">
        <h4 class="list-group-item-heading">Hardware Engineering</h4>
        <p class="list-group-item-text">
            Computers are an integral part of professional environments in the 21st century and not knowing how to do basic troubleshooting can be a drawback for job seekers. In addition, specialized professionals in computer technology, who can design and program computers and fix complicated problems, are imperative for any organization relying heavily on computers. Therefore, basic or advanced knowledge of computer technology can have a number of advantages for your career.
        </p>
        <p class="list-group-text">
            <a href="" data-target="#har_end" data-toggle="modal"><strong>Get more</strong></a>
        </p>
        <div class="modal fade" id="har_end" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <strong>Hardware Engineering</strong>
                        <span class="close" data-dismiss="modal" arial-hidden="true">&times</span>
                    </div>
                    <div class="modal-body">
                        <dl class="dl-horizontal">
                            <dt>Number of Sessions</dt>
                            <dd>11 Sessions</dd>
                            <dt>Course Duration</dt>
                            <dd>21 Hours</dd>
                            <dt>Registration Fee</dt>
                            <dd>&#8358; 2,500</dd>
                            <dt>Cost</dt>
                            <dd>FREE</dd>
                        </dl>
                        <h5><strong>CAREER FOCUS</strong></h5>
                        Computers are an integral part of professional environments in the 21st century and not knowing how to do basic troubleshooting can be a drawback for job seekers. In addition, specialized professionals in computer technology, who can design and program computers and fix complicated problems, are imperative for any organization relying heavily on computers. Therefore, basic or advanced knowledge of computer technology can have a number of advantages for your career.


                        Most professions use computers for a variety of reasons, but being a computer technology specialist can open opportunities for technical jobs, such as computer programmer, technician, software development engineer and server analyst. As modern workplaces rely heavily on computers, they need specialists to ensure nothing goes wrong with the computer devices or the network. In addition, electronic methods of communication, including multimedia websites and mass email systems, are necessary for large organizations, and employees with computer technology knowledge can set them up.


                        To troubleshoot your computer you can do very well with a simple computer hardware course. You can become your own specialist and forgo spending the money on an IT specialist.


                        Not only will you be able to fix your computer, but you will have the knowledge to build a computer from scratch. The cost of computer parts is often far less expensive than buying a pre-made package. You will also be able to customize your computer so that it is perfectly designed for your needs.

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--            4TH PAGE-->
<div class="courses-list hidden" id="page_4">
<div class="list-group-item">
    <h4 class="list-group-item-heading">Digital Films and Video Editing</h4>
    <p class="list-group-item-text">
        Without film/video editors, movies would last for days, television shows would be completely incoherent, and music videos would look like they were filmed in your parents’ garage. You can’t just take the raw footage from a film shoot, mash it all together and then release it upon the world. Indeed, skilled and experienced film/video editors are required during every single post-production process. Film/video editors use state-of-the-art video editing software, such as Avid Symphony and Montage Extreme, to transform the ingredients of a film (i.e. the sound effects, the CGI, the dialogue and the action) into a refined, tasty treat that the target audience can devour on the screen.
    </p>
    <p class="list-group-text">
        <a href="" data-target="#dig_edi" data-toggle="modal"><strong>Get more</strong></a>
    </p>
    <div class="modal fade" id="dig_edi" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>Digital Films and Video Editing</strong>
                    <span class="close" data-dismiss="modal" arial-hidden="true">&times</span>
                </div>
                <div class="modal-body">
                    <dl class="dl-horizontal">
                        <dt>Number of Sessions</dt>
                        <dd>11 Sessions</dd>
                        <dt>Course Duration</dt>
                        <dd>21 Hours</dd>
                        <dt>Registration Fee</dt>
                        <dd>&#8358; 2,500</dd>
                        <dt>Cost</dt>
                        <dd>FREE</dd>
                    </dl>

                    <h5><strong>CAREER FOCUS</strong></h5>
                    Without film/video editors, movies would last for days, television shows would be completely incoherent, and music videos would look like they were filmed in your parents’ garage. You can’t just take the raw footage from a film shoot, mash it all together and then release it upon the world. Indeed, skilled and experienced film/video editors are required during every single post-production process. Film/video editors use state-of-the-art video editing software, such as Avid Symphony and Montage Extreme, to transform the ingredients of a film (i.e. the sound effects, the CGI, the dialogue and the action) into a refined, tasty treat that the target audience can devour on the screen.


                    Editors typically work alongside the director in an editing suite or studio. They amass all of the recorded material (including sound files and daily rushes), organise it, store it digitally and then arrange it, cutting parts and restructuring scenes until a ‘rough cut’ can be put together. Once the right shots have been sequenced and the sound effects and dialogue have been overdubbed properly, an editor’s job involves putting the finishing touches to everything. This process might involve enriching colours or adding in special effects and stylistic flourishes. Furthermore, film/video editors might work alongside music supervisors, choosing background music for certain scenes to enhance their dramatic effect.

                    Be the creative vision behind the camera. Our video production program immerses students in real client challenges. Gain hands-on experience using professional equipment and learn from instructors who work in the industry. Tell your story the way you envision it.


                    If shooting is like generating the raw material of a film, the edit table is where the film finds its final processed & packaged form. The art of editing, no longer an invisible art as it used to be, is perhaps the very essence of Film Making. The Editor is the one who makes the final decisions about what needs to be seen and heard in a film, while also controlling its rhythm & pace. Extremely technical and yet tremendously artistic, a career in film and TV editing is monetarily as well as creatively satisfying.


                    With the advent of affordable, high quality production and editing technology, careers in video are more attainable than ever. However, these industry changes have also required media professionals to possess a greater range of skills – including the ability to capture compelling video, create trailers, titles, graphics, sound, and more for a variety of outputs – all from a personal laptop or desktop computer.


                    Modern day software allows this video and audio information to be viewed, modified, and eventually played back in real time from the system, without changing the original rushes. There is a technical side to video editing, but there’s also an opportunity to extend storytelling deeper into the production process. Many of the decisions made in the editing phase have a big impact on stories. Pacing, structure and sequencing are just a few of the factors that go into it.
                </div>
            </div>
        </div>
    </div>
</div>

<div class="list-group-item">
    <h4 class="list-group-item-heading">Human Resources Business Professional (HRBP) - HR Certification Course</h4>
    <p class="list-group-item-text">
        In the HR business partner model, the human resource department participates in strategic planning to help the business meet present and future goals. Rather than concentrating solely on HR duties such as benefits, payroll and employee relations, HR departments seek to add value to the company by overseeing recruiting, training, advancement and placement of new and current employees.
    </p>
    <p class="list-group-text">
        <a href="" data-target="#hum_cor" data-toggle="modal"><strong>Get more</strong></a>
    </p>
    <div class="modal fade" id="hum_cor" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>Human Resources Business Professional (HRBP) - HR Certification Course</strong>
                    <span class="close" data-dismiss="modal" arial-hidden="true">&times</span>
                </div>
                <div class="modal-body">
                    <dl class="dl-horizontal">
                        <dt>Number of Sessions</dt>
                        <dd>11 Sessions</dd>
                        <dt>Course Duration</dt>
                        <dd>21 Hours</dd>
                        <dt>Registration Fee</dt>
                        <dd>&#8358; 2,500</dd>
                        <dt>Cost</dt>
                        <dd>FREE</dd>
                    </dl>

                    <h5><strong>CAREER FOCUS</strong></h5>
                    In the HR business partner model, the human resource department participates in strategic planning to help the business meet present and future goals. Rather than concentrating solely on HR duties such as benefits, payroll and employee relations, HR departments seek to add value to the company by overseeing recruiting, training, advancement and placement of new and current employees.

                    The business partner model relieves pressure from management to hone employee job skills for efficiency and productivity, as well as identifying, developing and grooming key employees for advancement. Human resource personnel is also responsible for analyzing employee review data. This allows HR personnel to recognize strengths, which they may then further develop to place employees more effectively, and weaknesses, which they may correct with further job skill training or disciplinary action.

                    Earning your credentials as an HRBP (Human Resource Business Professional) or HRMP (Human Resource Management Professional) makes you a recognized expert in the HR field. This professional distinction sets you apart from your colleagues, showcasing your knowledge and skills. It makes you a more valuable asset, keeping you and your organization more competitive in today's economy.

                    <h5><strong>HR Credentials Represent</strong></h5>
                    <div class="padding-all">
                        <ul>
                            <li>Demonstrated long-term commitment to the HR profession.</li>
                            <li>Furthered education to benefit yourself and your organization.</li>
                            <li>Mastery of the HRBP or HRMP body of knowledge.</li>
                            <li>Developed skills to improve your efficiency.</li>
                            <li>Expanded outlook on the HR field.</li>
                            <li>Ability to keep up with HR developments and bring new ideas to your workplace.</li>
                            <li>Eligibility for new positions at organizations that require certification.</li>
                            <li>Increase in confidence that comes with worldwide recognition as an HR expert.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="list-group-item">
    <h4 class="list-group-item-heading">CCTV Technology and Installation</h4>
    <p class="list-group-item-text">
        CCTV plays a major role in the security systems in place today and has developed very fast in the last few years.The courses can benefit people either working or intending to work as a: - Security Manager, Security Supervisor, Security Guard, CCTV Control Room Employee, CCTV Sales Person, CCTV Protected Premises Owner/Manager, Potential CCTV Purchaser/Advisor, CCTV System Maintenance Employee, CCTV Installer, CCTV Specifier.
    </p>
    <p class="list-group-text">
        <a href="" data-target="#cctv" data-toggle="modal"><strong>Get more</strong></a>
    </p>
    <div class="modal fade" id="cctv" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>CCTV Technology and Installation</strong>
                    <span class="close" data-dismiss="modal" arial-hidden="true">&times</span>
                </div>
                <div class="modal-body">
                    <dl class="dl-horizontal">
                        <dt>Number of Sessions</dt>
                        <dd>11 Sessions</dd>
                        <dt>Course Duration</dt>
                        <dd>21 Hours</dd>
                        <dt>Registration Fee</dt>
                        <dd>&#8358; 2,500</dd>
                        <dt>Cost</dt>
                        <dd>FREE</dd>
                    </dl>
                    <h5><strong>CAREER FOCUS</strong></h5>
                    CCTV plays a major role in the security systems in place today and has developed very fast in the last few years.The courses can benefit people either working or intending to work as a: - Security Manager, Security Supervisor, Security Guard, CCTV Control Room Employee, CCTV Sales Person, CCTV Protected Premises Owner/Manager, Potential CCTV Purchaser/Advisor, CCTV System Maintenance Employee, CCTV Installer, CCTV Specifier.

                    They can also benefit people with technical experience who need to upgrade their skill levels and qualifications such as Alarm Installers, Cable Installation Personnel, Electricians, Fitters, Technicians, etc. OR anyone just wishing to improve their knowledge of CCTV.

                    The true scope for applications is almost unlimited. Some examples are listed below.
                    <div class="padding-all">
                        <ul>
                            <li>Monitoring traffic on a bridge.</li>
                            <li>Recording the inside of a baking oven to find the cause of problems.</li>
                            <li>A temporary system to carry out a traffic survey in a town centre.</li>
                            <li>Time lapse recording for the animation of plasticine puppets.</li>
                            <li>Used by the stage manager of a show to see obscured parts of a set.</li>
                            <li>The well-publicised use at football stadiums.</li>
                            <li>Hidden in buses to control vandalism.</li>
                            <li> Recording the birth of a gorilla at a zoo.</li>
                            <li>Making a wildlife program using a large model helicopter.</li>
                            <li>Reproducing the infrared vision of a goldfish!</li>
                            <li>Aerial photography from a hot air balloon.</li>
                            <li>Production control in a factory.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="list-group-item basic_info" id="change_email">
    <h4 class="list-group-item-heading">DSTV Technology and Installation</h4>
    <p class="list-group-item-text">
        Dish TV has revolutionized the world of satellite television. Through Dish TV in American homes, people can now watch television programs from all across the globe. Be it any sports, you can catch up with all the live action that is available through sports channels on the satellite television network. There are a host of movie channels where you catch up with your favorite genre of movies. No matter whether you are hooked on to the news or are addicted to a particular sitcom, with Dish TV you never have to lose out on your daily dose of entertainment.
    </p>
    <p class="list-group-text">
        <a href="" data-target="#dstv" data-toggle="modal"><strong>Get more</strong></a>
    </p>
    <div class="modal fade" id="dstv" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>DSTV Technology and Installation</strong>
                    <span class="close" data-dismiss="modal" arial-hidden="true">&times</span>
                </div>
                <div class="modal-body">
                    <dl class="dl-horizontal">
                        <dt>Number of Sessions</dt>
                        <dd>11 Sessions</dd>
                        <dt>Course Duration</dt>
                        <dd>21 Hours</dd>
                        <dt>Registration Fee</dt>
                        <dd>&#8358; 2,500</dd>
                        <dt>Cost</dt>
                        <dd>FREE</dd>
                    </dl>

                    <h5><strong>CAREER FOCUS</strong></h5>
                    Dish TV has revolutionized the world of satellite television. Through Dish TV in American homes, people can now watch television programs from all across the globe. Be it any sports, you can catch up with all the live action that is available through sports channels on the satellite television network. There are a host of movie channels where you catch up with your favorite genre of movies. No matter whether you are hooked on to the news or are addicted to a particular sitcom, with Dish TV you never have to lose out on your daily dose of entertainment.

                    Satellite installing is a career choice for many people. It is an industry in demand all over the world. You can work as an installer for one of the major satellite providers, a local retailer or create your own company. It is a rewarding, challenging and profitable profession to become involved in. You will have to be trained properly, certified and other requirements may be needed. Any person willing to spend the time necessary to learn the trade can become a successful satellite installer.

                    The Satellites Technician is responsible for the installation and service of DISH Satellites Systems/Products, performed in the customer's home, in their presence, and without supervision. This may include testing and repairing equipment that receives digital signals for residential customers by performing the following duties:
                    <div class="padding-all">
                        <ul>
                            <li>Uses problem-solving and technical skills to install satellite dishes and related equipment</li>
                            <li>Provides excellent customer experience while maintains a safe work environment</li>
                            <li>Evaluates job site to assess optimal placement of satellite dish and communicates with customers to review the installation process</li>
                            <li>Troubleshoots systems to determine the appropriate resolution for reported problems with usage</li>
                            <li>Provides customer education regarding system usage</li>
                            <li>Manages the administrative processes including van inventory and appropriate documentation</li>
                            <li>Ensures that the highest quality of service is provided to promote superior customer satisfaction</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!--5TH PAGE-->
<div class="courses-list hidden" id="page_5">
<div class="list-group-item">
    <h4 class="list-group-item-heading">Microsoft (MS) Project</h4>
    <p class="list-group-item-text">
        Microsoft Project Server 2010 is built on Microsoft SharePoint® Server 2010, and brings together powerful business collaboration platform services with structured execution capabilities to provide flexible work management solutions. Project Server 2010 unifies project and portfolio management to help organizations align resources and investments with strategic priorities, gain control across all types of work, and visualize performance by using powerful dashboards.
    </p>
    <p class="list-group-text">
        <a href="" data-target="#ms_pro" data-toggle="modal"><strong>Get more</strong></a>
    </p>
    <div class="modal fade" id="ms_pro" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>Microsoft (MS) Project</strong>
                    <span class="close" data-dismiss="modal" arial-hidden="true">&times</span>
                </div>
                <div class="modal-body">
                    <dl class="dl-horizontal">
                        <dt>Number of Sessions</dt>
                        <dd>6 Sessions</dd>
                        <dt>Course Duration</dt>
                        <dd>11 Hours</dd>
                        <dt>Registration Fee</dt>
                        <dd>&#8358; 2,500</dd>
                        <dt>Cost</dt>
                        <dd>FREE</dd>
                    </dl>

                    <h5><strong>CAREER FOCUS</strong></h5>
                    Microsoft Project Server 2010 is built on Microsoft SharePoint® Server 2010, and brings together powerful business collaboration platform services with structured execution capabilities to provide flexible work management solutions. Project Server 2010 unifies project and portfolio management to help organizations align resources and investments with strategic priorities, gain control across all types of work, and visualize performance by using powerful dashboards.

                    When planning and executing IT project plans, defaulting to using Excel could impede your progress. It could be worth learning Microsoft Project.
                    With Microsoft Project, you will be able to achieve the following:
                    <div class="padding-all">
                        <ul>
                            <li>Unified project and portfolio management.</li>
                            <li>Drive accountability and control with governance workflow.</li>
                            <li>Standardize and streamline project initiation.</li>
                            <li>Select the right portfolios that align with strategy.</li>
                            <li>Effectively prioritise and communicate business strategy.</li>
                            <li>Run what-if analyses under varying constraints.</li>
                            <li>Proactively reschedule projects to maximise resource utilization.</li>
                            <li>Easily build Web-based project schedules.</li>
                            <li>Intuitively submit time and task updates.</li>
                            <li>Gain visibility and control through reports and dashboards.</li>
                            <li>Simplified administration and flexibility.</li>
                            <li>Gain additional value from the Microsoft platform.</li>
                            <li>Extensible and programmable platform.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="list-group-item">
    <h4 class="list-group-item-heading">Beads, Wireworks and Gele Tying</h4>
    <p class="list-group-item-text">
        It was a vocation she ventured into by accident, but now she has managed to turn it into a very successful and highly rewarding enterprise. YemisiOludiran, a graduate of Lagos State University (LASU) went into the business of bead making and wireworks about three years ago, while the Academic Staff Union of Universities (ASUU) embarked on one of its famous industrial actions.
    </p>
    <p class="list-group-text">
        <a href="" data-target="#bead" data-toggle="modal"><strong>Get more</strong></a>
    </p>
    <div class="modal fade" id="bead" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>Beads, Wireworks and Gele Tying</strong>
                    <span class="close" data-dismiss="modal" arial-hidden="true">&times</span>
                </div>
                <div class="modal-body">
                    <dl class="dl-horizontal">
                        <dt>Number of Sessions</dt>
                        <dd>6 Sessions</dd>
                        <dt>Course Duration</dt>
                        <dd>11 Hours</dd>
                        <dt>Registration Fee</dt>
                        <dd>&#8358; 2,500</dd>
                        <dt>Cost</dt>
                        <dd>FREE</dd>
                    </dl>

                    <h5><strong>CAREER FOCUS</strong></h5>
                    It was a vocation she ventured into by accident, but now she has managed to turn it into a very successful and highly rewarding enterprise. YemisiOludiran, a graduate of Lagos State University (LASU) went into the business of bead making and wireworks about three years ago, while the Academic Staff Union of Universities (ASUU) embarked on one of its famous industrial actions.

                    Deciding not to sit at home as the strike lasted, she  proceeded for a beads and wireworks training, where she learnt the art of manipulating wires and beads to form different designs of beads, necklaces, bangles, headpins, earrings, rings and pendants. Ever since then, she has never had cause to regret her decision. Today, the buzz of beads and wireworks is sweeping over the fashion world, Nigeria inclusive, and Yemisi’s is just one among the hordes of stories of young entrepreneurs making good money from the current Beads and Wireworks revolution.

                    In years past, beads and wireworks were considered fashion items belonging to the old school, but it appears like things are taking a new turn today. Among both the young and the old, they are now seen as a collector’s item and a must-have for any fashionable person. Taking a look around, especially during wedding ceremonies and other social events, it is clear that these items are now the vogue. To add impetus to this, several young Nigerians have embraced this craft as a way of solving their unemployment problems and they are not regretting it. The business is also proving to be a very profitable one, probably because of the paradigm shift in fashion and style amongst ladies, with beads and wireworks gradually replacing Gold and Silver necklaces and bangles as more creative designs show up on a daily basis.

                    Starting this business is very easy as the raw materials for bead making are easily sourced and the duration for training is always short, which makes the business very attractive to women, irrespective of their financial status. With as little as N10, 000 one can successfully set up the business of beads and wireworks.

                    <h5><strong>Gele Tying</strong></h5>
                    Gone are those days when Africans find it difficult to make a living from contemporary African dresses or attires in a foreign land. The amazing fact is that gele (head tie) is associated with Nigerian women, but funny enough this time around, the king of gele in a foreign land happens to be a Nigerian male. Hakeem Oluwasegun Olaleye, aka Segun Gele, is a man making a name for himself in a woman's world. To meet him is to understand how he became a celebrity in a field only a few years in the making. He is a Houston-based businessman who makes his living from tying geles (African headtie) for women. Watching SegunGele whip the material into graceful folds and arcs in less than five minutes, evidently supports his mastery of the trade.

                    He earns approximately $60,000.00 per annum just from tying geles and has been global trotting every weekend for events since his business started. He’s not only a vivacious self-promoter; he’s also clearly thrilled to find himself making money doing something that comes so naturally to him.
                    "Gele" is a Yoruba {Nigeria, West Africa} word for a female head wrap. A "head wrap" is a long piece of cloth that you wrap and tuck on your head to create different looks. Geles come in different fabrics such as damask, jacquard, net and “aso-oke” (hand-woven fabrics popular for Yoruba special occasions in Nigeria).

                    The most popular fabric among Nigerian women is a metallic fabric made from jacquard. Gele is often used to grace a woman’s outfit, although most women tie it when they are wearing traditional African clothes, this is slowly changing and the use of gele is being revolutionized to tying it with Western dresses or skirts. Also it is not uncommon to see women at African events all wearing the same gele in a uniform; this is called “asoebi” in Yoruba.
                </div>
            </div>
        </div>
    </div>
</div>

<div class="list-group-item">
    <h4 class="list-group-item-heading">Cake Designs and Decorations</h4>
    <p class="list-group-item-text">
        A cake decorator is a culinary professional that is responsible for turning a simple cake into a mountain of confectionary perfection. Cake decorators use their creative flair to make cake for a client's wedding day. Having baking skills and a background in the culinary arts benefits aspiring wedding cake decorators. They work closely with clients to gather ideas, so they need to be able to visualize the finished product.A cake decorator designs, bakes, and garnishes wedding cakes for customers.
    </p>
    <p class="list-group-text">
        <a href="" data-target="#cdd" data-toggle="modal"><strong>Get more</strong></a>
    </p>
    <div class="modal fade" id="cdd" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>Cake Designs and Decorations</strong>
                    <span class="close" data-dismiss="modal" arial-hidden="true">&times</span>
                </div>
                <div class="modal-body">
                    <dl class="dl-horizontal">
                        <dt>Number of Sessions</dt>
                        <dd>6 Sessions</dd>
                        <dt>Course Duration</dt>
                        <dd>11 Hours</dd>
                        <dt>Registration Fee</dt>
                        <dd>&#8358; 2,500</dd>
                        <dt>Cost</dt>
                        <dd>FREE</dd>
                    </dl>
                    <h5><strong>CAREER FOCUS</strong></h5>
                    A cake decorator is a culinary professional that is responsible for turning a simple cake into a mountain of confectionary perfection. Cake decorators use their creative flair to make cake for a client's wedding day. Having baking skills and a background in the culinary arts benefits aspiring wedding cake decorators. They work closely with clients to gather ideas, so they need to be able to visualize the finished product.A cake decorator designs, bakes, and garnishes wedding cakes for customers.

                    After discussing ideas with clients, wedding cake decorators ensure the images get brought to life. They must have excellent baking skills and manage their time well. Taking notes and communicating with the couple helps cake decorators translate the vision, and their creativity should lend itself to tying the cake into a client's wedding theme. Wedding cake decorators need to have reliable transportation to get their finished product to the wedding site.


                    Today, decorated cakes are often used to celebrate all sorts of events and milestones. For example, birthdays, along with weddings, usually aren't complete without a decorated cake. Other events, like retirements, graduations, and new additions can also be celebrated with a decorated cake. Some individuals even add lavishly decorated cakes to holiday celebrations. For many people, the taste of the cake is only as good as the decorated icing on the cake.

                    <h5><strong>Interior designs and decorations</strong></h5>
                    Professionals who specialize in interior decorating are called interior designers or decorators. They beautify interior spaces and make them functional and safe. Though they may have their own studios and offices from which to plan projects, they often visit project sites to oversee work and check progress. Most work full time, but may adjust their schedules for the convenience of their clients. Their salaries and benefits depend on their employers and location.
                </div>
            </div>
        </div>
    </div>
</div>

<div class="list-group-item">
    <h4 class="list-group-item-heading">Events Planning and Management</h4>
    <p class="list-group-item-text">
        An event planner organizes and facilitates special events, large gatherings, and functions that are often of a celebratory nature. The events planner may be employed by a large corporation that frequently holds such functions for employees or clients. It is often common that businesses in the hospitality industry also have their own event planner on staff to help individuals and businesses execute these special gatherings.
    </p>
    <p class="list-group-text">
        <a href="" data-target="#eve_pm" data-toggle="modal"><strong>Get more</strong></a>
    </p>
    <div class="modal fade" id="eve_pm" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>Events Planning and Management</strong>
                    <span class="close" data-dismiss="modal" arial-hidden="true">&times</span>
                </div>
                <div class="modal-body">
                    <dl class="dl-horizontal">
                        <dt>Number of Sessions</dt>
                        <dd>6 Sessions</dd>
                        <dt>Course Duration</dt>
                        <dd>11 Hours</dd>
                        <dt>Registration Fee</dt>
                        <dd>&#8358; 2,500</dd>
                        <dt>Cost</dt>
                        <dd>FREE</dd>
                    </dl>
                    <h5><strong>CAREER FOCUS</strong></h5>
                    An event planner organizes and facilitates special events, large gatherings, and functions that are often of a celebratory nature. The events planner may be employed by a large corporation that frequently holds such functions for employees or clients. It is often common that businesses in the hospitality industry also have their own event planner on staff to help individuals and businesses execute these special gatherings.


                    The most important skill of an event planner is organization. To do this job well, the planner must be able to set up events that may be attended by as few as 10 people or as many as thousands. They need to be able to make smart decisions on the needs for food and refreshments to suit the occasion. Each event may have its own needs and requirements, and it is up to the planner to ensure that a feasible schedule is drawn up and followed. It is also important for an event planner to be skilled in negotiating prices for goods, services, and facilities; they should establish and maintain relationships with quality purveyors and providers, while ensuring that all pricing remains competitive.

                    <h5><strong>Event Planner Tasks</strong></h5>
                    Coordinate transportation and parking, location support, arrangement of decor and furniture, and emergency support.
                    Identify and secure venues for events in addition to acquiring permits.
                    Determine logistics, including food and beverage needs and establish date(s) and alternate dates for event.

                    <h5><strong>Benefits of a Career as an Event Planner</strong></h5>
                    <div class="padding-all">
                        <ul>
                            <li>Recognition:Events involve people – usually large groups of people – and you'll fast become known as the person behind the scenes who gets the job done and makes sure everything is done flawlessly. Event planning is a great way to get to know your community, and will help you network with the movers and shakers in your town, your state, and even beyond.</li>
                            <li>Financial Rewards:With the increased recognition of event planning as an industry, professionals in the field are seeing an increase in compensation in both the business and non-profit arenas.  Event planners can expect to earn a bit less comedians.  Of course, if you start your own event planning business, your income potential is much higher, and will reflect the growth and success of your business from year to year.</li>
                            <li>Personal Satisfaction:If you've ever known the satisfaction that comes from working incredibly hard on a challenge and seeing outstanding results, you can understand why event planning can be so rewarding on a personal level. Financial and professional rewards are fine, but what a gift it is to find a career that pays you to have this much fun!</li>
                            <li>Flexibility:If you are looking for a job that will allow you some flexibility in setting your own schedule, event planning might be a good fit. Many smaller organizations and businesses can't afford a full-time event planner. And recent downsizing in some fields have left remaining staff overworked. This creates an opportunity for part-time or seasonal work rather than a rigid 8am-to-6pm routine.</li>
                            <li>Independence:Because of the timely and transient nature of event planning, the field lends itself beautifully to independent consulting. If you've ever wanted to start your own business, this field offers some terrific opportunities.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>

<button class="btn  btn-default course_page selected-page " id="page_1">1</button>
<button class="btn btn-default course_page" id="page_2">2</button>
<button class="btn  btn-default course_page" id="page_3">3</button>
<button class="btn  btn-default course_page" id="page_4">4</button>
<button class="btn  btn-default course_page" id="page_5">5</button>
</div>

<div class="col-md-4">
    {{-- INFORMATION PANEL --}}
    @include('utilities.info_panel')
</div>
</div>
</div>


</div>



</div>
@stop()


@extends('layouts.page')

@section('styles')
@parent
{{HTML::style('css/events.css') }}
@stop()

@section('scripts')
@parent

@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container" id="page-main">
    <?php
    //      var_dump($data);

    ?>
    <div class="col-xs-12">
        <ul class="event-list">
            <?php
            foreach($data as $d){
                $format_date = Olajuwon::format_date($d->date_created);
                ?>
                <li>
                    <time datetime="<?php echo $d->date_created?>">
                        <span class="day"><?php echo $format_date['day']; ?></span>
                        <span class="month"><?php echo $format_date['month']; ?></span>
                        <span class="year"><?php echo $format_date['year']; ?></span>
                    </time>
                    <div class="info">
                        <h2 class='title'><?php echo $d->title; ?></h2>
                        <p class='desc'><?php echo str_limit($d->description, 325); ?></p>
                    </div>
                </li>
            <?php
            }
            ?>
        </ul>
    </div>
    <?php
    echo $data->links();

    ?>

</div>
@stop()


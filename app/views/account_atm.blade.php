@extends('layouts.page')

@section('styles')
@parent
@stop()

@section('scripts')
@parent
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container" id="page-main">
    <div class="row">
        <div class="col-md-7">
            <div class="acc_pane">

                <h5 class="text-right small">Hi! <?php echo ucwords($user->surname." ".$user->first_name)?></h5>

                <h3 class="pg-title">Account Balance</h3>
                <p class="large_font">
                    <strong class="<?php echo ($account->balance < 2000) ? "text-danger" : "theme-color"; ?>"><?php echo Olajuwon::format_money($account->balance) ?></strong>
                </p>
                   @if(empty($active_app))
                        <p class="text-muted">No active application</p>
                   @else
                   <h3 class="pg-title">Active Applications</h3>
                        <ul>
                            @foreach($active_app as $app)
                               <li>
                                    <a class="black-text" href="{{ URL::to("apply/".$app->code) }}">
                                        {{ $app->name }}
                                        <p class="small text-info">Starts on {{$app->start_date}}</p>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                   @endif

                   @if(!empty($past_app))
                        <h3 class="pg-title">Past Trainings</h3>
                        <ul>
                            @foreach($past_app as $app)
                               <li>
                                        {{ $app->name }}
                                        <p class="small">
                                            {{$app->start_date}}
                                            @if($app->end_date !== '0000-00-00')
                                                till {{  $app->end_date }}
                                            @endif
                                        </p>
                                </li>
                            @endforeach
                        </ul>
                    @endif
            </div>

        </div>

        <div class="col-md-5">
            <div class="acc_pane acc_pane_odd">

                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#by_voucher" data-toggle="tab">By Voucher</a>
                    </li>
                    <li>
                        <a href="#by_deposit" data-toggle="tab">By Deposit</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="by_voucher">
                        <?php
                            if(isset($load_response)){
//                                var_dump($load_response);
                                if($load_response['status']){
                                    ?>
                                <div class="alert alert-success">
                                    <p><?php echo "Your account has been credited"; ?></p>
                                </div>
                        <?php
                                }else{
                                    ?>
                                <div class="alert alert-danger">
                                    <p><?php echo $load_response['msg']; ?></p>
                                </div>
                        <?php
                                }
                            }
                        ?>
                        <form role="form" action="{{route('load_voucher')}}" method="post">
                            <div class="form-group">
                                <label for="voucher">Enter voucher digit</label>
                                <input required id="voucher" class="form-control" type="text" name="voucher" placeholder="Code is case sensitive">
                            </div>
                            <button type="submit" class="large_button btn my-btn">Recharge</button>
                        </form>
                    </div>

                    <div class="tab-pane fade" id="by_deposit">
                        By Deposit
                    </div>
                </div>


            </div>

        </div>

    </div>

</div>
@stop()


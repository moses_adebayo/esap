<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    {{ HTML::style('css/bootstrap.min.css')}}

</head>
<style>
    *{
        font-family: "Segoe UI", "Helvetica Neue", Helvetica, Arial, sans-serif;
    }
    h2{
        text-align: center;
    }
    th{
        text-align: center;
    }
    #slip_content{
        padding: 4em;
    }
    #footer{
        text-align: center;
        margin-top: 5em;
    }
</style>
<body>
<?php

?>
<div id="slip_content">
    <img src="{{asset('images/logo.png') }}">

    <h2>{{ $data['training_name'] }}</h2>

    <h3><strong>Applicant Name: </strong>{{ $data['applicant_name'] }}</h3>
    <h3><strong>Registration Number:</strong> {{  $data['reg_num']; }}</h3>
    <p>
        <strong>Venue: </strong>{{ $data['venue']; }}
    </p>
    <p>
        <strong>Date: </strong> {{ $data['start_date']; }}
              @if($data['end_date'] !== '0000-00-00')
                {{" till ". $data['end_date'] }} ?>
              @endif
    </p>
    <hr/>
    <h4>Training cost: <?php echo $data['cost']; ?></h4>

    <div id="footer">

        <strong>e-SAP</strong> Copyright &nbsp; 2014 All Right Reserved
    </div>

</div>
</body>

<script>
    window.print();
</script>
</html>

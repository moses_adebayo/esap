@extends('layouts.page')

@section('styles')
@parent
{{HTML::style('css/events.css') }}
@stop()

@section('scripts')
@parent
{{HTML::script('js/jquery.refineslide.js') }}
{{HTML::script('js/Slider.js') }}
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container" id="page-main">
    <div class="row">
        <div class="col-md-6">
            <h3 class="pg-title">Career and Internship</h3>
            <div class="alert alert-danger" style="font-size: 2em">
                <span class="fa fa-pulse fa-spinner">&nbsp;</span> This feature is coming soon!
            </div>
    </div>
    <div class="col-md-4 col-sm-offset-2">
        @include('utilities.info_panel')
    </div>
</div>



</div>
@stop()


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    {{ HTML::style('css/bootstrap.min.css')}}

</head>
<style>
    *{
      font-family: "Segoe UI", "Helvetica Neue", Helvetica, Arial, sans-serif;
    }
    h2{
        text-align: center;
    }
    th{
        text-align: center;
    }
    #slip_content{
        padding: 4em;
    }
    #footer{
        text-align: center;
        margin-top: 5em;
    }
</style>
<body>
<?php

?>
<div id="slip_content">
    <div class="col-xs-2">
        <img src="{{asset('images/logo.png') }}" width="50" class="img-responsive">
    </div>
    <div class="col-xs-10">
        <h2 style="margin: 0;">Extra-mural Skills Acquisition Programme (e-SAP)</h2>
    </div>
    <div class="clearfix"></div>
<hr/>
    <h2>{{ $data['training_name'] }}</h2>

    <h3><strong>Applicant Name: </strong>{{ $data['applicant_name'] }}</h3>
    <h3><strong>Registration Number:</strong> {{  $data['reg_num']; }}</h3>
    <p>
        <strong>Venue: </strong>{{ $data['venue']; }}
    </p>
    <p>
        <strong>Date: </strong> {{ date("F j, Y", strtotime($data['start_date'])). "<strong> till </strong>" . date("F j, Y", strtotime($data['end_date']))  }}
    </p>
    <hr/>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>S/N</th>
            <th>Course Name</th>
            <th>Courses Code</th>
            <th>Courses Cost</th>
        </tr>
        </thead>

        <tbody>
        <?php
        $counter = 1;
        foreach($data['courses'] as $c){
            ?>
            <tr>
                <td><?php echo $counter ?></td>
                <td><?php echo $c['course_name'] ?></td>
                <td><?php echo $c['course_code'] ?></td>
                <td><?php echo $c['course_cost'] ?></td>
            </tr>
            <?php
            $counter +=1;
        }
        ?>

        </tbody>
    </table>
      <hr/>
    <h4> Total Training cost: <?php echo $data['cost']; ?></h4>

    <div id="footer">

        <strong>e-SAP</strong> Copyright &nbsp; 2014 All Right Reserved
    </div>

</div>
</body>

<script>
    window.print();
</script>
</html>

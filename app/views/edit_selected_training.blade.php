@extends('layouts.page')

@section('styles')
    @parent
    {{HTML::style('css/events.css') }}
@stop()

@section('scripts')
    @parent
    {{HTML::script('js/EditCourses.js') }}
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
    <div class="container-fluid" id="page-main">
        <div class="row">
            <div class="col-sm-7">
                <div class="acc_pane">
                    <div class="alert alert-info">
                        <h4>Available Courses</h4>
                    </div>
                    @foreach($courses as $course)
                        <div class="form-group">
                            <div class="course checkbox">
                                <label>
                                    <input type="checkbox" class="select_course" data-course-cost="{{ $course->reg_cost }}" data-course-id = "{{$course->course_id}}" id="{{$course->course_id}}" data-options='{"cost":"{{$course->reg_cost}}", "name":"{{ $course->course_name }}", "course_id":"{{$course->course_id; }}"}' >
                                    {{$course->course_name}}
                                </label>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-sm-4 col-sm-offset-1">
                <div>
                    <div class="acc_pane acc_pane_odd" >
                        <a target="_blank" href="{{ URL::to('timetable') }}" class="theme-color">
                            <button class="btn my-btn btn-sm">Training Time-Table</button>
                        </a>
                        <h3>Total Cost</h3>
                        <div class="alert alert-success">
                            <div id="cost"></div>
                        </div>
                        <h3>Courses Selected</h3>
                        <div id="courses_selected">
                            <div class="alert alert-success">
                                <ul>
                                    @foreach($data as $d)
                                        <li class="selected_course" data-course-id="{{$d->course_id}}">{{ $d->course_name }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="padding-all text-center">
                        <button class="btn my-btn hidden large_button" id="edit" data-options ='{"training_id": "{{$data[0]->training_id}}", "training_code": "{{$courses[0]->code}}", "application_id": "{{ $data[0]->application_id }}" }'>Update</button>
                        <div id="app-success" class="margin-all alert alert-success hidden text-center"></div>
                        <div id="app-error" class="margin-all alert alert-danger hidden text-center"></div>
                    </div>
                    <div class="loading hidden">
                        <img src="{{ asset('images/loading.gif') }}">
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop()

@section('footer')

@stop()
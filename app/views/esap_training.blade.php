@extends('layouts.page')

@section('styles')
@parent
{{HTML::style('css/events.css') }}
@stop()

@section('scripts')
@parent
{{HTML::script('js/jquery.refineslide.js') }}
{{HTML::script('js/Courses.js') }}
@stop()

{{-- NAVBAR APPEAR HERE --}}
@section('content')
<div class="container-fluid" id="page-main">
<div class="row" id="top">
<div class="col-md-8">
<div class="padding-all">

<h3 class="pg-title">Our Trainings</h3>
<p>
The training sub-channel of what we do is where we identify certain skills that are capable of making our youths become highly valuable individuals, especially those that are in high demand in the employment market ad bring such skills (that are usually very expensive to acquire) down to various campuses so that students can acquire them and have more than mere academic achievement during their stay on campus.
</p>
<p>
        Our trainings are in various categories; they include but not limited to:
</p>
<ol>
    <li>Vocational courses</li>
    <li>Entrepreneurship courses</li>
    <li>Information Technology courses</li>
    <li>Corporate courses</li>
    <li>Professional courses</li>
    <li>Life courses</li>
    <li>Specialized training for the unemployed graduates</li>
</ol>
<p>
There are currently over 25 free <a href="{{URL::to('courses')}}">courses</a> which are designed to add specific and special values to anyone who undergo them. These courses have been so structured to achieve our aims and objectives and as such, anyone who go through these courses will always be preferred by employers – he will not be unemployed, neither will he ever have to beg around for jobs but will become very valuable to employers.
The only challenge such a person will then have is deciding which of the numerous job-offers to go with!
</p>

<p>
Click <a href="{{URL::to('courses')}}">here</a> to view the full list of our courses.

</p>



</div>


</div>

<div class="col-md-4">
    {{-- INFORMATION PANEL --}}
    @include('utilities.info_panel')
</div>
</div>
</div>


</div>



</div>
@stop()


<?php

class AccountController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    |Account Controller
    |--------------------------------------------------------------------------
    |
    |
    |
    */

//    OPEN ACCOUNT PAGE
    public function open(){
        $data = User::userInfo(Auth::id());
        $account = Account::get_balance(Auth::id());
        $active_app = Training::getAllUserActiveApplications(Auth::id());
        $past_app = Training::getAllUserPastApplications(Auth::id());
        return View::make('account', array("user"=>$data, "account"=>$account, 'active_app'=>$active_app, 'past_app'=>$past_app));
    }
}


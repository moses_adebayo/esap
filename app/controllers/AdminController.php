<?php

class AdminController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    |Admin Controller
    |--------------------------------------------------------------------------
    |
    |
    |
    */

//  OPEN Admin homepage
    public function home(){
        $data = User::userInfo(Auth::id());
        if($data->user_type == 1){
            $info = Admin::unusedVoucher();
            $history = Admin::rechargeHistory();
            return View::make('admin.index', array("user"=>$data, "info"=>$info, "history"=>$history));

        }else{
            return Redirect::to('/');
        }
    }

    public function tellerView(){
        $history = Admin::confirmedTellers();
        return View::make('admin.teller_panel', array('history'=>$history));
    }

    //Generate voucher
    public function generateVoucher(){
        $data = Admin::generateVoucher();
        $info = Admin::unusedVoucher();
        $history = Admin::rechargeHistory();
        return View::make('admin.index', array("v_response"=>$data, "info"=>$info, "history"=>$history));
    }

    //print voucher
    public function printVoucher(){
        if(Session::token() !== Input::get('_token')){
            return Redirect::route('homepage');
        }else{
            $data = Admin::printVoucher();
            return View::make('admin.voucher_print', array("data"=>$data));
        }

    }


    //load voucher
    public static function loadVoucher(){
        $request = Admin::loadVoucher(Input::get('voucher'));
        return $request;
// return 1;
    }

    //send deposit teller number to system
    public static function sendSlipDetails(){
        $data = array('tellerNumber'=>Input::get('teller_number'), 'amount'=>Input::get('amount'));
        $request = Admin::sendSlip($data);
        return $request;
    }

//    user enter slip details of the teller
    public static function checkSlipDetails(){
        $data = array('tellerNumber'=>Input::get('teller_number'), 'amount'=>Input::get('amount'));
        $request = Admin::checkSlip($data);
        return $request;
    }
    //open start training form
    public static  function startTrainingForm(){
        $courses = Course::allCourses();
        $active_training = Training::active_training(TRAINING);
        $all_training = Training::all_training(TRAINING);
        $inactive_training = Training::inactive_training(TRAINING);
        $running_training = Training::runningTraining(TRAINING);

        return View::make('admin.training', array("courses"=>$courses, "active_training"=>$active_training, "all_training"=>$all_training, "inactive_training"=>$inactive_training, "running_training"=>$running_training));
    }
    public static  function startConferenceForm(){
        $active_training = Training::active_training(CONFERENCE);
        $all_training = Training::all_training(CONFERENCE);
        $inactive_training = Training::inactive_training(CONFERENCE);
        $running_training = Training::runningTraining(CONFERENCE);

        return View::make('admin.conference', array("active_training"=>$active_training, "all_training"=>$all_training, "inactive_training"=>$inactive_training, "running_training"=>$running_training));
    }

    //open training
    public static function openTraining(){
        $data['id'] = Input::get('t_id');
        Admin::openTraining($data);

        return Redirect::route('form_training');

    }
     public static function openConference(){
            $data['id'] = Input::get('t_id');
            Admin::openTraining($data);

            return Redirect::route('form_conference');

        }

//end training
    public static function closeTraining(){
        $data['id'] = Input::get('t_id');
        Admin::closeTraining($data);

        return Redirect::route('form_training');
    }
     public static function closeConference(){
            $data['id'] = Input::get('t_id');
            Admin::closeTraining($data);

            return Redirect::route('form_conference');
        }

    //terminate training from users access
    //open training
    public static function terminateTraining(){
        $data['id'] = Input::get('t_id');
        Admin::terminateTraining($data);

        return Redirect::route('form_training');

    }

    public static function terminateConference(){
            $data['id'] = Input::get('t_id');
            Admin::terminateTraining($data);

            return Redirect::route('form_conference');

        }


    //create training
    public static  function  createTraining(){
        $response['status'] = true;

        if(Session::token() !== Input::get('_token')){
            return View::make('illegal');
        }else{
            $status = true;
            $form_errors = array();
            if(!isset($_POST['courses'])){
                $form_errors['course'] = "No course selected";
                $status = false;
            }
            foreach(Input::get() as $key=>$fields){
                if(empty($fields)){
                    $form_errors[$key]= "Field is required";
                    $status = false;
                }
            }
            if($status){
                $input['training'] = array(
                    "name"=>Input::get('training_name'),
                    "location"=>Input::get('location'),
                    "code"=>Input::get('code'),
                    "season"=>Input::get('season'),
                    "start_date"=>Input::get('start_date'),
                    "end_date"=>Input::get('end_date'),
                    "timetable" => Input::get('season')."_".Input::get('code')
                );
                foreach(Input::get('courses') as $key=>$value){
                    $input['courses'][$key] = $value;
                }
                $server = Admin::createTraining($input);
                $courses = Course::allCourses();
                $active_training = Training::active_training(TRAINING);
                $all_training = Training::all_training(TRAINING);
                $inactive_training = Training::inactive_training(TRAINING);
                $running_training = Training::runningTraining(TRAINING);
                return View::make('admin.training', array("server"=>$server, "courses"=>$courses, "active_training"=>$active_training, "all_training"=>$all_training, "running_training"=>$running_training, "inactive_training"=>$inactive_training));

            }else if(!$status){
                $courses = Course::allCourses();
                $active_training = Training::active_training(TRAINING);
                $all_training = Training::all_training(TRAINING);
                $running_training = Training::runningTraining(TRAINING);
                $inactive_training = Training::inactive_training(TRAINING);
                return View::make('admin.training', array("form_errors"=>$form_errors, "courses"=>$courses, "active_training"=>$active_training, "all_training"=>$all_training, "running_training"=>$running_training, "inactive_training"=>$inactive_training));
            }

        }
    }
    //create training
    public static  function  createConference(){
        $response['status'] = true;

        if(Session::token() !== Input::get('_token')){
            return View::make('illegal');
        }else{
            $status = true;
            $form_errors = array();

            foreach(Input::get() as $key=>$fields){
                if($key !== 'end_date'){
                    if(empty($fields)){
                        $form_errors[$key]= "Field is required";
                        $status = false;
                    }
                }

            }
            if($status){
                $input = array(
                    "name"=>Input::get('conference_name'),
                    "location"=>Input::get('location'),
                    "code"=>Input::get('code'),
                    "start_date"=>Input::get('start_date'),
                    "end_date"=>Input::get('end_date'),
                    "type"=>CONFERENCE
                );

                $confData = array("cost"=>Input::get('cost'), "time"=>Input::get('time'), 'date_created'=>date('Y-m-d'), 'date_modified'=>date('Y-m-d'));

                $server = Admin::createConference($input, $confData);
                $active_training = Training::active_training(CONFERENCE);
                $all_training = Training::all_training(CONFERENCE);
                $inactive_training = Training::inactive_training(CONFERENCE);
                $running_training = Training::runningTraining(CONFERENCE);
                return View::make('admin.conference', array("server"=>$server, "active_training"=>$active_training, "all_training"=>$all_training, "running_training"=>$running_training, "inactive_training"=>$inactive_training));

            }else if(!$status){
                $active_training = Training::active_training(CONFERENCE);
                $all_training = Training::all_training(CONFERENCE);
                $running_training = Training::runningTraining(CONFERENCE);
                $inactive_training = Training::inactive_training(CONFERENCE);
                return View::make('admin.conference', array("form_errors"=>$form_errors, "active_training"=>$active_training, "all_training"=>$all_training, "running_training"=>$running_training, "inactive_training"=>$inactive_training));
            }

        }
    }

    public static function userSlip($id){
        $result = Training::user_slip($id);
        if(!empty($result)){
            $data = array();
            $data['training_name'] = $result[0]->name;
            $data['applicant_name'] = ucwords($result[0]->surname." ".$result[0]->first_name);
            $data['reg_num'] = $result[0]->reg_num;
            $data['venue'] = $result[0]->location;
            $data['start_date'] = $result[0]->start_date;
            $data['end_date'] = $result[0]->end_date;
            $data['cost'] = $result[0]->app_cost;
            for($i = 0; $i < sizeof($result); $i++){
                $data['courses'][$i]['course_name'] = $result[$i]->course_name;
                $data['courses'][$i]['course_code'] = $result[$i]->course_code;
            }
            return  View::make('admin.slip', array('data'=>$data));
        }else{
            return  View::make('not_found');
        }
    }


    //create information
    public static function createInformation(){
        $info = Admin::getInformation();
        return View::make('admin.information', array("info"=>$info));
    }

    public static function postInformation(){
        $data['title'] = Input::get('title');
        $data['description'] = Input::get('message');
        $response = Admin::postInformation($data);

        $info = Admin::getInformation();

        return View::make('admin.information', array("response"=>$response, "info"=>$info));

    }

    public static function deleteInformation(){
        Admin::deleteInformation(Input::get('info_id'));
        return Redirect::route('new_info');
    }
}


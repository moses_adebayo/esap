<?php

class UserController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    |User Controller
    |--------------------------------------------------------------------------
    |
    |
    |
    */

    public function register()
    {
        $response = array();
        $response['status'] = true;

//         $response['status'] = false;
        //checking the inputs
        if(Session::token() !== Input::get('_token') ){
            $response['status'] = false;
            $response['error_code'] = -1 ;
        }else{
            //checking for empty fields
            if(!isset($_POST['gender'])){
                $response['status'] =  false;
                $response['msg'] = "Gender not selected";
            }else{

                foreach(Input::get() as $fields){
                    if(empty($fields)){
                        $response['status'] =  false;
                        $response['msg'] = "All fields are compulsory";
                    }
                }
                if($response['status']){
                    //validate inputs sent
                    $validate = User::validateRegForm(Input::get());
                    if($validate['status']){
                        $data = array();
                        $data['users'] = array('user_type'=>2, 'status'=>1);
                        $data['user_auth'] = array('email'=>Input::get('email'), 'password'=>Input::get('password'));
                        $data['identification'] = array('surname'=>Input::get('surname'), 'first_name'=>Input::get('first_name'),
                            'phone_num'=>Input::get('phone_num'), 'country'=>Input::get('country'),
                            'address'=>Input::get('address'), 'institution_type'=>Input::get('institution_type'),'institution_name'=>Input::get('institution_name'), 'level'=>Input::get('level'),
                            'course_study'=>Input::get('course_study'), 'lga'=>Input::get('lga'), 'state'=>Input::get('state'), 'gender'=>Input::get('gender'));
                        $data['user_balance'] = array("balance"=>10);
                        $result = User::registration($data);
                        if(is_numeric($result)){
                            $response['status'] = true;
                            $response['user_id'] = $result;
                            $url = Session::get('url');
                            $response['url'] = (sizeof($url) == 0 ? 'login' : $url['intended']) ;  //get the intended url--
                            $response['msg'] = "Your registration was successfull, You can now log in to gain access to eSAP training";
                        }else{
                            $response['status'] = false;
                            $response['msg'] = $result;
                        }
                    }else{
                        $response['status'] = false;
                        $response['msg'] = $validate['msg'];
                    }
                }
            }
        }
        return Response::json($response);
    }
    public function completeRegister()
    {
        $response = array();
        $response['status'] = true;

//
        foreach(Input::get() as $fields){
            if(empty($fields)){
                $response['status'] =  false;
                $response['msg'] = "All fields are compulsory";
            }
        }
        if($response['status']){
            //validate inputs sent

            $data = array();
            $data = array('phone_num'=>Input::get('phone_num'), 'country'=>Input::get('country'),
                'address'=>Input::get('address'), 'institution_type'=>Input::get('institution_type'),'institution_name'=>Input::get('institution_name'), 'level'=>Input::get('level'),
                'course_study'=>Input::get('course_study'), 'lga'=>Input::get('lga'), 'state'=>Input::get('state'));
            $result = User::complete_registration($data);
            if($result == 1){
                $response['status'] = true;
                $url =  Session::pull('keep_intended');
                $response['url'] = (sizeof($url) == 0 ? 'home' : $url['intended']) ;  //get the intended url--
                $response['msg'] = "Account setup is now complete";
            }else{
                $response['status'] = false;
                $response['msg'] = $result;
            }
        }

        return Response::json($response);
    }

    public function registerWithGoogle()
    {
        $response = array();
        $response['status'] = true;

        if(!User::verifyEmail(Input::get('email'))){
            $data = array();
            $data['users'] = array('user_type'=>2);
            $data['user_auth'] = array('email'=>Input::get('email'), 'password'=>'google');
            $data['identification'] = array('surname'=>Input::get('surname'), 'first_name'=>Input::get('first_name'), 'gender'=>Input::get('gender'));
            $data['user_balance'] = array("balance"=>0);
            $result = User::registration($data);
            if(is_numeric($result)){
                $response['status'] = true;
                $response['user_id'] = $result;
                $url = Session::get('url');
                $response['url'] = (sizeof($url) == 0 ? 'home' : $url['intended']) ;  //get the intended url--
                $response['msg'] = "Your registration was successfull, You are log in to gain access to eSAP training";
            }else{
                $response['status'] = false;
                $response['msg'] = $result;
            }
        }else{
            $response['status'] = false;
            $response['msg'] = 'Email Registered before';
        }

        return Response::json($response);
    }
    public function registerWithSocial()
    {
        $response = array();
        $response['status'] = true;

        if(!User::verifyEmail(Input::get('email'))){
            $data = array();
            $data['users'] = array('user_type'=>2);
            $data['user_auth'] = array('email'=>Input::get('email'), 'password'=>'google');
            $data['identification'] = array('surname'=>Input::get('surname'), 'first_name'=>Input::get('first_name'), 'gender'=>Input::get('gender'));
            $data['user_balance'] = array("balance"=>0);
            $result = User::registration($data);
            if(is_numeric($result)){
                Auth::loginUsingId($result);
                $response['status'] = true;
                $response['user_id'] = $result;
                $url = Session::get('url');
                $response['url'] = (sizeof($url) == 0 ? 'home' : $url['intended']) ;  //get the intended url--
                $response['msg'] = "Your registration was successfull, You are log in to gain access to eSAP training";
            }else{
                $response['status'] = false;
                $response['msg'] = $result;
            }
        }else{
            $response['status'] = false;
            $response['msg'] = 'Email Registered before, you can now login with this email';
        }

        return Response::json($response);
    }

    public function login(){
        $log_data = array('email'=>Input::get('email'), 'password'=>Input::get('password'));
        if(Auth::attempt($log_data)){
            $data['status'] = true;
            //check type of user
            $user = DB::table('user_auth')->select('user_type')->where('id', '=', Auth::id())->first();
            if($user->user_type == 1){
                return Redirect::to('admin');
            }else{
                return Redirect::intended('/');
            }
        }else{
            return View::make('login')->withInput(Input::except('password'));
        }
    }
    public function loginWGoogle(){
        $log_data = array('email'=>Input::get('email'), 'password'=>'google');
        if(Auth::attempt($log_data)){
            $response['new'] = false;

//            //check type of user
            $user = DB::table('user_auth')->select('user_type')->where('id', '=', Auth::id())->first();
////            $user = User::userInfo(Auth::id());
            if($user->user_type == 1){
//                    return Redirect::to('admin');
                return array('status'=>true, 'msg'=>'Admin', 'id'=>Auth::id());
            }else{
//                    return Redirect::intended('/');
                return array('status'=>true, 'msg'=>'user', 'url'=>Redirect::intended()->getTargetUrl());
            }


        }else{
            //register the user
            $response['new'] = true;

            $data['users'] = array('user_type'=>2);
            $data['user_auth'] = array('email'=>Input::get('email'), 'password'=>'google');
            $data['identification'] = array('surname'=>Input::get('surname'), 'first_name'=>Input::get('first_name'), 'gender'=>Input::get('gender'));
            $data['user_balance'] = array("balance"=>0);
            $result = User::registration($data);
            if(is_numeric($result)){
                $response['status'] = true;
                $response['user_id'] = $result;
                $url = Session::get('url');
                Session::put('keep_intended', $url);

                $response['url'] = (sizeof($url) == 0 ? 'login' : $url['intended']) ;  //get the intended url--
                $response['c_reg_url'] = URL::to('complete_reg');
                $response['msg'] = "Your registration was successfull, You are log in to gain access to eSAP training";
            }else{
                $response['status'] = false;
                $response['msg'] = $result;
            }
//            return array('status'=>false, 'msg'=>'Email address is not registered');
            return Response::json($response);
        }
    }
    public function loginWSocial(){
        $log_data = array('email'=>Input::get('email'), 'password'=>'social');
        if(Auth::attempt($log_data)){
            $response['new'] = false;
//            //check type of user
            $user = DB::table('user_auth')->select('user_type')->where('id', '=', Auth::id())->first();
////            $user = User::userInfo(Auth::id());
            if($user->user_type == 1){
//                    return Redirect::to('admin');
                return array('status'=>true, 'msg'=>'Admin', 'id'=>Auth::id());
            }else{
//                    return Redirect::intended('/');
                return array('status'=>true, 'msg'=>'user', 'url'=>Redirect::intended()->getTargetUrl());
            }
        }else{
            //register the user
            $response['new'] = true;

            $data['users'] = array('user_type'=>2);
            $data['user_auth'] = array('email'=>Input::get('email'), 'password'=>'social');
            $data['identification'] = array('surname'=>Input::get('surname'), 'first_name'=>Input::get('first_name'), 'gender'=>Input::get('gender'));
            $data['user_balance'] = array("balance"=>0);
            $result = User::registration($data);
            if(is_numeric($result)){
                Auth::loginUsingId($result);
                $response['status'] = true;
                $response['user_id'] = $result;
                $url = Session::get('url');
                Session::put('keep_intended', $url);

                $response['url'] = (sizeof($url) == 0 ? 'home' : $url['intended']) ;  //get the intended url--
                $response['c_reg_url'] = URL::to('complete_reg');
                $response['msg'] = "Your registration was successfull, You are log in to gain access to eSAP training";
            }else{
                $response['status'] = false;
                $response['msg'] = $result;
            }
//            return array('status'=>false, 'msg'=>'Email address is not registered');
            return Response::json($response);
        }
    }

    public function getAllUsers(){
        $data = User::getAllUsers();
        return View::make('admin.report.all_students')->with('users', $data);
    }
    public function homepage(){
//        $data = Information::latest_info();
        return View::make('index');
    }

    public function logout(){
        Auth::logout();
        return Redirect::to('/');
    }
}


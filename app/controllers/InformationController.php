<?php

class InformationController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    |Training Controller
    |--------------------------------------------------------------------------
    |
    |
    |
    */

    //retrieve all the information
    public function records()
    {
        return View::make('announcements', array('data'=>Information::records()));
    }

    public function side_info(){
        return Information::side_info();
    }
}
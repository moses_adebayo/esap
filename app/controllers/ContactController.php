<?php
/**
 * Created by PhpStorm.
 * User: olajuwon
 * Date: 12/27/14
 * Time: 9:39 AM
 */

class ContactController extends BaseController{

    public static function sendMail(){
        $response['status'] = true;
        $form_errors = array();
        foreach(Input::get() as $key=>$fields){
            if(empty($fields)){
                $response['status'] = false;
                $form_errors[$key]= "Field is required";
            }
        }
        if(!User::isValidEmail(Input::get('email'))){
            $response['status'] = false;
            $response['msg'] = "Invalid email";
        }
        if($response['status']){
            if(Contact::sendEmail(Input::get('name'), Input::get('email'), Input::get('subject'), Input::get('message'))){
            }else{
                $response['status'] = false;
                $response['msg'] = "Unable to send mail";
            }
                return View::make('contact', array("response"=>$response));
        }else if(!$response['status']){
            return View::make('contact', array("response"=>$response, "form_errors"=>$form_errors))->withInput(Input::except('_token'));
        }
    }
    public static function sendMailForm2(){
        $active_page = 'invite_esap';
        if(Input::get('subject') === "e-SAP Volunteer"){
            $active_page = 'esap_volunteer';
        }

        $response['status'] = true;
        $form_errors = array();
        foreach(Input::get() as $key=>$fields){
            if(empty($fields)){
                $response['status'] = false;
                $form_errors[$key]= "Field is required";
            }
        }
        if(!User::isValidEmail(Input::get('email'))){
            $response['status'] = false;
            $response['msg'] = "Invalid email";
        }
        if(!is_numeric(Input::get('number'))){
            $response['status'] = false;
            $response['msg'] = "Invalid phone number entered";
        }
        if($response['status']){
            $message= "Phone number:".Input::get('number').",";
            $message .= "Institution".Input::get('institution').",";
            $message .= "Other Info".Input::get('message');

            if(Contact::sendEmail(Input::get('name'), Input::get('email'), Input::get('subject'), $message)){
            }else{
                $response['status'] = false;
                $response['msg'] = "Unable to send mail";
            }
                return View::make($active_page, array("response"=>$response));
        }else if(!$response['status']){
            return View::make($active_page, array("response"=>$response, "form_errors"=>$form_errors))->withInput(Input::except('_token'));
        }
    }
    public static function sendMailForm3(){
        $active_page = 'esap_volunteer';
        if(Input::get('subject') == 'e-SAP Volunteer'){
            $active_page = 'esap_volunteer';
        }

        $response['status'] = true;
        $form_errors = array();
        foreach(Input::get() as $key=>$fields){
            if(empty($fields)){
                $response['status'] = false;
                $form_errors[$key]= "Field is required";
            }
        }
        if(!User::isValidEmail(Input::get('email'))){
            $response['status'] = false;
            $response['msg'] = "Invalid email";
        }
        if(!is_numeric(Input::get('number'))){
            $response['status'] = false;
            $response['msg'] = "Invalid phone number entered";
        }
        if($response['status']){
            $message= "Phone number:".Input::get('number').",";
            $message .= "Institution".Input::get('institution').",";
            $message .= "Other Info".Input::get('message');

            if(Contact::sendEmail(Input::get('name'), Input::get('email'), Input::get('subject'), $message)){
            }else{
                $response['status'] = false;
                $response['msg'] = "Unable to send mail";
            }
                return View::make($active_page, array("response"=>$response));
        }else if(!$response['status']){
            return View::make($active_page, array("response"=>$response, "form_errors"=>$form_errors))->withInput(Input::except('_token'));
        }
    }


}
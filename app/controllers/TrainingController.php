<?php

class TrainingController extends BaseController {
    /*
    |--------------------------------------------------------------------------
    |Training Controller
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    //retrieve all the training
    public function open()
    {
        $training = new Training();
        $training_active = $training->active_training(TRAINING);
        return View::make('training', array('training'=>$training_active));
    }
//    OPEN THE SELECTED APPLICATION
    public function open_selected($code = false)
    {
        if (!$code) {
//            training code not specified
            return Redirect::route('training');
        } else {
            $data = Training::selected_training($code);
            $length = sizeof($data);
            if ($length == 0) {
//                INVALID CODE SENT
                return Redirect::route('training');
            } else {
                //Verify the status of the training
                if ($data[0]->status == Constants::ACTIVE_TRAINING) {
                    $output['training_id'] = $data[0]->training_id;
                    $output['name'] = $data[0]->name;
                    $output['start'] = $data[0]->start_date;
                    $output['end'] = $data[0]->end_date;
                    $output['name'] = $data[0]->name;
                    $output['location'] = $data[0]->location;
                    $output['code'] = $data[0]->code;
                    $output['courses'] = array();
                    for ($i = 0; $i < $length; $i++) {
                        $output['courses'][$i]['course_name'] = $data[$i]->course_name;
                        $output['courses'][$i]['course_id'] = $data[$i]->course_id;
                        $output['courses'][$i]['course_desc'] = $data[$i]->course_desc;
                        $output['courses'][$i]['reg_cost'] = $data[$i]->reg_cost;
                    }
                    $record = Training::get_user_records(Auth::id(), $data[0]->training_id);
                    if (sizeof($record) == 0) {
                        $user['new'] = true;
                    } else {
                        $user['new'] = false;
                        $user['application_id'] = $record[0]->application_id;
                        $user['record'] = $record;
                        $user['reg_num'] = $record[0]->reg_num;
                    }
                    return View::make('selected_training', array('training' => $output, 'user' => $user));
                } else if ($data[0]->status == Constants::RUNNING_TRAINING) {
                    $output['training_id'] = $data[0]->training_id;
                    $output['name'] = $data[0]->name;
                    $output['start'] = $data[0]->start_date;
                    $output['end'] = $data[0]->end_date;
                    $output['name'] = $data[0]->name;
                    $output['location'] = $data[0]->location;
                    $output['code'] = $data[0]->code;

                    $record = Training::get_user_records(Auth::id(), $data[0]->training_id);
                    if (sizeof($record) == 0) {
                        //user did not apply for this training
                        $user['new'] = true;
                    } else {
                        $user['new'] = false;
                        $user['application_id'] = $record[0]->application_id;
                        $user['record'] = $record;
                        $user['reg_num'] = $record[0]->reg_num;
                        $output['courses'] = array();
                        for ($i = 0; $i < $length; $i++) {
                            $output['courses'][$i]['course_name'] = $data[$i]->course_name;
                            $output['courses'][$i]['course_id'] = $data[$i]->course_id;
                            $output['courses'][$i]['course_desc'] = $data[$i]->course_desc;
                            $output['courses'][$i]['reg_cost'] = $data[$i]->reg_cost;
                        }
                    }
                    return View::make('running_training', array('training' => $output, 'user' => $user));
                }
                return NULL;
            }
        }
    }

//    PROCESS THE APPLICATION FOR A TRAINING
    public function  apply(){
        //verify balance of the user
        $user_balance = Account::get_balance(Auth::id());
        $data = array();
        $data['balance'] = $user_balance->balance;
        if($user_balance->balance < Input::get('cost')){
            $response['status'] = false;
            $response['msg'] = "Insufficient Balance <strong>&nbsp;<a href='".URL::to('account')."'>Recharge</a></strong>";
        }else{
            $data['application'] = array();
            $data['application_courses'] = array();

            //Get user id from auth
            $data['application']['user_id'] = Auth::id();
            $data['application']['training_id'] = Input::get('training_id');
            $data['application']['app_cost'] = Input::get('cost');
            $data['courses'] = Input::get('courses');

            $result = Training::apply($data);;
            if(is_numeric($result)){
                $response['status'] = true;
                $response['application_id'] = $result;
                $response['msg'] = "Your application was successful";
            }else{
                $response['status'] = false;
                $response['msg'] = $result;
            }
        }
        return Response::json($response);
    }

    //print slip
    public function print_slip($id){
        $result = Training::user_slip($id);
        if(!empty($result)){
            $data = array();
            $data['training_name'] = $result[0]->name;
            $data['applicant_name'] = ucwords($result[0]->surname." ".$result[0]->first_name);
            $data['reg_num'] = $result[0]->reg_num;
            $data['venue'] = $result[0]->location;
            $data['start_date'] = $result[0]->start_date;
            $data['end_date'] = $result[0]->end_date;
            $data['cost'] = 0;
            for($i = 0; $i < sizeof($result); $i++){
                $data['courses'][$i]['course_name'] = $result[$i]->course_name;
                $data['courses'][$i]['course_code'] = $result[$i]->course_code;
                $data['courses'][$i]['course_cost'] = $result[$i]->reg_cost;
                $data['cost'] += $result[$i]->reg_cost;
            }
            return  View::make('slip', array('data'=>$data));
        }else{
            return  View::make('not_found');
        }
    }

    public static function getList($id){
        $courses = Training::getTrainingCourse($id);
        if(sizeof($courses) == 0){
            return View::make('illegal');
        }else{
            $students = Training::getStudentList($id, $courses[0]->course_id);
            $data['empty'] = false;
            if(sizeof($students) == 0){
                $data['empty'] = true;
            }else{
                $data['course_name'] = $students[0]->course_name;
                $data['total'] = 0;
                $pointer = 1;
                foreach($students as $s ){
                    $data['list'][$pointer]['name'] = ucwords($s->surname.' '.$s->first_name);
                    $data['list'][$pointer]['reg'] = $s->reg_num;
                    $data['list'][$pointer]['application_id'] = $s->id;
                    $data['total'] +=1;
                    $pointer +=1;
                }
            }
            return View::make('admin.training_list', array('t_id'=>$id, 'courses'=>$courses, 'students'=>$data));
        }
    }

    public static function getCourseList($id, $course_id){
        $courses = Training::getTrainingCourse($id);
        if(sizeof($courses) == 0){
            return View::make('illegal');
        }else{
            $students = Training::getStudentList($id, $course_id);
            $data['empty'] = false;
            if(sizeof($students) == 0){
                $data['empty'] = true;
            }else{
                $data['course_name'] = $students[0]->course_name;
                $data['total'] = 0;
                $pointer = 1;
                foreach($students as $s ){
                    $data['list'][$pointer]['name'] = ucwords($s->surname.' '.$s->first_name);
                    $data['list'][$pointer]['reg'] = $s->reg_num;
                    $data['list'][$pointer]['application_id'] = $s->id;
                    $data['total'] +=1;
                    $pointer +=1;
                }
            }
            return View::make('admin.training_list', array('t_id'=>$id, 'courses'=>$courses, 'students'=>$data));
        }
    }


//    open page to edit application
    public static function updateApplication($app_id){
        $data = Training::updateApplication($app_id);

        if(sizeof($data) == 0){
            return View::make('illegal');
        }else{
            $courses = Training::getTrainingCourse($data[0]->training_id);
            return View::make('edit_selected_training', array('data'=>$data, 'courses'=>$courses));

        }

    }

//    process edit application command

    public static function editApplication(){
        $response = array();
        $response['status'] = true;

        //Verify balance of the money with the returned money
        $user_balance = Account::get_balance(Auth::id());
        $temp_balance = $user_balance->balance + intval(Input::get('prev_cost'));
//        return Response::json(array("prev_cost"=>Input::get('prev_cost'), "app_cost"=>Input::get('cost')));
        if($temp_balance < Input::get('cost')){
            $response['status'] = false;
            $response['msg'] = "Insufficient Balance <strong>&nbsp;<a href='".URL::to('account')."'>Recharge</a></strong>";
        }else{
            $data['application_id'] = Input::get('application_id');
            $data['prev_cost'] = intval(Input::get('prev_cost'));
            $data['app_cost'] = intval(Input::get('cost'));
            $data['courses'] = Input::get('courses');
            if(!Training::editApplication($data)){
                $response['status'] = false;
                $response['msg'] = "Server down";
            }else{
                $response['msg'] ="Your Application has being updated <strong><br/><a href='".URL::to('apply/'.Input::get('training_code'))."'>Print Slip</a></strong>";
            }
        }
        return Response::json($response);
    }

    public static function getAllUsers($t_id){
        $data = Training::getAllUsers($t_id);
        $output = array();
//
        if(!empty($data)){
            $output['training_name'] = $data[0]->name;
            $output['training_id'] = $data[0]->training_id;
            $output['courses'] = array();
            $output['students'] = array();
            foreach($data as $d){
                if(!array_key_exists($d->course_id, $output['courses'])) {
                    $output['courses'][$d->course_id] = array('id'=>$d->course_id, 'name'=>$d->course_name);
                }
                array_push($output['students'], array('name'=>ucwords($d->surname.' '.$d->first_name), 'reg_num'=>$d->reg_num, 'phone_num'=>$d->phone_num, 'course_name'=>$d->course_name));
            }
        }
        return View::make('admin.training_users', array("data"=>$output));
    }

    public static function getAllCourse($t_id){

        $data = Training::getTrainingCourse($t_id);

        return View::make('admin.training_users', array('data'=>$data));
    }

    //students for each course
    public static  function getTrainingStudents($t_id, $c_id){
        $info = array();
        $info['empty'] = false;
        if($c_id == -1){
            //get all student
            $data = Training::getAllStudents($t_id);
            $info['students'] = array();
            if(!empty($data)){
                foreach($data as $d){
                    array_push($info['students'], array('name'=>ucwords($d->surname.' '.$d->first_name), 'reg_num'=>$d->reg_num, 'phone_num'=>$d->phone_num, 'course_name'=>$d->course_name));
                }
            }else{
                $info['empty'] = true;
            }
        }else{
            $info['students'] = array();
            $data = Training::getStudentList($t_id, $c_id);
           if(!empty($data)){
               foreach($data as $d){
                   array_push($info['students'], array('name'=>ucwords($d->surname.' '.$d->first_name), 'reg_num'=>$d->reg_num, 'phone_num'=>$d->phone_num, 'course_name'=>$d->course_name));
               }
           }else{
               $info['empty'] = true;
           }
        }
        return View::make('admin.report.courses_students', array('data'=>$info));
    }

    /*Temporary page for active training*/
    public function activeRunningTrainingPage(){
        return View::make('training_page.oaus4');

    }
}


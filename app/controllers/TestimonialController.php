<?php

class TestimonialController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    |Account Controller
    |--------------------------------------------------------------------------
    |
    |
    |
    */

    public function get(){
        $data = Testimonial::get_testimony();
        return Response::json($data);
    }

    public function get_all(){
        $data = Testimonial::get_all();
        return View::make('credits',array('data'=>$data));
    }
}


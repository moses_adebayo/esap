<?php

class ConferenceController extends BaseController {
    /*
    |--------------------------------------------------------------------------
    |Training Controller
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    //retrieve all the training
    public function open()
    {
        $training = new Training();
        $training_active = $training->active_training(CONFERENCE);
        return View::make('conference', array('training'=>$training_active));
    }
//    OPEN THE SELECTED APPLICATION
    public function open_selected($code = false){
        if(!$code){
//            training code not specified
            return Redirect::route('conference');
        }else{
            $data = Training::selected_conference($code);
            if(is_null($data)){
//                INVALID CODE SENT
                return Redirect::route('conference');
            }else{
                $record = Training::get_user_conf_records(Auth::id(), $data->training_id);
                if(sizeof($record) == 0){
                    $user['new'] = true;
                }else{

                    $user['new'] = false;
                    $user['application_id'] = $record->id;
                    $user['reg_num'] = $record->reg_num;
                }
                return View::make('running_conference', array('record'=>$record, 'conference'=>$data, 'user'=>$user));
                }
            }
    }

//    PROCESS THE APPLICATION FOR A TRAINING
    public function  apply(){
        //verify balance of the user
        $user_balance = Account::get_balance(Auth::id());
        $conference = Training::getConferenceDetails(Input::get('conference_id'));
        $data = array();
        $data['balance'] = $user_balance->balance;
        if($user_balance->balance < $conference->cost){
            $response['status'] = false;
            $response['msg'] = "Insufficient Balance <strong>&nbsp;<a href='".URL::to('account')."'>Recharge</a></strong>";
        }else{

            $result = Training::applyConference(array('training_id'=>Input::get('conference_id'), 'user_id'=>Auth::id(), 'app_cost'=>$conference->cost), $conference->code);
            if(is_numeric($result)){
                $response['status'] = true;
                $response['application_id'] = $result;
                $response['msg'] = "Your application was successful";
            }else{
                $response['status'] = false;
                $response['msg'] = $result;
            }
        }
        return Response::json($response);
    }


    //print slip
    public function print_slip($id){
        $result = Training::user_slip2($id);
        if(sizeof($result) !== 0){
            $data = array();
            $data['training_name'] = $result->name;
            $data['applicant_name'] = ucwords($result->surname." ".$result->first_name);
            $data['reg_num'] = $result->reg_num;
            $data['venue'] = $result->location;
            $data['start_date'] = $result->start_date;
            $data['end_date'] = $result->end_date;
            $data['cost'] = $result->app_cost;

            return  View::make('slip2', array('data'=>$data));
        }else{
            return  View::make('not_found');
        }
    }

    public static function getList($id){
        $details = Training::getConferenceDetails($id);
        if(is_null($details)){
            return View::make('illegal');
        }else{
            $students = Training::getStudentList($id,2);

            return View::make('admin.conference_list', array('details'=>$details, 'students'=>$students));
        }

    }

    public static function getCourseList($id, $course_id){
        $courses = Training::getTrainingCourse($id);
        if(sizeof($courses) == 0){
            return View::make('illegal');
        }else{
            $students = Training::getStudentList($id, $course_id);
            $data['empty'] = false;
            if(sizeof($students) == 0){
                $data['empty'] = true;
            }else{
                $data['course_name'] = $students[0]->course_name;
                $data['total'] = 0;
                $pointer = 1;
                foreach($students as $s ){
                    $data['list'][$pointer]['name'] = ucwords($s->surname.' '.$s->first_name);
                    $data['list'][$pointer]['reg'] = $s->reg_num;
                    $data['list'][$pointer]['application_id'] = $s->id;
                    $data['total'] +=1;
                    $pointer +=1;
                }
            }
            return View::make('admin.training_list', array('t_id'=>$id, 'courses'=>$courses, 'students'=>$data));
        }
    }


//    open page to edit application
    public static function updateApplication($app_id){
        $data = Training::updateApplication($app_id);

        if(sizeof($data) == 0){
            return View::make('illegal');
        }else{
            $courses = Training::getTrainingCourse($data[0]->training_id);
            return View::make('edit_selected_training', array('data'=>$data, 'courses'=>$courses));

        }

    }

//    process edit application command

    public static function editApplication(){
        $response = array();
        $response['status'] = true;

        //Verify balance of the money with the returned money
        $user_balance = Account::get_balance(Auth::id());
        $temp_balance = $user_balance->balance + intval(Input::get('prev_cost'));

        if($temp_balance < Input::get('cost')){
            $response['status'] = false;
            $response['msg'] = 'Insufficient Balance';
        }else{
            $data['application_id'] = Input::get('application_id');
            $data['prev_cost'] = Input::get('prev_cost');
            $data['app_cost'] = Input::get('cost');
            $data['courses'] = Input::get('courses');
            $response['status'] = Training::editApplication($data);
            if(!Training::editApplication($data)){
                $response['status'] = false;
                $response['msg'] = "Server down";
            }else{
                $response['msg'] ="Your Application has being updated <strong><br/><a href='".URL::to('apply/'.Input::get('training_code'))."'>Print Slip</a></strong>";
            }
        }
        return Response::json($response);
    }

    public static function getAllUsers($t_id){
        $data = Training::getAllUsers($t_id);
        $output = array();

        if(!empty($data)){
            $output['training_name'] = $data[0]->name;
            $output['courses'] = array();
            foreach($data as $d){
                if(array_key_exists($d->course_id, $output['courses'])){
                    $output['courses'][$d->course_id]['students'][$d->reg_num] = $d->surname.' '.$d->first_name;
                }else{
                    $output['courses'][$d->course_id] = array();
                    $output['courses'][$d->course_id]['course_name'] = $d->course_name;
                    $output['courses'][$d->course_id]['students'] = array();
                    $output['courses'][$d->course_id]['students'][$d->reg_num] = $d->surname.' '.$d->first_name;
                }
            }
        }
        return View::make('admin.training_users', array("data"=>$output));
    }


    //show confernce info/ advert
    public static function showConference(){
        return View::make('conferences.conf001');
    }
}


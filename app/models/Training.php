<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Training extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'training';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');


    //apply for training
    public static  function active_training($type){
        try{
            $data = DB::table('training')->where('status', '=', Constants::ACTIVE_TRAINING)->where('type', $type)->orderBy('training_id', 'desc')->get();
            return $data;
        }catch (Exception $ex){
            Log::error('Error in retrieving training');
            return $ex->getMessage();
        }
    }

    //all training
    public static  function all_training($type){
        try{
            $data = DB::table('training')->where('type', $type)->orderBy('training_id', 'desc')->paginate(10);
            return $data;
        }catch (Exception $ex){
            Log::error('Error in retrieving training');
            return $ex->getMessage();
        }
    }
    //pending training
    public static  function inactive_training($type){
        try{
            $data = DB::table('training')->where('status', Constants::PENDING_TRAINING)->where('type', $type)->orderBy('training_id', 'desc')->get();
            return $data;
        }catch (Exception $ex){
            Log::error('Error in retrieving training');
            return $ex->getMessage();
        }
    }

    //running training
    public static  function runningTraining($type){
        try{
            $data = DB::table('training')->where('status', Constants::RUNNING_TRAINING)->where('type', $type)->orderBy('training_id', 'desc')->get();
            return $data;
        }catch (Exception $ex){
            Log::error('Error in retrieving training');
            return $ex->getMessage();
        }
    }


    public static function selected_training($code){
        try{
            $data = DB::table('training')->where('training.code', '=', $code)->where('training.type', TRAINING)->join('training_courses', 'training_courses.training_id', '=', 'training.training_id')->join('courses', 'training_courses.course_id', '=', 'courses.course_id')->get();
            return $data;
        }catch (Exception $ex){
            Log::error('Error in retrieving training');
            return $ex->getMessage();
        }
    }

 public static function selected_conference($code){

        try{
            $data = DB::table('training')->where('training.code', '=', $code)->join('conferences', 'conferences.conf_id', '=', 'training.training_id')->where('training.type',CONFERENCE)->first();
            return $data;
        }catch (Exception $ex){
            Log::error('Error in retrieving training');
            return $ex->getMessage();
        }
    }


    public static function get_user_records($user_id, $training_id){
        $data = DB::table('application')->where('application.training_id', '=', $training_id)->where('application.user_id', $user_id)->join('application_courses', 'application.id', '=', 'application_courses.application_id')->join('courses', 'courses.course_id', '=','application_courses.course_id')->get();
        return $data;

    }
    public static function get_user_conf_records($user_id, $training_id){
        $data = DB::table('application')->where('application.training_id', '=', $training_id)->where('application.user_id', $user_id)->first();
        return $data;

    }
    public static function getAllUserRecords($user_id){
        $data = DB::table('application')->where('application.user_id', $user_id)->join('training', 'training.training_id', '=', 'application.training_id')->get();
        return $data;

    }
    public static function getAllUserActiveApplications($user_id){
        $data = DB::table('application')->where('application.user_id', $user_id)->where('training.status', Constants::ACTIVE_TRAINING)->join('training', 'training.training_id', '=', 'application.training_id')->get();
        return $data;

    }
    public static function getAllUserPastApplications($user_id){
            $data = DB::table('application')->where('application.user_id', $user_id)->where('training.status', Constants::TERMINATED_TRAINING)->join('training', 'training.training_id', '=', 'application.training_id')->get();
            return $data;

        }

    //for training
    public static  function apply ($data){
        try{
            DB::beginTransaction();
            $application_id = DB::table('application')->insertGetId($data['application']);
            $data['application_courses']['application_id'] = $application_id;
            $data['application_courses']['user_id'] = $data['application']['user_id'];
            foreach($data['courses'] as $course_id=>$course){
                $data['application_courses']['course_id'] = $course_id;
                DB::table('application_courses')->insert($data['application_courses']);
            }
            //update user balance
            Account::withdraw(Auth::id(), $data['application']['app_cost']);
            //generate USER-REG
            $training_data = DB::table('training')->where('training_id', $data['application']['training_id'])->first();
            $user_reg = "eSAP/".strtoupper($training_data->code."/".$training_data->season."/".$application_id);
            DB::table('application')->where('id', $application_id)->update(array('reg_num'=>$user_reg));
            DB::commit();
            return $application_id;
        }catch(Exception $ex){
            Log::error('error in application');
            DB::rollback();
            return $ex->getMessage();
        }
    }
   public static  function applyConference ($data, $code){
        try{
            DB::beginTransaction();

             $application_id = DB::table('application')->insertGetId($data);

            //update user balance
            Account::withdraw(Auth::id(), $data['app_cost']);
            //generate USER-REG
            $user_reg = "eSAP/".strtoupper($code."/".str_limit(User::userInfo(Auth::id())->first_name,3,'')."/".$application_id);
            DB::table('application')->where('id', $application_id)->update(array('reg_num'=>$user_reg));
            DB::commit();
            return $application_id;
        }catch(Exception $ex){
            Log::error('error in application');
            DB::rollback();
            return $ex->getMessage();
        }
    }

//        open page to update application
    public static function updateApplication($app_id){

        $data = DB::table('application')->where('application.id', $app_id)->join('application_courses', 'application_courses.application_id', '=', 'application.id')->join('courses', 'courses.course_id', '=', 'application_courses.course_id')->get();

        //delete user application


        //delete all record of the course user reg for

        //return user money to account

        //redirect back to applicaltion page
        return $data;
    }

    public static function editApplication($data){
        try{
            DB::beginTransaction();
            //delete former courses
            DB::table('application_courses')->where('application_id', '=', $data['application_id'])->delete();
            //return back the money
            Account::deposit(Auth::id(), $data['prev_cost']);

            //apply afresh
            $data['application_courses']['application_id'] = $data['application_id'];
            $data['application_courses']['user_id'] = Auth::id();
            foreach($data['courses'] as $course_id=>$course){
                $data['application_courses']['course_id'] = $course_id;
                DB::table('application_courses')->insert($data['application_courses']);
            }
//            update app_cost
            DB::table('application')->where('id', $data['application_id'])->update(array('app_cost'=>$data['app_cost']));

            //update user balance
            Account::withdraw(Auth::id(), $data['app_cost']);
            DB::commit();
            return true;
        }catch(Exception $ex){
//            Log::error($ex->getMessage());
            DB::rollback();
            return false;
        }
    }

    public static function user_slip($application_id){
        try{
            $data = DB::table('application')->where('application.id', $application_id)->join('application_courses', 'application_courses.application_id', '=', 'application.id')->join('training', 'application.training_id', '=', 'training.training_id')->join('courses', 'courses.course_id', '=', 'application_courses.course_id')->join('identification', 'application.user_id', '=', 'identification.user_id')->get();
            return $data;
        }catch(Exception $ex){
            return $ex->getMessage();
        }
    }

//    for conference
    public static function user_slip2($application_id){
        try{
            $data = DB::table('application')->where('application.id', $application_id)->join('training', 'application.training_id', '=', 'training.training_id')->join('identification', 'application.user_id', '=', 'identification.user_id')->first();
            return $data;
        }catch(Exception $ex){
            return $ex->getMessage();
        }
    }

    //get training courses
    public static function getTrainingCourse($id){
//        $data = DB::table('training')->where('training.training_id', $id)->join('application', 'application.training_id', '=', 'training.training_id')->join('application_courses', 'application.id', '=', 'application_courses.application_id')->join('courses', 'courses.course_id', '=','application_courses.course_id')->join('identification', 'identification.user_id', '=', 'application_courses.user_id')->get();
        $data = DB::table('training')->where('training.training_id', $id)->join('training_courses', 'training_courses.training_id', '=', 'training.training_id')->join('courses', 'courses.course_id', '=', 'training_courses.course_id')->get();
        return $data;
    }
     public static function getConferenceDetails($id){
            $data = DB::table('training')->join('conferences','training.training_id','=', 'conferences.conf_id')->where('training.training_id', $id)->first();
            return $data;
        }

    //get student for a courses
    public static function getStudentList($t_id, $s_id){
        try{
            $data = DB::table('application')->where('application.training_id', $t_id)->join('application_courses', 'application_courses.application_id', '=', 'application.id')->join('identification', 'identification.user_id', '=', 'application.user_id')->join('courses', 'courses.course_id', '=', 'application_courses.course_id')->where('application_courses.course_id',$s_id)->get();
            return $data;
        }catch (Exception $ex){
            return $ex->getMessage();
        }
    }

    public static function getAllUsers($id){
        $data = DB::table('application')->where('application.training_id', $id)->join('training', 'training.training_id','=', 'application.training_id')->join('application_courses', 'application_courses.application_id', '=', 'application.id')->join('identification', 'identification.user_id', '=', 'application.user_id')->join('courses', 'courses.course_id', '=', 'application_courses.course_id')->get();
        return $data;
    }

    public static  function getAllStudents($t_id){
        $data = DB::table('application')->where('application.training_id', $t_id)->join('application_courses', 'application_courses.application_id', '=', 'application.id')->join('identification', 'identification.user_id', '=', 'application.user_id')->join('courses', 'courses.course_id', '=', 'application_courses.course_id')->get();
        return $data;
    }


}

<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Information extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'training';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');


    public static  function latest_info(){
        try{
            $data = DB::table('information')->orderBy('id', 'DESC')->first();
            return $data;
        }catch (Exception $ex){
            Log::error('Error in retrieving training');
            return $ex->getMessage();
        }
    }
    public static  function side_info(){
        try{
            $data = DB::table('information')->orderBy(DB::raw('RAND()'))->take(4)->get();
            return $data;
        }catch (Exception $ex){
            Log::error('Error in retrieving training');
            return $ex->getMessage();
        }
    }


    public static function records(){
        try{
            $data = DB::table('information')->orderBy('id', 'DESC')->paginate(5);
            return $data;
        }catch (Exception $ex){
            Log::error('Error in retrieving training');
            return $ex->getMessage();
        }
    }

}

<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Course {

    public static function allCourses(){
        $course = DB::table('courses')->select('course_id', 'course_name')->get();
        return $course;
    }
}

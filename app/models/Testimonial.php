<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Testimonial extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'training';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');


    //apply for training
    public static  function get_testimony(){
        $data['status'] = true;
        try{
            $info = DB::table('testimonials')->where('status', 1)->orderBy(DB::raw('RAND()'))->first();
            $data['info'] = $info;
        }catch (Exception $ex){
            Log::error('Error in retrieving training');
            $data['status'] = false;
            $data['info'] = $ex->getMessage();
        }

        return $data;
    }

    public static function get_all(){
        $data['status'] = true;
        try{
            $info = DB::table('testimonials')->orderBy('id', 'DESC')->paginate(6);
            $data['info'] = $info;
        }catch (Exception $ex){
            Log::error('Error in retrieving training');
            $data['status'] = false;
            $data['info'] = $ex->getMessage();
        }

        return $data;
    }




}

<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Admin{


    public static  function generateVoucher($total = 100, $price=2500){
        $data = array();
        for($i = 0; $i < $total; $i++){
            $data['code'] = str_random(7);
            $data['price'] = $price;
            DB::table('voucher')->insert($data);
        }
        return true;
    }

    public static  function adminTest(){
        $data = User::userInfo(Auth::id());
        if($data->user_type == 1){
            return View::make('admin.index', array("user"=>$data));

        }else{
            return Redirect::to('/');
        }
    }

    public static function unusedVoucher(){
        $data = array();
        $data['unused'] = DB::table('voucher')->where('status', 0)->count();
        $data['unused_value'] = DB::table('voucher')->where('status', 0)->sum('price');
        return $data;
    }

    public static function loadVoucher($data){
        $response['status'] = true;
        DB::beginTransaction();
        try{
            $check = DB::table('voucher')->where(DB::raw('BINARY code'), $data)->first();
            if($check == null){
                $response['status'] = false;
                $response['msg'] = "Invalid card code";
            }else if($check->status == CARD_USED){
                $response['status'] = false;
                if($check->used_by == Auth::id()){
                    $response['msg'] = "Card have being used by you";
                }else{
                    $response['msg'] = "Card have being used before";
                }
            }else if($check->status == CARD_NOT_USED){
                $dt = new DateTime();
                $value['used_by'] = Auth::id();
                $value['status'] = CARD_USED;
                $value['date_used'] = $dt->format('Y-m-d H:i:s');
                DB::table('voucher')->where('id', $check->id)->update($value);
                //credit account
                DB::table('user_balance')->where('user_id', Auth::id())->increment('balance', $check->price, array('date_modified'=>$dt->format('Y-m-d H:i:s')));
                $response['status'] = true;

                DB::commit();
            }else{
                $response['status'] = false;
                $response['msg'] = "Server down";
            }
        }catch(Exception $ex){
            $response['status'] = false;
            $response['msg'] = $ex->getMessage();
            DB::rollback();
        }
        return $response;
    }

    //get history of used voucher
    public static function rechargeHistory(){
        $data = DB::table('voucher')->where('status', 1)->join('identification', 'identification.user_id', '=', 'voucher.used_by')->orderBy('date_used', 'DESC')->paginate(15);
        return $data;
    }

    //get history of verified tellers
    public static function confirmedTellers(){
        $data = DB::table('deposit_slips')->where('status', CARD_USED)->join('identification', 'identification.user_id', '=', 'deposit_slips.usedBy')->orderBy('date_used', 'DESC')->paginate(15);
        return $data;
    }

//print voucher
    public static function printVoucher(){
        $data = DB::table('voucher')->where('print_status', 0)->get();
        $value['print_status'] = 1;
        DB::table('voucher')->where('print_status', 0)->update($value);
        return $data;
    }
    //enter slip into deposit system
    public static  function sendSlip($data){
        $dt = new DateTime();
        $data['date_created'] = $dt->format('Y-m-d H:i:s');
        try{
            DB::table('deposit_slips')->insertGetId($data);
            $response['status'] = true;
        }catch (Exception $ex){
            $response['status'] = false;
            $response['msg'] = $ex->getMessage();
        }
        return $response;
    }

    //confirm slip entry
    public static function checkSlip($data)
    {
        $response['status'] = true;
        $check = DB::table('deposit_slips')->where(DB::raw('BINARY tellerNumber'), $data['tellerNumber'])->where('amount', $data['amount'])->first();
        if ($check !== null){
            DB::beginTransaction();
            try {
                if($check->status == CARD_USED){
                    $response['status'] = false;
                    if($check->used_by == Auth::id){
                        $response['msg'] = "Deposit slip has being credited to your account before";
                    }else{
                        $response['msg'] = "Deposit slip has being credited before";
                    }
                }else if($check->status == CARD_NOT_USED){
                    $dt = new DateTime();
                    $value['usedBy'] = Auth::id();
                    $value['status'] = CARD_USED;
                    $value['date_used'] = $dt->format('Y-m-d H:i:s');
                    DB::table('deposit_slips')->where('id', $check->id)->update($value);
                    //credit account
                    DB::table('user_balance')->where('user_id', Auth::id())->increment('balance', $check->amount, array('date_modified'=>$dt->format('Y-m-d H:i:s')));
                    $response['status'] = true;
                    DB::commit();
                }

            } catch (Exception $ex) {
                $response['status'] = false;
                $response['msg'] = $ex->getMessage();
                DB::rollback();
            }
        }
        else{
            $response['status'] =  false;
            $response['msg'] = "Teller not confirmed yet!";
        }

        return $response;
    }

    //create training
    public static function createTraining($data){
        $response['status'] = true;
        DB::beginTransaction();
        try{
            $training_id = DB::table('training')->insertGetId($data['training']);
            foreach($data['courses'] as $key=>$value){
                $info['training_id'] = $training_id;
                $info['course_id'] = $value;
                DB::table('training_courses')->insert($info);
            }
            DB::commit();
        }catch (Exception $ex){
            $response['status'];
            $response['msg'] = $ex->getMessage();
            DB::rollback();
        }
        return $response;
    }

    public static function createConference($data, $confData){
        $response['status'] = true;
        DB::beginTransaction();
        try{
            $id = DB::table('training')->insertGetId($data);
            $confData['conf_id'] = $id;
            DB::table('conferences')->insert($confData);
            DB::commit();
        }catch (Exception $ex){
            $response['status'];
            $response['msg'] = $ex->getMessage();
            DB::rollback();
        }
        return $response;
    }

    //open training
    public static  function openTraining($data){
        try{
            DB::table('training')->where('training_id', $data['id'])->update(array('status'=>Constants::ACTIVE_TRAINING));
            return true;
        }catch (Exception $ex){
            return false;
        }
    }

//close training
    public static  function closeTraining($data){
        try{
            DB::table('training')->where('training_id', $data['id'])->update(array('status'=>Constants::RUNNING_TRAINING));
            return true;
        }catch (Exception $ex){
            return false;
        }
    }
    //terminate training
    public static  function terminateTraining($data){
        try{
            DB::table('training')->where('training_id', $data['id'])->update(array('status'=>Constants::TERMINATED_TRAINING));
            return true;
        }catch (Exception $ex){
            return false;
        }
    }


    //information
    public static function postInformation($data){
        $response['status'] = true;
        try{
            DB::table('information')->insert($data);
        }catch (Exception $ex){
            $response['msg'] = $ex->getMessage();
            $response['status'] = false;
        }
        return $response;
    }

    public static function deleteInformation($id){
        try{
            DB::table('information')->where('id', '=', $id)->delete();
            return true;
        }catch (Exception $ex){
            return false;
        }
    }

    public static function getInformation(){
        $data =  DB::table('information')->orderBY('date_modified', 'DESC')->paginate(10);
        return $data;
    }

}

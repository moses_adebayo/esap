<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Olajuwon {


    public static function format_date($date){
        $month = substr($date, 5, 2);
        switch($month){
            case 01: $month = "Jan."; break;
            case 02: $month = "Feb.";  break;
            case 03: $month = "Mar.";  break;
            case 04: $month = "Apr."; break;
            case 05: $month = "May";  break;
            case 06: $month = "Jun.";  break;
            case 07: $month = "Jul."; break;
            case 8: $month = "Aug.";  break;
            case 9: $month = "Sep.";  break;
            case 10: $month = "Oct."; break;
            case 11: $month = "Nov.";  break;
            case 12: $month = "Dec.";  break;
        }
        $format_date = array("year"=>substr($date, 0, 4), "month"=>$month, "day"=>substr($date, 8, 2));

        return $format_date;
    }

    public static function format_money($money){
        $format = "<span class='naira'>N</span>".number_format($money, 0);
        return $format;
    }


}

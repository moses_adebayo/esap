<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    //registration
    public static function registration($data){
        try{
            DB::beginTransaction();
            //users table
            $user_id =  DB::table('users')->insertGetId($data['users']);
            //user auth table info
            $data['user_auth']['id'] = $user_id;
            $data['user_auth']['password'] = Hash::make($data['user_auth']['password']);
            DB::table('user_auth')->insert($data['user_auth']);
            //identification
            $data['identification']['user_id'] = $user_id;
            DB::table('identification')->insert($data['identification']);
            //user balance
            $data['user_balance']['user_id'] = $user_id;
            DB::table('user_balance')->insert($data['user_balance']);
            Log::info($data['identification']['first_name'] . ' registered successfully');
            DB::commit();
            return $user_id;
        }catch(Exception $ex){
            Log::error('error in registration');
            DB::rollback();
            return $ex->getMessage();
        }
    }
    public static function complete_registration($data){
        try{
            DB::beginTransaction();
            //update user status
            $user_id = Auth::id();
            DB::table('users')->where('id', $user_id)->update(array('status'=>1));

            //identification
            DB::table('identification')->where('user_id', $user_id)->update($data);
            //user balance
            Log::info($user_id . ' registered successfully');
            DB::commit();
            return 1;
        }catch(Exception $ex){
            Log::error('error in completing registration');
            DB::rollback();
            return $ex->getMessage();
        }
    }

    //verify uniqueness of email
    public static function verifyEmail($email){
        $check = DB::table('user_auth')->where('email', $email)->exists();
        return $check;
    }
    public static function validateRegForm($data){
        $response = array();
        $response['status'] = true;
        if(strcmp($data['password'], $data['c_password']) != 0){
            $response['status'] = false;
            $response['msg'] = 'Password miss-match';
            Log::error('Error in registration');
        }else if(strlen($data['password']) < 6){
            $response['status'] = false;
            $response['msg'] = 'Password must be at least 6 letters';
            Log::error('Error in registration');
        }else if(!User::isValidEmail($data['email'])){
            $response['status'] = false;
            $response['msg'] = 'Email not valid';
            Log::error('Error in registration');
        }else if(User::verifyEmail($data['email'])){
            $response['status'] = false;
            $response['msg'] = "Email has been registered before";
        }
        return $response;
    }

    public static function isValidEmail($email){
        $regex = '/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i';
        $check = preg_replace($regex, '', $email);
        return empty($check) ? true : false;
    }

    public static function userInfo($id){
        $data = DB::table('identification')->where('user_id', '=', $id)->join('user_auth', 'identification.user_id', '=', 'user_auth.id')->first();
        return $data;
    }
    public static function userType(){
        $data = DB::table('user_auth')->select('user_type')->where('id', '=', Auth::id())->first();
        return $data;
    }

    //login attempts
    public static function loginUser($data){

        $password  = DB::table('user_auth')->select('password')->where('email', '=', $data['email'])->first();
        if(isset($password)){
            if(Hash::check($data['password'], $password->password)){
                Log::info('User log in successfully'.$data['email']);
                $user_info  = DB::table('user_auth')->where('email', '=', $data['email'])->take(1)->get();
                return $user_info;
            }else{
                Log::info('User log failed'.$data['email']);
                return null;
            }

        }else{
            Log::info('User log failed'.$data['email']);
            return null;
        }
    }

    //get all users
    public static function getAllUsers(){
        $data = DB::table('identification')->join('user_auth', 'user_auth.id','=', 'identification.user_id')->where('user_auth.user_type', 2)->get();
        return $data;
    }

}

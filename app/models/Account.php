<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Account {


    //apply for training
    public static  function get_balance($user_id){
        try{
            $data = DB::table('user_balance')->where('user_id', '=', $user_id)->first();
            return $data;
        }catch (Exception $ex){
            Log::error('Error in retrieving training');
            return $ex->getMessage();
        }
    }

    public static  function withdraw($user_id, $amount){
        DB::beginTransaction();
        try{
            DB::table('user_balance')->where('user_id', $user_id)->decrement('balance', $amount);
            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
        }
    }

    public static  function deposit($user_id, $amount){
        DB::beginTransaction();
        try{
            DB::table('user_balance')->where('user_id', $user_id)->increment('balance', $amount);
            DB::commit();
        }catch(Exception $ex){
            Log::error($ex->getMessage());
            DB::rollback();
        }
    }


}

<?php
/**
 * Created by PhpStorm.
 * User: olajuwon
 * Date: 1/31/2015
 * Time: 2:58 AM
 */

//application types
define('TRAINING', 1);
define('CONFERENCE', 2);

//ACCESS CARDS STATUS
define('CARD_USED', 1);
define('CARD_NOT_USED', 0);
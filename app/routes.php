<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as'=>'homepage', 'uses'=>'UserController@homepage'));
Route::get('home', array('uses'=>'UserController@homepage'));

Route::get('credits', array('uses'=>'TestimonialController@get_all'));

Route::get('information', array('uses'=>'InformationController@records'));
Route::any('get_info', array('uses'=>'InformationController@side_info'));

Route::get('contact', function(){
    return View::make('contact');
});
Route::get('timetable', function(){
 return View::make('timetable');
});

Route::post('contact', array("as"=>"send_mail", "before"=>"csrf", "uses"=>"ContactController@sendMail"));
Route::post('contact2', array("as"=>"send_mail_form2", "before"=>"csrf", "uses"=>"ContactController@sendMailForm2"));

Route::get('login', function(){
    return View::make('login');
});


Route::any('user_login', array('uses'=>'UserController@login'));
Route::any('user_login_wGoogle', array('uses'=>'UserController@loginWGoogle'));
Route::any('user_login_wSocial', array('uses'=>'UserController@loginWSocial'));
Route::any('logout', array('uses'=>'UserController@logout'));
Route::get('registration', function(){
    return View::make('registration');
});

//retrieve all training
Route::get('training',array('as'=>'training','uses'=>'TrainingController@open'));

Route::get('conference',array('as'=>'conference','uses'=>'ConferenceController@open'));

//open selected training
Route::get('apply/{code?}', array('before'=>'auth','as'=>'application', 'uses'=>'TrainingController@open_selected'));

Route::get('apply/conference/{code?}', array('before'=>'auth','as'=>'conference_application', 'uses'=>'ConferenceController@open_selected'));

//open edit application form
Route::get('apply/edit/{app_id}', array('before'=>'auth', 'as'=>'edit_application', 'uses'=>'TrainingController@updateApplication'));

//process user application
Route::any('apply/apply_training',array('before'=>'auth', 'uses'=>'TrainingController@apply'));
Route::any('apply/conference/apply_conference',array('before'=>'auth', 'uses'=>'ConferenceController@apply'));

Route::get('test2', array('uses'=>'ConferenceController@apply'));
//edit user application
Route::any('apply/edit/edit_application',array('before'=>'auth', 'uses'=>'TrainingController@editApplication'));



Route::get('about', function(){
    return View::make('about');
});
Route::get('esap_training', function(){
    return View::make('esap_training');
});
Route::get('courses', function(){
    return View::make('courses');
});
Route::get('legs_of_esap', function(){
    return View::make('legs_of_esap');
});
Route::get('invite', function(){
    return View::make('invite_esap');
});
Route::get('volunteer', function(){
    return View::make('esap_volunteer');
});
Route::get('careers', function(){
    return View::make('careers');
});

//Conferences announcement
Route::get('studentsconference101', array('as'=>'showConf', 'uses'=>'ConferenceController@showConference'));

Route::get('testimonial', array('uses'=>'TestimonialController@get'));

Route::post('register', array('uses'=>'UserController@register'));
Route::post('register_with_google', array('uses'=>'UserController@registerWithGoogle'));
Route::post('register_with_social', array('uses'=>'UserController@registerWithSocial'));

Route::get('account', array('as'=>'user_account', 'before'=>'auth', 'uses'=>'AccountController@open'));

Route::get('pdf', function(){
    $pdf = PDF::loadView('moses');
    return $pdf->download('invoice.pdf');
});

//FOR TRAINING
Route::get('slip/{id}', array('uses'=>'TrainingController@print_slip'));
//FOR CONFERENCE
Route::get('slip2/{id}', array('uses'=>'ConferenceController@print_slip'));

//Login users actions
//admin panel
Route::group(array('before'=>'auth'), function(){
    Route::get('complete_reg', function(){
        return View::make('complete_reg');
    });
    Route::post('complete_register', array('uses'=>'UserController@completeRegister'));
    Route::any('load_voucher', array('as'=>'load_voucher', 'uses'=>'AdminController@loadVoucher'));
    Route::post('recharge_by_deposit', array('uses'=>'AdminController@checkSlipDetails'));

});

//admin panel
Route::group(array('before'=>'auth|admin'), function(){
    Route::get('admin', array('as'=>'admin_panel', 'uses'=>'AdminController@home'));
    Route::get('teller_form', array('uses'=>'AdminController@tellerView'));
    Route::post('teller_form_process', array('uses'=>'AdminController@sendSlipDetails'));
    Route::post('admin', array('as'=>'generate_voucher', 'uses'=>'AdminController@generateVoucher'));
    Route::post('admin/print', array('as'=>'print_voucher', 'uses'=>'AdminController@printVoucher'));
    Route::any('create_training', array('as'=>'create_training', 'uses'=>'AdminController@createTraining'));
    Route::get('admin/training', array('as'=>'form_training', 'uses'=>'AdminController@startTrainingForm'));
    Route::post('open/training', array('before'=>'csrf','as'=>'open_training', 'uses'=>'AdminController@openTraining'));
    Route::post('terminate/training', array('before'=>'csrf','as'=>'terminate_training', 'uses'=>'AdminController@terminateTraining'));
    Route::post('close/training', array('before'=>'csrf','as'=>'close_training', 'uses'=>'AdminController@closeTraining'));
    Route::any('create_conference', array('as'=>'create_conference', 'uses'=>'AdminController@createConference'));
    Route::get('admin/conference', array('as'=>'form_conference', 'uses'=>'AdminController@startConferenceForm'));
    Route::post('open/conference', array('before'=>'csrf','as'=>'open_conference', 'uses'=>'AdminController@openConference'));
    Route::post('terminate/conference', array('before'=>'csrf','as'=>'terminate_conference', 'uses'=>'AdminController@terminateConference'));
    Route::post('close/training', array('before'=>'csrf','as'=>'close_conference', 'uses'=>'AdminController@closeConference'));
    Route::get('conference/list/{id}', array("uses"=>"ConferenceController@getList"));
    Route::get('training/list/{id}', array("uses"=>"TrainingController@getList"));
    Route::get('training/course/list/{id}/{course_id}', array("uses"=>"TrainingController@getCourseList"));
    Route::get('user/slip/{id}', array("uses"=>"AdminController@userSlip"));
    Route::get('panel/information', array("as"=>"new_info", "uses"=>"AdminController@createInformation"));
    Route::post('panel/information', array("before"=>"csrf", "as"=>"post_info", "uses"=>"AdminController@postInformation"));
    Route::post('panel//delete/information', array("before"=>"csrf", "as"=>"delete_info", "uses"=>"AdminController@deleteInformation"));
    Route::get('training/all_users/{t_id}', array("as"=>"all_users", "uses"=>"TrainingController@getAllCourse"));
    Route::any('training_students/{t_id}/{c_id}', array("uses"=>"TrainingController@getTrainingStudents"));
    Route::get('all-users', array("as"=>"all-users", "uses"=>"UserController@getAllUsers"));
});

/*
Temporary pages
*/
Route::get('oau/season4', array("as" => "oaus4", "uses"=>"TrainingController@activeRunningTrainingPage"));